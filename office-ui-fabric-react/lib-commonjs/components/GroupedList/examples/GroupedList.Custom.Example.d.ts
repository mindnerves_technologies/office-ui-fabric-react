import * as React from 'react';
export declare class GroupedListCustomExample extends React.Component {
    private _items;
    private _groups;
    render(): JSX.Element;
    private _onRenderCell;
    private _onRenderHeader;
    private _onRenderFooter;
}
