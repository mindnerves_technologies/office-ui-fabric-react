"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable */
var load_themed_styles_1 = require("@microsoft/load-themed-styles");
load_themed_styles_1.loadStyles([{ "rawString": ".content_015fb52a{background-color:" }, { "theme": "themePrimary", "defaultValue": "#0078d4" }, { "rawString": ";color:" }, { "theme": "white", "defaultValue": "#ffffff" }, { "rawString": ";line-height:50px;padding:0 20px;display:-webkit-box;display:-ms-flexbox;display:flex}.textContent_015fb52a{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1}.nonLayered_015fb52a{background-color:" }, { "theme": "neutralTertiaryAlt", "defaultValue": "#c8c6c4" }, { "rawString": ";line-height:50px;padding:0 20px;margin:8px 0}.customHost_015fb52a.ms-LayerHost{height:100px;padding:20px;background:rgba(255,0,0,0.2);border:1px dashed black;position:relative}.customHost_015fb52a:before{position:absolute;left:50%;top:50%;-webkit-transform:translate(-50%, -50%);transform:translate(-50%, -50%);content:'I am a LayerHost with id=layerhost1'}\n" }]);
exports.content = "content_015fb52a";
exports.textContent = "textContent_015fb52a";
exports.nonLayered = "nonLayered_015fb52a";
exports.customHost = "customHost_015fb52a";
//# sourceMappingURL=Layer.Example.scss.js.map