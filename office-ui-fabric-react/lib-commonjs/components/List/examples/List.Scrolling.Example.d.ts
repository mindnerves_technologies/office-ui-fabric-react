import * as React from 'react';
import { ScrollToMode } from 'office-ui-fabric-react/lib/List';
import { IExampleItem } from '@uifabric/example-data';
export interface IListScrollingExampleProps {
    items?: IExampleItem[];
}
export interface IListScrollingExampleState {
    selectedIndex: number;
    scrollToMode: ScrollToMode;
    showItemIndexInView: boolean;
}
export declare class ListScrollingExample extends React.Component<IListScrollingExampleProps, IListScrollingExampleState> {
    private _list;
    private _items;
    constructor(props: IListScrollingExampleProps);
    render(): JSX.Element;
    componentWillUnmount(): void;
    private _getPageHeight;
    private _onChangeText;
    private _onDropdownChange;
    private _onRenderCell;
    private _scrollRelative;
    private _scroll;
    private _resolveList;
    private _onShowItemIndexInViewChanged;
}
