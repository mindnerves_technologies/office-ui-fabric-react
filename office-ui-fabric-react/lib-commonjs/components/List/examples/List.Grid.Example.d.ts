import * as React from 'react';
import { IExampleItem } from '@uifabric/example-data';
export interface IListGridExampleProps {
    items?: IExampleItem[];
}
export declare class ListGridExample extends React.Component<IListGridExampleProps> {
    private _columnCount;
    private _columnWidth;
    private _rowHeight;
    private _items;
    constructor(props: IListGridExampleProps);
    render(): JSX.Element;
    private _getItemCountForPage;
    private _getPageHeight;
    private _onRenderCell;
}
