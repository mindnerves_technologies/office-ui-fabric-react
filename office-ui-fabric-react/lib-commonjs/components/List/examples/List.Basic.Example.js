"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var Utilities_1 = require("office-ui-fabric-react/lib/Utilities");
var FocusZone_1 = require("office-ui-fabric-react/lib/FocusZone");
var TextField_1 = require("office-ui-fabric-react/lib/TextField");
var Image_1 = require("office-ui-fabric-react/lib/Image");
var Icon_1 = require("office-ui-fabric-react/lib/Icon");
var List_1 = require("office-ui-fabric-react/lib/List");
var Styling_1 = require("office-ui-fabric-react/lib/Styling");
var example_data_1 = require("@uifabric/example-data");
var theme = Styling_1.getTheme();
var palette = theme.palette, semanticColors = theme.semanticColors, fonts = theme.fonts;
var classNames = Styling_1.mergeStyleSets({
    itemCell: [
        Styling_1.getFocusStyle(theme, { inset: -1 }),
        {
            minHeight: 54,
            padding: 10,
            boxSizing: 'border-box',
            borderBottom: "1px solid " + semanticColors.bodyDivider,
            display: 'flex',
            selectors: {
                '&:hover': { background: palette.neutralLight },
            },
        },
    ],
    itemImage: {
        flexShrink: 0,
    },
    itemContent: {
        marginLeft: 10,
        overflow: 'hidden',
        flexGrow: 1,
    },
    itemName: [
        fonts.xLarge,
        {
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
        },
    ],
    itemIndex: {
        fontSize: fonts.small.fontSize,
        color: palette.neutralTertiary,
        marginBottom: 10,
    },
    chevron: {
        alignSelf: 'center',
        marginLeft: 10,
        color: palette.neutralTertiary,
        fontSize: fonts.large.fontSize,
        flexShrink: 0,
    },
});
var ListBasicExample = /** @class */ (function (_super) {
    tslib_1.__extends(ListBasicExample, _super);
    function ListBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onFilterChanged = function (_, text) {
            _this.setState({
                filterText: text,
                items: text
                    ? _this._originalItems.filter(function (item) { return item.name.toLowerCase().indexOf(text.toLowerCase()) >= 0; })
                    : _this._originalItems,
            });
        };
        _this._originalItems = props.items || example_data_1.createListItems(5000);
        _this.state = {
            filterText: '',
            items: _this._originalItems,
        };
        return _this;
    }
    ListBasicExample.prototype.render = function () {
        var _a = this.state.items, items = _a === void 0 ? [] : _a;
        var resultCountText = items.length === this._originalItems.length ? '' : " (" + items.length + " of " + this._originalItems.length + " shown)";
        return (React.createElement(FocusZone_1.FocusZone, { direction: FocusZone_1.FocusZoneDirection.vertical },
            React.createElement(TextField_1.TextField, { label: 'Filter by name' + resultCountText, onChange: this._onFilterChanged }),
            React.createElement(List_1.List, { items: items, onRenderCell: this._onRenderCell })));
    };
    ListBasicExample.prototype._onRenderCell = function (item, index) {
        return (React.createElement("div", { className: classNames.itemCell, "data-is-focusable": true },
            React.createElement(Image_1.Image, { className: classNames.itemImage, src: item.thumbnail, width: 50, height: 50, imageFit: Image_1.ImageFit.cover }),
            React.createElement("div", { className: classNames.itemContent },
                React.createElement("div", { className: classNames.itemName }, item.name),
                React.createElement("div", { className: classNames.itemIndex }, "Item " + index),
                React.createElement("div", null, item.description)),
            React.createElement(Icon_1.Icon, { className: classNames.chevron, iconName: Utilities_1.getRTL() ? 'ChevronLeft' : 'ChevronRight' })));
    };
    return ListBasicExample;
}(React.Component));
exports.ListBasicExample = ListBasicExample;
//# sourceMappingURL=List.Basic.Example.js.map