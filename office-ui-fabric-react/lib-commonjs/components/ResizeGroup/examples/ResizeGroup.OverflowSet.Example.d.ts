import * as React from 'react';
import { IContextualMenuItem } from 'office-ui-fabric-react/lib/ContextualMenu';
export interface IOverflowData {
    primary: IContextualMenuItem[];
    overflow: IContextualMenuItem[];
    cacheKey?: string;
}
export interface IResizeGroupOverflowSetExampleState {
    short: boolean;
    numberOfItems: number;
    buttonsChecked: boolean;
    cachingEnabled: boolean;
    onGrowDataEnabled: boolean;
}
export declare class ResizeGroupOverflowSetExample extends React.Component<{}, IResizeGroupOverflowSetExampleState> {
    constructor(props: {});
    render(): JSX.Element;
    private _onReduceData;
    private _onGrowData;
    private _onCachingEnabledChanged;
    private _onGrowDataEnabledChanged;
    private _onButtonsCheckedChanged;
    private _onNumberOfItemsChanged;
}
