"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var index_1 = require("office-ui-fabric-react/lib/index");
var Styling_1 = require("office-ui-fabric-react/lib/Styling");
var classNames = Styling_1.mergeStyleSets({
    wrapper: { display: 'flex' },
    column2: { marginLeft: 10 },
});
var colorPickerStyles = {
    panel: { padding: 12 },
    root: {
        maxWidth: 352,
        minWidth: 352,
    },
    colorRectangle: { height: 268 },
};
var ColorPickerBasicExample = /** @class */ (function (_super) {
    tslib_1.__extends(ColorPickerBasicExample, _super);
    function ColorPickerBasicExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            color: index_1.getColorFromString('#ffffff'),
            alphaSliderHidden: false,
            showPreview: true,
        };
        _this._updateColor = function (ev, colorObj) {
            _this.setState({ color: colorObj });
        };
        _this._onHideAlphaClick = function (ev, checked) {
            var color = _this.state.color;
            if (checked) {
                // If hiding the alpha slider, remove transparency from the color
                color = index_1.updateA(_this.state.color, 100);
            }
            _this.setState({ alphaSliderHidden: !!checked, color: color });
        };
        _this._onShowPreviewBoxClick = function (ev, checked) {
            _this.setState({ showPreview: !!checked });
        };
        return _this;
    }
    ColorPickerBasicExample.prototype.render = function () {
        var _a = this.state, color = _a.color, alphaSliderHidden = _a.alphaSliderHidden, showPreview = _a.showPreview;
        return (React.createElement("div", { className: classNames.wrapper },
            React.createElement(index_1.ColorPicker, { color: color, onChange: this._updateColor, alphaSliderHidden: alphaSliderHidden, showPreview: showPreview, styles: colorPickerStyles, 
                // The ColorPicker provides default English strings for visible text.
                // If your app is localized, you MUST provide the `strings` prop with localized strings.
                // Below are the recommended aria labels for the hue and alpha slider
                strings: {
                    alphaAriaLabel: 'Alpha Slider: Use left and right arrow keys to change value, hold shift for a larger jump',
                    hueAriaLabel: 'Hue Slider: Use left and right arrow keys to change value, hold shift for a larger jump',
                } }),
            React.createElement("div", { className: classNames.column2 },
                React.createElement(index_1.Toggle, { label: "Hide alpha slider", onChange: this._onHideAlphaClick, checked: alphaSliderHidden }),
                React.createElement(index_1.Toggle, { label: "Show Preview Box", onChange: this._onShowPreviewBoxClick, checked: showPreview }))));
    };
    return ColorPickerBasicExample;
}(React.Component));
exports.ColorPickerBasicExample = ColorPickerBasicExample;
//# sourceMappingURL=ColorPicker.Basic.Example.js.map