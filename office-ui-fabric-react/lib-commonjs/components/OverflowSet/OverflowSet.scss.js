"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable */
var load_themed_styles_1 = require("@microsoft/load-themed-styles");
load_themed_styles_1.loadStyles([{ "rawString": ".root_1a34f243{position:relative;display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:nowrap;flex-wrap:nowrap}.rootVertical_1a34f243{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.item_1a34f243{-ms-flex-negative:0;flex-shrink:0;display:inherit}\n" }]);
exports.root = "root_1a34f243";
exports.rootVertical = "rootVertical_1a34f243";
exports.item = "item_1a34f243";
//# sourceMappingURL=OverflowSet.scss.js.map