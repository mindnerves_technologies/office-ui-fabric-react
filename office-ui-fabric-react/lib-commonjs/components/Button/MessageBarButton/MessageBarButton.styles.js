"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Styling_1 = require("../../../Styling");
var Utilities_1 = require("../../../Utilities");
var BaseButton_styles_1 = require("../BaseButton.styles");
exports.getStyles = Utilities_1.memoizeFunction(function (theme, customStyles, focusInset, focusColor) {
    var baseButtonStyles = BaseButton_styles_1.getStyles(theme);
    var messageBarButtonStyles = {
        root: [
            Styling_1.getFocusStyle(theme, {
                inset: 1,
                highContrastStyle: {
                    outlineOffset: '-4px',
                    outlineColor: 'ActiveBorder',
                },
                borderColor: 'transparent',
            }),
            {
                height: 24,
                borderColor: theme.palette.neutralTertiaryAlt,
            },
        ],
    };
    return Styling_1.concatStyleSets(baseButtonStyles, messageBarButtonStyles, customStyles);
});
//# sourceMappingURL=MessageBarButton.styles.js.map