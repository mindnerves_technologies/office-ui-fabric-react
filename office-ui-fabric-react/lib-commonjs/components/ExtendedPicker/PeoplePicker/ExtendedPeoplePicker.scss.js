"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable */
var load_themed_styles_1 = require("@microsoft/load-themed-styles");
load_themed_styles_1.loadStyles([{ "rawString": ".resultContent_be21407a{display:table-row}.resultContent_be21407a .resultItem_be21407a{display:table-cell;vertical-align:bottom}.peoplePickerPersona_be21407a{width:180px}.peoplePickerPersona_be21407a .ms-Persona-details{width:100%}.peoplePicker_be21407a .ms-BasePicker-text{min-height:40px}.peoplePickerPersonaContent_be21407a{display:-webkit-box;display:-ms-flexbox;display:flex;width:100%;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center}\n" }]);
exports.resultContent = "resultContent_be21407a";
exports.resultItem = "resultItem_be21407a";
exports.peoplePickerPersona = "peoplePickerPersona_be21407a";
exports.peoplePicker = "peoplePicker_be21407a";
exports.peoplePickerPersonaContent = "peoplePickerPersonaContent_be21407a";
//# sourceMappingURL=ExtendedPeoplePicker.scss.js.map