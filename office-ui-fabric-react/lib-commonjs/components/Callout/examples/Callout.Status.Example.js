"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var office_ui_fabric_react_1 = require("office-ui-fabric-react");
// Themed styles for the example.
var theme = office_ui_fabric_react_1.getTheme();
var styles = office_ui_fabric_react_1.mergeStyleSets({
    buttonArea: {
        verticalAlign: 'top',
        display: 'inline-block',
        textAlign: 'center',
        margin: '0 100px',
        minWidth: 130,
        height: 32,
    },
    callout: {
        maxWidth: 300,
    },
    subtext: [
        theme.fonts.small,
        {
            margin: 0,
            height: '100%',
            padding: '24px 20px',
            fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
        },
    ],
});
// Example code
var StatusCalloutExample = /** @class */ (function (_super) {
    tslib_1.__extends(StatusCalloutExample, _super);
    function StatusCalloutExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            isCalloutVisible: false,
        };
        _this._menuButtonElement = React.createRef();
        _this._onShowMenuClicked = function () {
            _this.setState({
                isCalloutVisible: !_this.state.isCalloutVisible,
            });
        };
        _this._onCalloutDismiss = function () {
            _this.setState({
                isCalloutVisible: false,
            });
        };
        return _this;
    }
    StatusCalloutExample.prototype.render = function () {
        var isCalloutVisible = this.state.isCalloutVisible;
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: styles.buttonArea, ref: this._menuButtonElement },
                React.createElement(office_ui_fabric_react_1.DefaultButton, { onClick: this._onShowMenuClicked, text: isCalloutVisible ? 'Hide StatusCallout' : 'Show StatusCallout' })),
            this.state.isCalloutVisible && (React.createElement(office_ui_fabric_react_1.Callout, { className: styles.callout, target: this._menuButtonElement.current, onDismiss: this._onCalloutDismiss, role: "status", "aria-live": "assertive" },
                React.createElement(office_ui_fabric_react_1.DelayedRender, null,
                    React.createElement(React.Fragment, null,
                        React.createElement("p", { className: styles.subtext }, "This message is treated as an aria-live assertive status message, and will be read by a screen reader regardless of focus.")))))));
    };
    return StatusCalloutExample;
}(React.Component));
exports.StatusCalloutExample = StatusCalloutExample;
//# sourceMappingURL=Callout.Status.Example.js.map