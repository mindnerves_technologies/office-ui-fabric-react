"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var office_ui_fabric_react_1 = require("office-ui-fabric-react");
// Themed styles for the example.
var theme = office_ui_fabric_react_1.getTheme();
var styles = office_ui_fabric_react_1.mergeStyleSets({
    buttonArea: {
        verticalAlign: 'top',
        display: 'inline-block',
        textAlign: 'center',
        margin: '0 100px',
        minWidth: 130,
        height: 32,
    },
    callout: {
        maxWidth: 300,
    },
    header: {
        padding: '18px 24px 12px',
    },
    title: [
        theme.fonts.xLarge,
        {
            margin: 0,
            fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
        },
    ],
    inner: {
        height: '100%',
        padding: '0 24px 20px',
    },
    actions: {
        position: 'relative',
        marginTop: 20,
        width: '100%',
        whiteSpace: 'nowrap',
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
        padding: '0 24px 24px',
    },
    subtext: [
        theme.fonts.small,
        {
            margin: 0,
            fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
        },
    ],
});
var CalloutFocusTrapExample = /** @class */ (function (_super) {
    tslib_1.__extends(CalloutFocusTrapExample, _super);
    function CalloutFocusTrapExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            isCalloutVisible: false,
        };
        _this._onDismiss = function () {
            _this.setState({
                isCalloutVisible: !_this.state.isCalloutVisible,
            });
        };
        return _this;
    }
    CalloutFocusTrapExample.prototype.render = function () {
        var _this = this;
        var isCalloutVisible = this.state.isCalloutVisible;
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: styles.buttonArea, ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                React.createElement(office_ui_fabric_react_1.DefaultButton, { onClick: this._onDismiss, text: isCalloutVisible ? 'Hide FocusTrapCallout' : 'Show FocusTrapCallout' })),
            isCalloutVisible ? (React.createElement("div", null,
                React.createElement(office_ui_fabric_react_1.FocusTrapCallout, { role: "alertdialog", className: styles.callout, gapSpace: 0, target: this._menuButtonElement, onDismiss: this._onDismiss, setInitialFocus: true },
                    React.createElement("div", { className: styles.header },
                        React.createElement("p", { className: styles.title }, "Callout title here")),
                    React.createElement("div", { className: styles.inner },
                        React.createElement("div", null,
                            React.createElement("p", { className: styles.subtext }, "Content is wrapped in a FocusTrapZone so that user cannot accidently tab out of this callout."))),
                    React.createElement(office_ui_fabric_react_1.FocusZone, null,
                        React.createElement(office_ui_fabric_react_1.Stack, { className: styles.buttons, gap: 8, horizontal: true },
                            React.createElement(office_ui_fabric_react_1.PrimaryButton, { onClick: this._onDismiss }, "Button 1"),
                            React.createElement(office_ui_fabric_react_1.DefaultButton, { onClick: this._onDismiss }, "Button 2")))))) : null));
    };
    return CalloutFocusTrapExample;
}(React.Component));
exports.CalloutFocusTrapExample = CalloutFocusTrapExample;
//# sourceMappingURL=Callout.FocusTrap.Example.js.map