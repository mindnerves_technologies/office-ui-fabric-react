"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var office_ui_fabric_react_1 = require("office-ui-fabric-react");
var DIRECTION_OPTIONS = [
    { key: office_ui_fabric_react_1.DirectionalHint.topLeftEdge, text: 'Top Left Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.topCenter, text: 'Top Center' },
    { key: office_ui_fabric_react_1.DirectionalHint.topRightEdge, text: 'Top Right Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.topAutoEdge, text: 'Top Auto Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.bottomLeftEdge, text: 'Bottom Left Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.bottomCenter, text: 'Bottom Center' },
    { key: office_ui_fabric_react_1.DirectionalHint.bottomRightEdge, text: 'Bottom Right Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.bottomAutoEdge, text: 'Bottom Auto Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.leftTopEdge, text: 'Left Top Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.leftCenter, text: 'Left Center' },
    { key: office_ui_fabric_react_1.DirectionalHint.leftBottomEdge, text: 'Left Bottom Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.rightTopEdge, text: 'Right Top Edge' },
    { key: office_ui_fabric_react_1.DirectionalHint.rightCenter, text: 'Right Center' },
    { key: office_ui_fabric_react_1.DirectionalHint.rightBottomEdge, text: 'Right Bottom Edge' },
];
var theme = office_ui_fabric_react_1.getTheme();
var styles = office_ui_fabric_react_1.mergeStyleSets({
    buttonArea: {
        verticalAlign: 'top',
        display: 'inline-block',
        textAlign: 'center',
        margin: '0 100px',
        minWidth: 130,
        height: 32,
    },
    configArea: {
        minWidth: '300px',
        display: 'inline-block',
    },
    callout: {
        maxWidth: 300,
    },
    calloutExampleButton: {
        width: '100%',
    },
    header: {
        padding: '18px 24px 12px',
    },
    title: [
        theme.fonts.xLarge,
        {
            margin: 0,
            fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
        },
    ],
    inner: {
        height: '100%',
        padding: '0 24px 20px',
    },
    subtext: [
        theme.fonts.small,
        {
            margin: 0,
            fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
        },
    ],
    link: [
        theme.fonts.medium,
        {
            color: theme.palette.neutralPrimary,
        },
    ],
    actions: {
        position: 'relative',
        marginTop: 20,
        width: '100%',
        whiteSpace: 'nowrap',
    },
});
var CalloutDirectionalExample = /** @class */ (function (_super) {
    tslib_1.__extends(CalloutDirectionalExample, _super);
    function CalloutDirectionalExample(props) {
        var _this = _super.call(this, props) || this;
        // Use getId() to ensure that the callout label and description IDs are unique on the page.
        // It's also okay, though not recommended, to use plain strings without getId() and manually ensure their uniqueness.
        _this._labelId = office_ui_fabric_react_1.getId('callout-label');
        _this._descriptionId = office_ui_fabric_react_1.getId('callout-description');
        _this._onCalloutDismiss = function () {
            _this.setState({
                isCalloutVisible: false,
            });
        };
        _this._onShowMenuClicked = function () {
            _this.setState({
                isCalloutVisible: !_this.state.isCalloutVisible,
            });
        };
        _this._onShowBeakChange = function (ev, isVisible) {
            _this.setState({
                isBeakVisible: isVisible,
                beakWidth: 10,
            });
        };
        _this._onDirectionalChanged = function (event, option) {
            _this.setState({
                directionalHint: option.key,
            });
        };
        _this._onGapSlider = function (value) {
            _this.setState({
                gapSpace: value,
            });
        };
        _this._onBeakWidthSlider = function (value) {
            _this.setState({
                beakWidth: value,
            });
        };
        _this.state = {
            isCalloutVisible: false,
            isBeakVisible: true,
            directionalHint: office_ui_fabric_react_1.DirectionalHint.bottomLeftEdge,
        };
        return _this;
    }
    CalloutDirectionalExample.prototype.render = function () {
        var _this = this;
        var _a = this.state, isCalloutVisible = _a.isCalloutVisible, isBeakVisible = _a.isBeakVisible, directionalHint = _a.directionalHint, gapSpace = _a.gapSpace, beakWidth = _a.beakWidth;
        //  ms-Callout-smallbeak is used in this directional example to reflect all the positions.
        //  Large beak will disable some position to avoid beak over the callout edge.
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: styles.configArea },
                React.createElement(office_ui_fabric_react_1.Checkbox, { styles: { root: { margin: '10px 0' } }, label: "Show beak", checked: isBeakVisible, onChange: this._onShowBeakChange }),
                React.createElement(office_ui_fabric_react_1.Slider, { max: 30, label: "Gap Space", min: 0, defaultValue: 0, onChange: this._onGapSlider }),
                isBeakVisible && (React.createElement(office_ui_fabric_react_1.Slider, { max: 50, label: "Beak Width", min: 10, defaultValue: 16, onChange: this._onBeakWidthSlider })),
                React.createElement(office_ui_fabric_react_1.Dropdown, { label: "Directional hint", selectedKey: directionalHint, options: DIRECTION_OPTIONS, onChange: this._onDirectionalChanged })),
            React.createElement("div", { className: styles.buttonArea, ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                React.createElement(office_ui_fabric_react_1.DefaultButton, { className: styles.calloutExampleButton, onClick: this._onShowMenuClicked, text: isCalloutVisible ? 'Hide callout' : 'Show callout' })),
            isCalloutVisible ? (React.createElement(office_ui_fabric_react_1.Callout, { ariaLabelledBy: this._labelId, ariaDescribedBy: this._descriptionId, className: styles.callout, gapSpace: gapSpace, target: this._menuButtonElement, isBeakVisible: isBeakVisible, beakWidth: beakWidth, onDismiss: this._onCalloutDismiss, directionalHint: directionalHint, setInitialFocus: true },
                React.createElement("div", { className: styles.header },
                    React.createElement("p", { className: styles.title, id: this._labelId }, "All of your favorite people")),
                React.createElement("div", { className: styles.inner },
                    React.createElement("p", { className: styles.subtext, id: this._descriptionId }, "Message body is optional. If help documentation is available, consider adding a link to learn more at the bottom."),
                    React.createElement("div", { className: styles.actions },
                        React.createElement(office_ui_fabric_react_1.Link, { className: styles.link, href: "http://microsoft.com", target: "_blank" }, "Go to Microsoft"))))) : null));
    };
    return CalloutDirectionalExample;
}(React.Component));
exports.CalloutDirectionalExample = CalloutDirectionalExample;
//# sourceMappingURL=Callout.Directional.Example.js.map