"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var Dropdown_1 = require("office-ui-fabric-react/lib/Dropdown");
var office_ui_fabric_react_1 = require("office-ui-fabric-react");
var DayPickerStrings = {
    months: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ],
    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    goToToday: 'Go to today',
    prevMonthAriaLabel: 'Go to previous month',
    nextMonthAriaLabel: 'Go to next month',
    prevYearAriaLabel: 'Go to previous year',
    nextYearAriaLabel: 'Go to next year',
    closeButtonAriaLabel: 'Close date picker',
};
var controlClass = office_ui_fabric_react_1.mergeStyleSets({
    control: {
        margin: '0 0 15px 0',
        maxWidth: '300px',
    },
});
var DatePickerBasicExample = /** @class */ (function (_super) {
    tslib_1.__extends(DatePickerBasicExample, _super);
    function DatePickerBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onDropdownChange = function (event, option) {
            _this.setState({
                firstDayOfWeek: office_ui_fabric_react_1.DayOfWeek[option.key],
            });
        };
        _this.state = {
            firstDayOfWeek: office_ui_fabric_react_1.DayOfWeek.Sunday,
        };
        return _this;
    }
    DatePickerBasicExample.prototype.render = function () {
        var firstDayOfWeek = this.state.firstDayOfWeek;
        return (React.createElement("div", { className: "docs-DatePickerExample" },
            React.createElement(office_ui_fabric_react_1.DatePicker, { className: controlClass.control, firstDayOfWeek: firstDayOfWeek, strings: DayPickerStrings, placeholder: "Select a date...", ariaLabel: "Select a date" }),
            React.createElement(Dropdown_1.Dropdown, { className: controlClass.control, label: "Select the first day of the week", options: [
                    {
                        text: 'Sunday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Sunday],
                    },
                    {
                        text: 'Monday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Monday],
                    },
                    {
                        text: 'Tuesday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Tuesday],
                    },
                    {
                        text: 'Wednesday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Wednesday],
                    },
                    {
                        text: 'Thursday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Thursday],
                    },
                    {
                        text: 'Friday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Friday],
                    },
                    {
                        text: 'Saturday',
                        key: office_ui_fabric_react_1.DayOfWeek[office_ui_fabric_react_1.DayOfWeek.Saturday],
                    },
                ], selectedKey: office_ui_fabric_react_1.DayOfWeek[firstDayOfWeek], onChange: this._onDropdownChange })));
    };
    return DatePickerBasicExample;
}(React.Component));
exports.DatePickerBasicExample = DatePickerBasicExample;
//# sourceMappingURL=DatePicker.Basic.Example.js.map