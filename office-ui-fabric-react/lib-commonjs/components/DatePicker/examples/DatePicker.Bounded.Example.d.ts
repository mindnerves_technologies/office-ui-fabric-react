import * as React from 'react';
import { DayOfWeek } from 'office-ui-fabric-react/lib/DatePicker';
export interface IDatePickerRequiredExampleState {
    firstDayOfWeek?: DayOfWeek;
}
export declare class DatePickerBoundedExample extends React.Component<{}, IDatePickerRequiredExampleState> {
    constructor(props: {});
    render(): JSX.Element;
}
