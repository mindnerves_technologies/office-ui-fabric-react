"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var Button_1 = require("office-ui-fabric-react/lib/Button");
var TeachingBubble_1 = require("office-ui-fabric-react/lib/TeachingBubble");
var Callout_1 = require("office-ui-fabric-react/lib/Callout");
var TeachingBubbleWideExample = /** @class */ (function (_super) {
    tslib_1.__extends(TeachingBubbleWideExample, _super);
    function TeachingBubbleWideExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onDismiss = _this._onDismiss.bind(_this);
        _this._onShow = _this._onShow.bind(_this);
        _this.state = {
            isTeachingBubbleVisible: false,
        };
        return _this;
    }
    TeachingBubbleWideExample.prototype.render = function () {
        var _this = this;
        var isTeachingBubbleVisible = this.state.isTeachingBubbleVisible;
        return (React.createElement("div", { className: "ms-TeachingBubbleExample" },
            React.createElement("span", { className: "ms-TeachingBubbleBasicExample-buttonArea", ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                React.createElement(Button_1.DefaultButton, { onClick: isTeachingBubbleVisible ? this._onDismiss : this._onShow, text: isTeachingBubbleVisible ? 'Hide TeachingBubble' : 'Show TeachingBubble' })),
            isTeachingBubbleVisible ? (React.createElement("div", null,
                React.createElement(TeachingBubble_1.TeachingBubble, { calloutProps: { directionalHint: Callout_1.DirectionalHint.bottomCenter }, isWide: true, hasCloseButton: true, closeButtonAriaLabel: "Close", target: this._menuButtonElement, onDismiss: this._onDismiss, headline: "Discover what\u2019s trending around you" }, "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nulla, ipsum? Molestiae quis aliquam magni harum non?"))) : null));
    };
    TeachingBubbleWideExample.prototype._onDismiss = function (ev) {
        this.setState({
            isTeachingBubbleVisible: false,
        });
    };
    TeachingBubbleWideExample.prototype._onShow = function (ev) {
        this.setState({
            isTeachingBubbleVisible: true,
        });
    };
    return TeachingBubbleWideExample;
}(React.Component));
exports.TeachingBubbleWideExample = TeachingBubbleWideExample;
//# sourceMappingURL=TeachingBubble.Wide.Example.js.map