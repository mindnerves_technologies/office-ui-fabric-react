import * as React from 'react';
interface IHoverCardExampleState {
    target: HTMLElement | null;
    eventListenerTarget: HTMLElement | null;
}
export declare class HoverCardEventListenerTargetExample extends React.Component<{}, IHoverCardExampleState> {
    state: {
        target: null;
        eventListenerTarget: null;
    };
    render(): JSX.Element;
    private _onRenderPlainCard;
    private _setTarget;
    private _setEventListenerTarget;
}
export {};
