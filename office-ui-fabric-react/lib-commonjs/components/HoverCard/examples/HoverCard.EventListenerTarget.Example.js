"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var HoverCard_1 = require("office-ui-fabric-react/lib/HoverCard");
var Fabric_1 = require("office-ui-fabric-react/lib/Fabric");
var Styling_1 = require("office-ui-fabric-react/lib/Styling");
var office_ui_fabric_react_1 = require("office-ui-fabric-react");
var classNames = Styling_1.mergeStyleSets({
    plainCard: {
        width: 200,
        height: 200,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textField: {
        paddingRight: 200,
    },
});
var HoverCardEventListenerTargetExample = /** @class */ (function (_super) {
    tslib_1.__extends(HoverCardEventListenerTargetExample, _super);
    function HoverCardEventListenerTargetExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            target: null,
            eventListenerTarget: null,
        };
        _this._onRenderPlainCard = function () {
            return React.createElement("div", { className: classNames.plainCard }, "plain card");
        };
        _this._setTarget = function (element) {
            _this.setState({
                target: element,
            });
        };
        _this._setEventListenerTarget = function (element) {
            _this.setState({
                eventListenerTarget: element,
            });
        };
        return _this;
    }
    HoverCardEventListenerTargetExample.prototype.render = function () {
        var plainCardProps = {
            onRenderPlainCard: this._onRenderPlainCard,
            directionalHint: HoverCard_1.DirectionalHint.rightTopEdge,
        };
        return (React.createElement(Fabric_1.Fabric, null,
            React.createElement("p", null, "Using the target to tag hover card on the right side of Emoji icon, and using eventListenerTarget to launch the card only when hovering over the text field, hovering over the icon doesn't trigger card open."),
            React.createElement("span", { ref: this._setTarget },
                React.createElement("span", { ref: this._setEventListenerTarget, className: classNames.textField }, "Hover Zone"),
                React.createElement(office_ui_fabric_react_1.IconButton, { iconProps: { iconName: 'Emoji2' }, title: 'Emoji' }),
                React.createElement(HoverCard_1.HoverCard, { plainCardProps: plainCardProps, type: HoverCard_1.HoverCardType.plain, target: this.state.target, eventListenerTarget: this.state.eventListenerTarget }))));
    };
    return HoverCardEventListenerTargetExample;
}(React.Component));
exports.HoverCardEventListenerTargetExample = HoverCardEventListenerTargetExample;
//# sourceMappingURL=HoverCard.EventListenerTarget.Example.js.map