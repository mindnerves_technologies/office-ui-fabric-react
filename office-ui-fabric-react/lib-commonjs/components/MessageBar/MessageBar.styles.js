"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Styling_1 = require("../../Styling");
var MessageBar_types_1 = require("./MessageBar.types");
var GlobalClassNames = {
    root: 'ms-MessageBar',
    error: 'ms-MessageBar--error',
    blocked: 'ms-MessageBar--blocked',
    severeWarning: 'ms-MessageBar--severeWarning',
    success: 'ms-MessageBar--success',
    warning: 'ms-MessageBar--warning',
    multiline: 'ms-MessageBar-multiline',
    singleline: 'ms-MessageBar-singleline',
    dismissalSingleLine: 'ms-MessageBar-dismissalSingleLine',
    expandingSingleLine: 'ms-MessageBar-expandingSingleLine',
    content: 'ms-MessageBar-content',
    iconContainer: 'ms-MessageBar-icon',
    text: 'ms-MessageBar-text',
    innerText: 'ms-MessageBar-innerText',
    dismissSingleLine: 'ms-MessageBar-dismissSingleLine',
    expandSingleLine: 'ms-MessageBar-expandSingleLine',
    dismissal: 'ms-MessageBar-dismissal',
    expand: 'ms-MessageBar-expand',
    actions: 'ms-MessageBar-actions',
    actionsSingleline: 'ms-MessageBar-actionsSingleLine',
};
// Returns the background color of the MessageBar root element based on the type of MessageBar.
var getRootBackground = function (messageBarType, palette, semanticColors) {
    switch (messageBarType) {
        case MessageBar_types_1.MessageBarType.error:
        case MessageBar_types_1.MessageBarType.blocked:
            return semanticColors.errorBackground;
        case MessageBar_types_1.MessageBarType.severeWarning:
            return semanticColors.blockingBackground;
        case MessageBar_types_1.MessageBarType.success:
            return semanticColors.successBackground;
        case MessageBar_types_1.MessageBarType.warning:
            return semanticColors.warningBackground;
    }
    return palette.neutralLighter;
};
/**
 * Returns the high contrast mode background color of the MessageBar root element based on the type of MessageBar.
 * The fact that the styles don't vary based on the theme is intentional since the objective is to show the message bar
 * type, and theme variations would not be appreciated in High Contrast either way.
 */
var getHighContrastRootBackground = function (messageBarType) {
    switch (messageBarType) {
        case MessageBar_types_1.MessageBarType.error:
        case MessageBar_types_1.MessageBarType.blocked:
        case MessageBar_types_1.MessageBarType.severeWarning:
            return 'rgba(255, 0, 0, 0.3)';
        case MessageBar_types_1.MessageBarType.success:
            return 'rgba(48, 241, 73, 0.3)';
        case MessageBar_types_1.MessageBarType.warning:
            return 'rgba(255, 254, 57, 0.3)';
    }
    return 'Window';
};
// Returns the icon color based on the type of MessageBar.
var getIconColor = function (messageBarType, palette, semanticColors) {
    switch (messageBarType) {
        case MessageBar_types_1.MessageBarType.error:
        case MessageBar_types_1.MessageBarType.blocked:
        case MessageBar_types_1.MessageBarType.severeWarning:
            return semanticColors.errorText;
        case MessageBar_types_1.MessageBarType.success:
            return palette.green;
        case MessageBar_types_1.MessageBarType.warning:
            return semanticColors.warningText;
    }
    return palette.neutralSecondary;
};
exports.getStyles = function (props) {
    var _a, _b, _c, _d, _e, _f;
    var theme = props.theme, className = props.className, messageBarType = props.messageBarType, onDismiss = props.onDismiss, truncated = props.truncated, isMultiline = props.isMultiline, expandSingleLine = props.expandSingleLine;
    var semanticColors = theme.semanticColors, palette = theme.palette, fonts = theme.fonts;
    var SmallScreenSelector = Styling_1.getScreenSelector(0, Styling_1.ScreenWidthMaxSmall);
    var classNames = Styling_1.getGlobalClassNames(GlobalClassNames, theme);
    var dismissalAndExpandIconStyle = {
        fontSize: Styling_1.IconFontSizes.xSmall,
        height: 10,
        lineHeight: '10px',
        color: palette.neutralPrimary,
        selectors: (_a = {},
            _a[Styling_1.HighContrastSelector] = {
                MsHighContrastAdjust: 'none',
                color: 'WindowText',
            },
            _a),
    };
    var dismissalAndExpandStyle = [
        Styling_1.getFocusStyle(theme, {
            inset: 1,
            highContrastStyle: {
                outlineOffset: '-4px',
                outlineColor: 'Window',
            },
            borderColor: 'transparent',
        }),
        {
            flexShrink: 0,
            width: 32,
            height: 32,
            padding: '8px 12px',
            selectors: {
                '& .ms-Button-icon': dismissalAndExpandIconStyle,
                ':hover': {
                    backgroundColor: 'transparent',
                },
                ':active': {
                    backgroundColor: 'transparent',
                },
            },
        },
    ];
    return {
        root: [
            classNames.root,
            theme.fonts.medium,
            messageBarType === MessageBar_types_1.MessageBarType.error && classNames.error,
            messageBarType === MessageBar_types_1.MessageBarType.blocked && classNames.blocked,
            messageBarType === MessageBar_types_1.MessageBarType.severeWarning && classNames.severeWarning,
            messageBarType === MessageBar_types_1.MessageBarType.success && classNames.success,
            messageBarType === MessageBar_types_1.MessageBarType.warning && classNames.warning,
            isMultiline ? classNames.multiline : classNames.singleline,
            !isMultiline && onDismiss && classNames.dismissalSingleLine,
            !isMultiline && truncated && classNames.expandingSingleLine,
            {
                background: getRootBackground(messageBarType, palette, semanticColors),
                color: palette.neutralPrimary,
                minHeight: 32,
                width: '100%',
                display: 'flex',
                wordBreak: 'break-word',
                selectors: (_b = {
                        '& .ms-Link': tslib_1.__assign(tslib_1.__assign({ color: palette.themeDark }, fonts.small), { selectors: (_c = {},
                                _c[Styling_1.HighContrastSelector] = {
                                    MsHighContrastAdjust: 'auto',
                                },
                                _c) })
                    },
                    _b[Styling_1.HighContrastSelector] = {
                        background: getHighContrastRootBackground(messageBarType),
                        border: '1px solid WindowText',
                        color: 'WindowText',
                    },
                    _b),
            },
            isMultiline && {
                flexDirection: 'column',
            },
            className,
        ],
        content: [
            classNames.content,
            {
                display: 'flex',
                width: '100%',
                lineHeight: 'normal',
            },
        ],
        iconContainer: [
            classNames.iconContainer,
            {
                fontSize: Styling_1.IconFontSizes.medium,
                minWidth: 16,
                minHeight: 16,
                display: 'flex',
                flexShrink: 0,
                margin: '8px 0 8px 12px',
            },
        ],
        icon: {
            color: getIconColor(messageBarType, palette, semanticColors),
            selectors: (_d = {},
                _d[Styling_1.HighContrastSelector] = {
                    MsHighContrastAdjust: 'none',
                    color: 'WindowText',
                },
                _d),
        },
        text: [
            classNames.text,
            tslib_1.__assign(tslib_1.__assign({ minWidth: 0, display: 'flex', flexGrow: 1, margin: 8 }, fonts.small), { selectors: (_e = {},
                    _e[Styling_1.HighContrastSelector] = {
                        MsHighContrastAdjust: 'none',
                    },
                    _e) }),
            !onDismiss && {
                marginRight: 12,
            },
        ],
        innerText: [
            classNames.innerText,
            {
                lineHeight: 16,
                selectors: {
                    '& span a': {
                        paddingLeft: 4,
                    },
                },
            },
            truncated && {
                overflow: 'visible',
                whiteSpace: 'pre-wrap',
            },
            !isMultiline && {
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
            },
            !isMultiline &&
                !truncated && {
                selectors: (_f = {},
                    _f[SmallScreenSelector] = {
                        overflow: 'visible',
                        whiteSpace: 'pre-wrap',
                    },
                    _f),
            },
            expandSingleLine && {
                overflow: 'visible',
                whiteSpace: 'pre-wrap',
            },
        ],
        dismissSingleLine: [classNames.dismissSingleLine],
        expandSingleLine: [classNames.expandSingleLine],
        dismissal: [classNames.dismissal, dismissalAndExpandStyle],
        expand: [classNames.expand, dismissalAndExpandStyle],
        actions: [
            isMultiline ? classNames.actions : classNames.actionsSingleline,
            {
                display: 'flex',
                flexGrow: 0,
                flexShrink: 0,
                flexBasis: 'auto',
                flexDirection: 'row-reverse',
                alignItems: 'center',
                margin: '0 12px 0 8px',
                selectors: {
                    '& button:nth-child(n+2)': {
                        marginLeft: 8,
                    },
                },
            },
            isMultiline && {
                marginBottom: 8,
            },
            onDismiss &&
                !isMultiline && {
                marginRight: 0,
            },
        ],
    };
};
//# sourceMappingURL=MessageBar.styles.js.map