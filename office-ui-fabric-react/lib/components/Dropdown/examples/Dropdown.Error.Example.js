import { __extends } from "tslib";
import * as React from 'react';
import { Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
var DropdownErrorExample = /** @class */ (function (_super) {
    __extends(DropdownErrorExample, _super);
    function DropdownErrorExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // Don't show the error message by default because it's annoying to screen reader users.
        _this.state = { showError: false };
        _this._updateShowError = function (event, checked) {
            _this.setState({ showError: !!checked });
        };
        return _this;
    }
    DropdownErrorExample.prototype.render = function () {
        var showError = this.state.showError;
        var stackTokens = { childrenGap: 30 };
        return (React.createElement(Stack, { horizontal: true, tokens: stackTokens, verticalAlign: "start" },
            React.createElement(Toggle, { label: "Show error message", onText: "Yes", offText: "No", checked: showError, onChange: this._updateShowError }),
            React.createElement(Dropdown, { placeholder: "Select an option", label: "Dropdown with error message", options: [
                    { key: 'A', text: 'Option a' },
                    { key: 'B', text: 'Option b' },
                    { key: 'C', text: 'Option c' },
                    { key: 'D', text: 'Option d' },
                    { key: 'E', text: 'Option e' },
                ], errorMessage: showError ? 'This dropdown has an error' : undefined, styles: { dropdown: { width: 300 }, root: { height: 100 } } })));
    };
    return DropdownErrorExample;
}(React.Component));
export { DropdownErrorExample };
//# sourceMappingURL=Dropdown.Error.Example.js.map