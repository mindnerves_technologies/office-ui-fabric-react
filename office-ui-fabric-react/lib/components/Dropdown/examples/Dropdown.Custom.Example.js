import { __extends } from "tslib";
import * as React from 'react';
import { Dropdown, DropdownMenuItemType } from 'office-ui-fabric-react/lib/Dropdown';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { IconButton } from 'office-ui-fabric-react/lib/Button';
var exampleOptions = [
    { key: 'Header', text: 'Options', itemType: DropdownMenuItemType.Header },
    { key: 'A', text: 'Option a', data: { icon: 'Memo' } },
    { key: 'B', text: 'Option b', data: { icon: 'Print' } },
    { key: 'C', text: 'Option c', data: { icon: 'ShoppingCart' } },
    { key: 'D', text: 'Option d', data: { icon: 'Train' } },
    { key: 'E', text: 'Option e', data: { icon: 'Repair' } },
    { key: 'divider_2', text: '-', itemType: DropdownMenuItemType.Divider },
    { key: 'Header2', text: 'More options', itemType: DropdownMenuItemType.Header },
    { key: 'F', text: 'Option f', data: { icon: 'Running' } },
    { key: 'G', text: 'Option g', data: { icon: 'EmojiNeutral' } },
    { key: 'H', text: 'Option h', data: { icon: 'ChatInviteFriend' } },
    { key: 'I', text: 'Option i', data: { icon: 'SecurityGroup' } },
    { key: 'J', text: 'Option j', data: { icon: 'AddGroup' } },
];
var stackTokens = { childrenGap: 20 };
var DropdownCustomExample = /** @class */ (function (_super) {
    __extends(DropdownCustomExample, _super);
    function DropdownCustomExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onRenderOption = function (option) {
            return (React.createElement("div", null,
                option.data && option.data.icon && (React.createElement(Icon, { style: { marginRight: '8px' }, iconName: option.data.icon, "aria-hidden": "true", title: option.data.icon })),
                React.createElement("span", null, option.text)));
        };
        _this._onRenderTitle = function (options) {
            var option = options[0];
            return (React.createElement("div", null,
                option.data && option.data.icon && (React.createElement(Icon, { style: { marginRight: '8px' }, iconName: option.data.icon, "aria-hidden": "true", title: option.data.icon })),
                React.createElement("span", null, option.text)));
        };
        _this._onRenderPlaceholder = function (props) {
            return (React.createElement("div", { className: "dropdownExample-placeholder" },
                React.createElement(Icon, { style: { marginRight: '8px' }, iconName: 'MessageFill', "aria-hidden": "true" }),
                React.createElement("span", null, props.placeholder)));
        };
        _this._onRenderCaretDown = function (props) {
            return React.createElement(Icon, { iconName: "CirclePlus" });
        };
        _this._onRenderLabel = function (props) {
            return (React.createElement(Stack, { horizontal: true, verticalAlign: "center" },
                React.createElement("span", { style: { fontWeight: 600 } }, props.label),
                React.createElement(IconButton, { iconProps: { iconName: 'Info' }, title: "Info", ariaLabel: "Info", styles: { root: { marginBottom: -3 } } })));
        };
        return _this;
    }
    DropdownCustomExample.prototype.render = function () {
        return (React.createElement(Stack, { tokens: stackTokens },
            React.createElement(Dropdown, { placeholder: "Select an option", label: "Custom example", ariaLabel: "Custom dropdown example", onRenderPlaceholder: this._onRenderPlaceholder, onRenderTitle: this._onRenderTitle, onRenderOption: this._onRenderOption, onRenderCaretDown: this._onRenderCaretDown, styles: { dropdown: { width: 300 } }, options: exampleOptions }),
            React.createElement(Dropdown, { placeholder: "Select an option", label: "Custom label", ariaLabel: "Custom dropdown label example", styles: { dropdown: { width: 300 } }, options: exampleOptions, onRenderLabel: this._onRenderLabel })));
    };
    return DropdownCustomExample;
}(React.PureComponent));
export { DropdownCustomExample };
//# sourceMappingURL=Dropdown.Custom.Example.js.map