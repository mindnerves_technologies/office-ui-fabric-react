import { __extends, __spreadArrays } from "tslib";
import * as React from 'react';
import { Dropdown, DropdownMenuItemType } from 'office-ui-fabric-react/lib/Dropdown';
var DropdownControlledMultiExample = /** @class */ (function (_super) {
    __extends(DropdownControlledMultiExample, _super);
    function DropdownControlledMultiExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            selectedItems: [],
        };
        _this._onChange = function (event, item) {
            var newSelectedItems = __spreadArrays(_this.state.selectedItems);
            if (item.selected) {
                // add the option if it's checked
                newSelectedItems.push(item.key);
            }
            else {
                // remove the option if it's unchecked
                var currIndex = newSelectedItems.indexOf(item.key);
                if (currIndex > -1) {
                    newSelectedItems.splice(currIndex, 1);
                }
            }
            _this.setState({
                selectedItems: newSelectedItems,
            });
        };
        return _this;
    }
    DropdownControlledMultiExample.prototype.render = function () {
        var selectedItems = this.state.selectedItems;
        return (React.createElement(Dropdown, { placeholder: "Select options", label: "Multi-select controlled example", selectedKeys: selectedItems, onChange: this._onChange, multiSelect: true, options: [
                { key: 'fruitsHeader', text: 'Fruits', itemType: DropdownMenuItemType.Header },
                { key: 'apple', text: 'Apple' },
                { key: 'banana', text: 'Banana' },
                { key: 'orange', text: 'Orange', disabled: true },
                { key: 'grape', text: 'Grape' },
                { key: 'divider_1', text: '-', itemType: DropdownMenuItemType.Divider },
                { key: 'vegetablesHeader', text: 'Vegetables', itemType: DropdownMenuItemType.Header },
                { key: 'broccoli', text: 'Broccoli' },
                { key: 'carrot', text: 'Carrot' },
                { key: 'lettuce', text: 'Lettuce' },
            ], styles: { dropdown: { width: 300 } } }));
    };
    return DropdownControlledMultiExample;
}(React.Component));
export { DropdownControlledMultiExample };
//# sourceMappingURL=Dropdown.ControlledMulti.Example.js.map