import { __extends } from "tslib";
import * as React from 'react';
import { Dropdown, DropdownMenuItemType } from 'office-ui-fabric-react/lib/Dropdown';
var DropdownControlledExample = /** @class */ (function (_super) {
    __extends(DropdownControlledExample, _super);
    function DropdownControlledExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            selectedItem: undefined,
        };
        _this._onChange = function (event, item) {
            console.log("Selection change: " + item.text + " " + (item.selected ? 'selected' : 'unselected'));
            _this.setState({ selectedItem: item });
        };
        return _this;
    }
    DropdownControlledExample.prototype.render = function () {
        var selectedItem = this.state.selectedItem;
        return (React.createElement(Dropdown, { label: "Controlled example", selectedKey: selectedItem ? selectedItem.key : undefined, onChange: this._onChange, placeholder: "Select an option", options: [
                { key: 'fruitsHeader', text: 'Fruits', itemType: DropdownMenuItemType.Header },
                { key: 'apple', text: 'Apple' },
                { key: 'banana', text: 'Banana' },
                { key: 'orange', text: 'Orange', disabled: true },
                { key: 'grape', text: 'Grape' },
                { key: 'divider_1', text: '-', itemType: DropdownMenuItemType.Divider },
                { key: 'vegetablesHeader', text: 'Vegetables', itemType: DropdownMenuItemType.Header },
                { key: 'broccoli', text: 'Broccoli' },
                { key: 'carrot', text: 'Carrot' },
                { key: 'lettuce', text: 'Lettuce' },
            ], styles: { dropdown: { width: 300 } } }));
    };
    return DropdownControlledExample;
}(React.Component));
export { DropdownControlledExample };
//# sourceMappingURL=Dropdown.Controlled.Example.js.map