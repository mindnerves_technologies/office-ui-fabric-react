import { __extends } from "tslib";
import * as React from 'react';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { Layer, LayerHost } from 'office-ui-fabric-react/lib/Layer';
import { AnimationClassNames, mergeStyles } from 'office-ui-fabric-react/lib/Styling';
import { getId, css } from 'office-ui-fabric-react/lib/Utilities';
import * as styles from './Layer.Example.scss';
var toggleStyles = {
    root: { margin: '10px 0' },
};
var rootClass = mergeStyles({
    selectors: { p: { marginTop: 30 } },
});
var LayerHostedExample = /** @class */ (function (_super) {
    __extends(LayerHostedExample, _super);
    function LayerHostedExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            showLayer: false,
            showLayerNoId: false,
            showHost: true,
        };
        // Use getId() to ensure that the ID is unique on the page.
        // (It's also okay to use a plain string without getId() and manually ensure uniqueness.)
        _this._layerHostId = getId('layerhost');
        _this._onChangeCheckbox = function (ev, checked) {
            _this.setState({ showLayer: checked });
        };
        _this._onChangeCheckboxNoId = function (ev, checked) {
            _this.setState({ showLayerNoId: checked });
        };
        _this._onChangeToggle = function (ev, checked) {
            _this.setState({ showHost: checked });
        };
        return _this;
    }
    LayerHostedExample.prototype.render = function () {
        var _a = this.state, showLayer = _a.showLayer, showLayerNoId = _a.showLayerNoId, showHost = _a.showHost;
        var content = (React.createElement("div", { className: css(styles.content, AnimationClassNames.scaleUpIn100) }, "This is example layer content."));
        return (React.createElement("div", { className: rootClass },
            React.createElement(Toggle, { label: "Show host", inlineLabel: true, checked: showHost, onChange: this._onChangeToggle }),
            showHost && React.createElement(LayerHost, { id: this._layerHostId, className: styles.customHost }),
            React.createElement("p", null, "In some cases, you may need to contain layered content within an area. Create an instance of a LayerHost along with an id, and provide a hostId on the layer to render it within the specific host. (Note that it's important that you don't include children within the LayerHost. It's meant to contain Layered content only.)"),
            React.createElement(Toggle, { styles: toggleStyles, label: "Render the box below in a Layer and target it at hostId=" + this._layerHostId, inlineLabel: true, checked: showLayer, onChange: this._onChangeCheckbox }),
            showLayer ? (React.createElement(Layer, { hostId: this._layerHostId, onLayerDidMount: this._log('didmount'), onLayerWillUnmount: this._log('willunmount') }, content)) : (content),
            React.createElement("div", { className: styles.nonLayered }, "I am normally below the content."),
            React.createElement("p", null, "If you do not specify a hostId, the hosted layer will default to being fixed to the page by default."),
            React.createElement(Toggle, { styles: toggleStyles, label: "Render the box below in a Layer without specifying a host, fixing it to the top of the page", inlineLabel: true, checked: showLayerNoId, onChange: this._onChangeCheckboxNoId }),
            showLayerNoId ? (React.createElement(Layer, { onLayerDidMount: this._log('didmount'), onLayerWillUnmount: this._log('willunmount') }, content)) : (content)));
    };
    LayerHostedExample.prototype._log = function (text) {
        return function () {
            console.log(text);
        };
    };
    return LayerHostedExample;
}(React.Component));
export { LayerHostedExample };
//# sourceMappingURL=Layer.Hosted.Example.js.map