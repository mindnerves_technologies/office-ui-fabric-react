/* tslint:disable */
import { loadStyles } from '@microsoft/load-themed-styles';
loadStyles([{ "rawString": ".content_015fb52a{background-color:" }, { "theme": "themePrimary", "defaultValue": "#0078d4" }, { "rawString": ";color:" }, { "theme": "white", "defaultValue": "#ffffff" }, { "rawString": ";line-height:50px;padding:0 20px;display:-webkit-box;display:-ms-flexbox;display:flex}.textContent_015fb52a{-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1}.nonLayered_015fb52a{background-color:" }, { "theme": "neutralTertiaryAlt", "defaultValue": "#c8c6c4" }, { "rawString": ";line-height:50px;padding:0 20px;margin:8px 0}.customHost_015fb52a.ms-LayerHost{height:100px;padding:20px;background:rgba(255,0,0,0.2);border:1px dashed black;position:relative}.customHost_015fb52a:before{position:absolute;left:50%;top:50%;-webkit-transform:translate(-50%, -50%);transform:translate(-50%, -50%);content:'I am a LayerHost with id=layerhost1'}\n" }]);
export var content = "content_015fb52a";
export var textContent = "textContent_015fb52a";
export var nonLayered = "nonLayered_015fb52a";
export var customHost = "customHost_015fb52a";
//# sourceMappingURL=Layer.Example.scss.js.map