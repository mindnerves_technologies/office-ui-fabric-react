import { __extends } from "tslib";
import { DefaultButton, PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import { Dialog, DialogFooter, DialogType } from 'office-ui-fabric-react/lib/Dialog';
import { Panel, PanelType } from 'office-ui-fabric-react/lib/Panel';
import * as React from 'react';
var LayerNestedLayersExample = /** @class */ (function (_super) {
    __extends(LayerNestedLayersExample, _super);
    function LayerNestedLayersExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            hideDialog: true,
            showPanel: false,
        };
        _this._showDialog = function () {
            _this.setState({ hideDialog: false });
        };
        _this._dismissDialog = function () {
            _this.setState({ hideDialog: true });
        };
        _this._dismissPanel = function () {
            _this.setState({ showPanel: false });
        };
        _this._onShowPanel = function () {
            _this.setState({ showPanel: true });
        };
        return _this;
    }
    LayerNestedLayersExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(DefaultButton, { secondaryText: "Opens the Sample Panel", onClick: this._onShowPanel, text: "Open Panel" }),
            React.createElement(Panel, { isOpen: this.state.showPanel, type: PanelType.smallFixedFar, onDismiss: this._dismissPanel, headerText: "This panel makes use of Layer and FocusTrapZone. Focus should be trapped in the panel.", closeButtonAriaLabel: "Close" },
                React.createElement(DefaultButton, { secondaryText: "Opens the Sample Dialog", onClick: this._showDialog, text: "Open Dialog" }),
                React.createElement(Dialog, { hidden: this.state.hideDialog, onDismiss: this._dismissDialog, isBlocking: true, dialogContentProps: {
                        type: DialogType.normal,
                        title: 'This dialog uses Modal, which also makes use of Layer and FocusTrapZone. ' +
                            'Focus should be trapped in the dialog.',
                        subText: "Focus will move back to the panel if you press 'OK' or 'Cancel'.",
                    }, modalProps: {
                        titleAriaId: 'myLabelId',
                        subtitleAriaId: 'mySubTextId',
                        isBlocking: false,
                        styles: { main: { maxWidth: 450 } },
                    } },
                    React.createElement(DialogFooter, null,
                        React.createElement(PrimaryButton, { onClick: this._dismissDialog, text: "OK" }),
                        React.createElement(DefaultButton, { onClick: this._dismissDialog, text: "Cancel" }))))));
    };
    return LayerNestedLayersExample;
}(React.Component));
export { LayerNestedLayersExample };
//# sourceMappingURL=Layer.NestedLayers.Example.js.map