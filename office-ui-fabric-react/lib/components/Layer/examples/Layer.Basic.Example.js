import { __extends } from "tslib";
import * as React from 'react';
import * as styles from './Layer.Example.scss';
import { AnimationClassNames } from 'office-ui-fabric-react/lib/Styling';
import { css, Async } from 'office-ui-fabric-react/lib/Utilities';
import { Layer } from 'office-ui-fabric-react/lib/Layer';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
var LayerBasicExampleContext = React.createContext({ message: undefined });
var LayerContentExample = /** @class */ (function (_super) {
    __extends(LayerContentExample, _super);
    function LayerContentExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            time: new Date().toLocaleTimeString(),
        };
        _this._async = new Async(_this);
        return _this;
    }
    LayerContentExample.prototype.componentDidMount = function () {
        var _this = this;
        this._async.setInterval(function () {
            _this.setState({
                time: new Date().toLocaleTimeString(),
            });
        }, 1000);
    };
    LayerContentExample.prototype.componentWillUnmount = function () {
        this._async.dispose();
    };
    LayerContentExample.prototype.render = function () {
        var _this = this;
        return (React.createElement(LayerBasicExampleContext.Consumer, null, function (value) { return (React.createElement("div", { className: css(styles.content, AnimationClassNames.scaleUpIn100) },
            React.createElement("div", { className: styles.textContent }, value.message),
            React.createElement("div", null, _this.state.time))); }));
    };
    return LayerContentExample;
}(React.Component));
var LayerBasicExample = /** @class */ (function (_super) {
    __extends(LayerBasicExample, _super);
    function LayerBasicExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            showLayer: false,
        };
        _this._onChange = function (ev, checked) {
            _this.setState({ showLayer: checked });
        };
        return _this;
    }
    LayerBasicExample.prototype.render = function () {
        var showLayer = this.state.showLayer;
        return (React.createElement(LayerBasicExampleContext.Provider, { value: {
                message: 'Hello world.',
            } },
            React.createElement("div", null,
                React.createElement(Toggle, { label: "Wrap the content box below in a Layer", inlineLabel: true, checked: showLayer, onChange: this._onChange }),
                showLayer ? (React.createElement(Layer, null,
                    React.createElement(LayerContentExample, null))) : (React.createElement(LayerContentExample, null)))));
    };
    return LayerBasicExample;
}(React.Component));
export { LayerBasicExample };
//# sourceMappingURL=Layer.Basic.Example.js.map