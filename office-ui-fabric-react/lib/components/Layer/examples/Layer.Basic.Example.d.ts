import * as React from 'react';
interface ILayerBasicExampleState {
    showLayer: boolean;
}
export declare class LayerBasicExample extends React.Component<{}, ILayerBasicExampleState> {
    state: {
        showLayer: boolean;
    };
    render(): JSX.Element;
    private _onChange;
}
export {};
