import { __extends } from "tslib";
import * as React from 'react';
import { IconButton } from 'office-ui-fabric-react/lib/Button';
import { Link } from 'office-ui-fabric-react/lib/Link';
import { OverflowSet } from 'office-ui-fabric-react/lib/OverflowSet';
var noOp = function () { return undefined; };
var OverflowSetBasicReversedExample = /** @class */ (function (_super) {
    __extends(OverflowSetBasicReversedExample, _super);
    function OverflowSetBasicReversedExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onRenderItem = function (item) {
            return (React.createElement(Link, { role: "menuitem", styles: { root: { marginRight: 10 } }, onClick: item.onClick }, item.name));
        };
        _this._onRenderOverflowButton = function (overflowItems) {
            var buttonStyles = {
                root: {
                    minWidth: 0,
                    padding: '0 4px',
                    alignSelf: 'stretch',
                    height: 'auto',
                },
            };
            return (React.createElement(IconButton, { role: "menuitem", title: "More options", styles: buttonStyles, menuIconProps: { iconName: 'More' }, menuProps: { items: overflowItems } }));
        };
        return _this;
    }
    OverflowSetBasicReversedExample.prototype.render = function () {
        return (React.createElement(OverflowSet, { "aria-label": "Basic Menu Example", role: "menubar", items: [
                {
                    key: 'item3',
                    name: 'Link 3',
                    onClick: noOp,
                },
                {
                    key: 'item2',
                    name: 'Link 2',
                    onClick: noOp,
                },
                {
                    key: 'item1',
                    name: 'Link 1',
                    onClick: noOp,
                },
            ], overflowItems: [
                {
                    key: 'item4',
                    name: 'Overflow Link 1',
                    onClick: noOp,
                },
                {
                    key: 'item5',
                    name: 'Overflow Link 2',
                    onClick: noOp,
                },
            ], onRenderOverflowButton: this._onRenderOverflowButton, onRenderItem: this._onRenderItem, overflowSide: 'start' }));
    };
    return OverflowSetBasicReversedExample;
}(React.PureComponent));
export { OverflowSetBasicReversedExample };
//# sourceMappingURL=OverflowSet.BasicReversed.Example.js.map