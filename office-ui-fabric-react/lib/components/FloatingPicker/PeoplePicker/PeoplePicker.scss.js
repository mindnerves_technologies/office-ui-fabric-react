/* tslint:disable */
import { loadStyles } from '@microsoft/load-themed-styles';
loadStyles([{ "rawString": ".resultContent_f536abdc{display:table-row}.resultContent_f536abdc .resultItem_f536abdc{display:table-cell;vertical-align:bottom}.peoplePickerPersona_f536abdc{width:180px}.peoplePickerPersona_f536abdc .ms-Persona-details{width:100%}.peoplePicker_f536abdc .ms-BasePicker-text{min-height:40px}.peoplePickerPersonaContent_f536abdc{display:-webkit-box;display:-ms-flexbox;display:flex;width:100%;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:7px 12px}\n" }]);
export var resultContent = "resultContent_f536abdc";
export var resultItem = "resultItem_f536abdc";
export var peoplePickerPersona = "peoplePickerPersona_f536abdc";
export var peoplePicker = "peoplePicker_f536abdc";
export var peoplePickerPersonaContent = "peoplePickerPersonaContent_f536abdc";
//# sourceMappingURL=PeoplePicker.scss.js.map