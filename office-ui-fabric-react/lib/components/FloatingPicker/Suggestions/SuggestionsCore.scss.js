/* tslint:disable */
import { loadStyles } from '@microsoft/load-themed-styles';
loadStyles([{ "rawString": ".suggestionsContainer_3d4990af{overflow-y:auto;overflow-x:hidden;max-height:300px}.suggestionsContainer_3d4990af .ms-Suggestion-item:hover{background-color:" }, { "theme": "neutralLighter", "defaultValue": "#f3f2f1" }, { "rawString": ";cursor:pointer}.suggestionsContainer_3d4990af .is-suggested{background-color:" }, { "theme": "themeLighter", "defaultValue": "#deecf9" }, { "rawString": "}.suggestionsContainer_3d4990af .is-suggested:hover{background-color:" }, { "theme": "themeLight", "defaultValue": "#c7e0f4" }, { "rawString": ";cursor:pointer}\n" }]);
export var suggestionsContainer = "suggestionsContainer_3d4990af";
//# sourceMappingURL=SuggestionsCore.scss.js.map