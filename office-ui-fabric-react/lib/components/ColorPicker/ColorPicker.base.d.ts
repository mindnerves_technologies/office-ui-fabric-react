import * as React from 'react';
import { IColorPickerProps, IColorPicker } from './ColorPicker.types';
import { IColor } from '../../utilities/color/interfaces';
declare type IRGBHex = Pick<IColor, 'r' | 'g' | 'b' | 'a' | 'hex'>;
export interface IColorPickerState {
    color: IColor;
    editingColor?: {
        component: keyof IRGBHex;
        value: string;
    };
}
/**
 * {@docCategory ColorPicker}
 */
export declare class ColorPickerBase extends React.Component<IColorPickerProps, IColorPickerState> implements IColorPicker {
    static defaultProps: Partial<IColorPickerProps>;
    private _textChangeHandlers;
    /**
     * Strings displayed in the UI as text field labels (these are in a separate object for convenient
     * indexing by short color component name).
     */
    private _textLabels;
    /** Strings besides red/green/blue/alpha/hex */
    private _strings;
    constructor(props: IColorPickerProps);
    readonly color: IColor;
    componentDidUpdate(prevProps: Readonly<IColorPickerProps>, prevState: Readonly<IColorPickerState>): void;
    render(): JSX.Element;
    private _getDisplayValue;
    private _onSVChanged;
    private _onHChanged;
    private _onAChanged;
    private _onTextChange;
    private _onBlur;
    /**
     * Update the displayed color and call change handlers if appropriate.
     * @param ev - Event if call was triggered by an event (undefined if triggered by props change)
     * @param newColor - Updated color
     */
    private _updateColor;
}
export {};
