import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { TeachingBubble } from 'office-ui-fabric-react/lib/TeachingBubble';
import { DirectionalHint } from 'office-ui-fabric-react/lib/Callout';
var TeachingBubbleWideExample = /** @class */ (function (_super) {
    __extends(TeachingBubbleWideExample, _super);
    function TeachingBubbleWideExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onDismiss = _this._onDismiss.bind(_this);
        _this._onShow = _this._onShow.bind(_this);
        _this.state = {
            isTeachingBubbleVisible: false,
        };
        return _this;
    }
    TeachingBubbleWideExample.prototype.render = function () {
        var _this = this;
        var isTeachingBubbleVisible = this.state.isTeachingBubbleVisible;
        return (React.createElement("div", { className: "ms-TeachingBubbleExample" },
            React.createElement("span", { className: "ms-TeachingBubbleBasicExample-buttonArea", ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                React.createElement(DefaultButton, { onClick: isTeachingBubbleVisible ? this._onDismiss : this._onShow, text: isTeachingBubbleVisible ? 'Hide TeachingBubble' : 'Show TeachingBubble' })),
            isTeachingBubbleVisible ? (React.createElement("div", null,
                React.createElement(TeachingBubble, { calloutProps: { directionalHint: DirectionalHint.bottomCenter }, isWide: true, hasCloseButton: true, closeButtonAriaLabel: "Close", target: this._menuButtonElement, onDismiss: this._onDismiss, headline: "Discover what\u2019s trending around you" }, "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nulla, ipsum? Molestiae quis aliquam magni harum non?"))) : null));
    };
    TeachingBubbleWideExample.prototype._onDismiss = function (ev) {
        this.setState({
            isTeachingBubbleVisible: false,
        });
    };
    TeachingBubbleWideExample.prototype._onShow = function (ev) {
        this.setState({
            isTeachingBubbleVisible: true,
        });
    };
    return TeachingBubbleWideExample;
}(React.Component));
export { TeachingBubbleWideExample };
//# sourceMappingURL=TeachingBubble.Wide.Example.js.map