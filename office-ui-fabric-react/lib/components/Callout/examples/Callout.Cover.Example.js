import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton, Callout, DirectionalHint, Dropdown, getTheme, mergeStyleSets, FontWeights, } from 'office-ui-fabric-react';
var DIRECTION_OPTIONS = [
    { key: DirectionalHint.topLeftEdge, text: 'Top Left Edge' },
    { key: DirectionalHint.topCenter, text: 'Top Center' },
    { key: DirectionalHint.topRightEdge, text: 'Top Right Edge' },
    { key: DirectionalHint.topAutoEdge, text: 'Top Auto Edge' },
    { key: DirectionalHint.bottomLeftEdge, text: 'Bottom Left Edge' },
    { key: DirectionalHint.bottomCenter, text: 'Bottom Center' },
    { key: DirectionalHint.bottomRightEdge, text: 'Bottom Right Edge' },
    { key: DirectionalHint.bottomAutoEdge, text: 'Bottom Auto Edge' },
    { key: DirectionalHint.leftTopEdge, text: 'Left Top Edge' },
    { key: DirectionalHint.leftCenter, text: 'Left Center' },
    { key: DirectionalHint.leftBottomEdge, text: 'Left Bottom Edge' },
    { key: DirectionalHint.rightTopEdge, text: 'Right Top Edge' },
    { key: DirectionalHint.rightCenter, text: 'Right Center' },
    { key: DirectionalHint.rightBottomEdge, text: 'Right Bottom Edge' },
];
var theme = getTheme();
var styles = mergeStyleSets({
    buttonArea: {
        verticalAlign: 'top',
        display: 'inline-block',
        textAlign: 'center',
        margin: '0 100px',
        minWidth: 130,
        height: 32,
    },
    configArea: {
        minWidth: '300px',
        display: 'inline-block',
    },
    callout: {
        maxWidth: 300,
    },
    header: {
        padding: '18px 24px 12px',
    },
    title: [
        theme.fonts.xLarge,
        {
            margin: 0,
            fontWeight: FontWeights.semilight,
        },
    ],
    inner: {
        height: '100%',
        padding: '0 24px 20px',
    },
});
var CalloutCoverExample = /** @class */ (function (_super) {
    __extends(CalloutCoverExample, _super);
    function CalloutCoverExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onDismiss = _this._onDismiss.bind(_this);
        _this._onShowMenuClicked = _this._onShowMenuClicked.bind(_this);
        _this._onDirectionalChanged = _this._onDirectionalChanged.bind(_this);
        _this.state = {
            isCalloutVisible: false,
            directionalHint: DirectionalHint.bottomLeftEdge,
        };
        return _this;
    }
    CalloutCoverExample.prototype.render = function () {
        var _this = this;
        var _a = this.state, isCalloutVisible = _a.isCalloutVisible, directionalHint = _a.directionalHint;
        // ms-Callout-smallbeak is used in this directional example to reflect all the positions.
        // Large beak will disable some position to avoid beak over the callout edge.
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: styles.configArea },
                React.createElement(Dropdown, { label: "Directional hint", selectedKey: directionalHint, options: DIRECTION_OPTIONS, onChange: this._onDirectionalChanged })),
            React.createElement("div", { className: styles.buttonArea, ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                React.createElement(DefaultButton, { text: isCalloutVisible ? 'Hide callout' : 'Show callout', onClick: this._onShowMenuClicked })),
            isCalloutVisible ? (React.createElement(Callout, { className: styles.callout, onDismiss: this._onDismiss, target: this._menuButtonElement, directionalHint: directionalHint, coverTarget: true, isBeakVisible: false, gapSpace: 0, setInitialFocus: true },
                React.createElement("div", { className: styles.header },
                    React.createElement("p", { className: styles.title }, "I'm covering the target!")),
                React.createElement("div", { className: styles.inner },
                    React.createElement(DefaultButton, { onClick: this._onShowMenuClicked, text: "Click to dismiss" })))) : null));
    };
    CalloutCoverExample.prototype._onDismiss = function () {
        this.setState({ isCalloutVisible: false });
    };
    CalloutCoverExample.prototype._onShowMenuClicked = function () {
        this.setState({
            isCalloutVisible: !this.state.isCalloutVisible,
        });
    };
    CalloutCoverExample.prototype._onDirectionalChanged = function (event, option) {
        this.setState({
            directionalHint: option.key,
        });
    };
    return CalloutCoverExample;
}(React.Component));
export { CalloutCoverExample };
//# sourceMappingURL=Callout.Cover.Example.js.map