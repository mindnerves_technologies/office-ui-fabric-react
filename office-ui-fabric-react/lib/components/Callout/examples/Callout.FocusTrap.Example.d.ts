import * as React from 'react';
export interface ICalloutFocusTrapExampleState {
    isCalloutVisible: boolean;
}
export declare class CalloutFocusTrapExample extends React.Component<{}, ICalloutFocusTrapExampleState> {
    state: ICalloutFocusTrapExampleState;
    private _menuButtonElement;
    render(): JSX.Element;
    private _onDismiss;
}
