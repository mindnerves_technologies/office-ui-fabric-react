import * as React from 'react';
export interface IStatusCalloutExampleState {
    isCalloutVisible?: boolean;
}
export declare class StatusCalloutExample extends React.Component<{}, IStatusCalloutExampleState> {
    state: IStatusCalloutExampleState;
    private _menuButtonElement;
    render(): JSX.Element;
    private _onShowMenuClicked;
    private _onCalloutDismiss;
}
