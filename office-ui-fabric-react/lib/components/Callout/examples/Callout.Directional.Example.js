import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton, Callout, DirectionalHint, Dropdown, Checkbox, Slider, getTheme, mergeStyleSets, FontWeights, Link, getId, } from 'office-ui-fabric-react';
var DIRECTION_OPTIONS = [
    { key: DirectionalHint.topLeftEdge, text: 'Top Left Edge' },
    { key: DirectionalHint.topCenter, text: 'Top Center' },
    { key: DirectionalHint.topRightEdge, text: 'Top Right Edge' },
    { key: DirectionalHint.topAutoEdge, text: 'Top Auto Edge' },
    { key: DirectionalHint.bottomLeftEdge, text: 'Bottom Left Edge' },
    { key: DirectionalHint.bottomCenter, text: 'Bottom Center' },
    { key: DirectionalHint.bottomRightEdge, text: 'Bottom Right Edge' },
    { key: DirectionalHint.bottomAutoEdge, text: 'Bottom Auto Edge' },
    { key: DirectionalHint.leftTopEdge, text: 'Left Top Edge' },
    { key: DirectionalHint.leftCenter, text: 'Left Center' },
    { key: DirectionalHint.leftBottomEdge, text: 'Left Bottom Edge' },
    { key: DirectionalHint.rightTopEdge, text: 'Right Top Edge' },
    { key: DirectionalHint.rightCenter, text: 'Right Center' },
    { key: DirectionalHint.rightBottomEdge, text: 'Right Bottom Edge' },
];
var theme = getTheme();
var styles = mergeStyleSets({
    buttonArea: {
        verticalAlign: 'top',
        display: 'inline-block',
        textAlign: 'center',
        margin: '0 100px',
        minWidth: 130,
        height: 32,
    },
    configArea: {
        minWidth: '300px',
        display: 'inline-block',
    },
    callout: {
        maxWidth: 300,
    },
    calloutExampleButton: {
        width: '100%',
    },
    header: {
        padding: '18px 24px 12px',
    },
    title: [
        theme.fonts.xLarge,
        {
            margin: 0,
            fontWeight: FontWeights.semilight,
        },
    ],
    inner: {
        height: '100%',
        padding: '0 24px 20px',
    },
    subtext: [
        theme.fonts.small,
        {
            margin: 0,
            fontWeight: FontWeights.semilight,
        },
    ],
    link: [
        theme.fonts.medium,
        {
            color: theme.palette.neutralPrimary,
        },
    ],
    actions: {
        position: 'relative',
        marginTop: 20,
        width: '100%',
        whiteSpace: 'nowrap',
    },
});
var CalloutDirectionalExample = /** @class */ (function (_super) {
    __extends(CalloutDirectionalExample, _super);
    function CalloutDirectionalExample(props) {
        var _this = _super.call(this, props) || this;
        // Use getId() to ensure that the callout label and description IDs are unique on the page.
        // It's also okay, though not recommended, to use plain strings without getId() and manually ensure their uniqueness.
        _this._labelId = getId('callout-label');
        _this._descriptionId = getId('callout-description');
        _this._onCalloutDismiss = function () {
            _this.setState({
                isCalloutVisible: false,
            });
        };
        _this._onShowMenuClicked = function () {
            _this.setState({
                isCalloutVisible: !_this.state.isCalloutVisible,
            });
        };
        _this._onShowBeakChange = function (ev, isVisible) {
            _this.setState({
                isBeakVisible: isVisible,
                beakWidth: 10,
            });
        };
        _this._onDirectionalChanged = function (event, option) {
            _this.setState({
                directionalHint: option.key,
            });
        };
        _this._onGapSlider = function (value) {
            _this.setState({
                gapSpace: value,
            });
        };
        _this._onBeakWidthSlider = function (value) {
            _this.setState({
                beakWidth: value,
            });
        };
        _this.state = {
            isCalloutVisible: false,
            isBeakVisible: true,
            directionalHint: DirectionalHint.bottomLeftEdge,
        };
        return _this;
    }
    CalloutDirectionalExample.prototype.render = function () {
        var _this = this;
        var _a = this.state, isCalloutVisible = _a.isCalloutVisible, isBeakVisible = _a.isBeakVisible, directionalHint = _a.directionalHint, gapSpace = _a.gapSpace, beakWidth = _a.beakWidth;
        //  ms-Callout-smallbeak is used in this directional example to reflect all the positions.
        //  Large beak will disable some position to avoid beak over the callout edge.
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: styles.configArea },
                React.createElement(Checkbox, { styles: { root: { margin: '10px 0' } }, label: "Show beak", checked: isBeakVisible, onChange: this._onShowBeakChange }),
                React.createElement(Slider, { max: 30, label: "Gap Space", min: 0, defaultValue: 0, onChange: this._onGapSlider }),
                isBeakVisible && (React.createElement(Slider, { max: 50, label: "Beak Width", min: 10, defaultValue: 16, onChange: this._onBeakWidthSlider })),
                React.createElement(Dropdown, { label: "Directional hint", selectedKey: directionalHint, options: DIRECTION_OPTIONS, onChange: this._onDirectionalChanged })),
            React.createElement("div", { className: styles.buttonArea, ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                React.createElement(DefaultButton, { className: styles.calloutExampleButton, onClick: this._onShowMenuClicked, text: isCalloutVisible ? 'Hide callout' : 'Show callout' })),
            isCalloutVisible ? (React.createElement(Callout, { ariaLabelledBy: this._labelId, ariaDescribedBy: this._descriptionId, className: styles.callout, gapSpace: gapSpace, target: this._menuButtonElement, isBeakVisible: isBeakVisible, beakWidth: beakWidth, onDismiss: this._onCalloutDismiss, directionalHint: directionalHint, setInitialFocus: true },
                React.createElement("div", { className: styles.header },
                    React.createElement("p", { className: styles.title, id: this._labelId }, "All of your favorite people")),
                React.createElement("div", { className: styles.inner },
                    React.createElement("p", { className: styles.subtext, id: this._descriptionId }, "Message body is optional. If help documentation is available, consider adding a link to learn more at the bottom."),
                    React.createElement("div", { className: styles.actions },
                        React.createElement(Link, { className: styles.link, href: "http://microsoft.com", target: "_blank" }, "Go to Microsoft"))))) : null));
    };
    return CalloutDirectionalExample;
}(React.Component));
export { CalloutDirectionalExample };
//# sourceMappingURL=Callout.Directional.Example.js.map