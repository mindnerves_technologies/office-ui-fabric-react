import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton, getTheme, FontWeights, mergeStyleSets, DelayedRender, Callout } from 'office-ui-fabric-react';
// Themed styles for the example.
var theme = getTheme();
var styles = mergeStyleSets({
    buttonArea: {
        verticalAlign: 'top',
        display: 'inline-block',
        textAlign: 'center',
        margin: '0 100px',
        minWidth: 130,
        height: 32,
    },
    callout: {
        maxWidth: 300,
    },
    subtext: [
        theme.fonts.small,
        {
            margin: 0,
            height: '100%',
            padding: '24px 20px',
            fontWeight: FontWeights.semilight,
        },
    ],
});
// Example code
var StatusCalloutExample = /** @class */ (function (_super) {
    __extends(StatusCalloutExample, _super);
    function StatusCalloutExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            isCalloutVisible: false,
        };
        _this._menuButtonElement = React.createRef();
        _this._onShowMenuClicked = function () {
            _this.setState({
                isCalloutVisible: !_this.state.isCalloutVisible,
            });
        };
        _this._onCalloutDismiss = function () {
            _this.setState({
                isCalloutVisible: false,
            });
        };
        return _this;
    }
    StatusCalloutExample.prototype.render = function () {
        var isCalloutVisible = this.state.isCalloutVisible;
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: styles.buttonArea, ref: this._menuButtonElement },
                React.createElement(DefaultButton, { onClick: this._onShowMenuClicked, text: isCalloutVisible ? 'Hide StatusCallout' : 'Show StatusCallout' })),
            this.state.isCalloutVisible && (React.createElement(Callout, { className: styles.callout, target: this._menuButtonElement.current, onDismiss: this._onCalloutDismiss, role: "status", "aria-live": "assertive" },
                React.createElement(DelayedRender, null,
                    React.createElement(React.Fragment, null,
                        React.createElement("p", { className: styles.subtext }, "This message is treated as an aria-live assertive status message, and will be read by a screen reader regardless of focus.")))))));
    };
    return StatusCalloutExample;
}(React.Component));
export { StatusCalloutExample };
//# sourceMappingURL=Callout.Status.Example.js.map