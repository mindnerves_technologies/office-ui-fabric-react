import { __assign, __extends } from "tslib";
import * as React from 'react';
import { DocumentCard, DocumentCardActions, DocumentCardActivity, DocumentCardLocation, DocumentCardPreview, DocumentCardTitle, } from 'office-ui-fabric-react/lib/DocumentCard';
import { ImageFit } from 'office-ui-fabric-react/lib/Image';
import { TestImages } from '@uifabric/example-data';
var DocumentCardCompleteExample = /** @class */ (function (_super) {
    __extends(DocumentCardCompleteExample, _super);
    function DocumentCardCompleteExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DocumentCardCompleteExample.prototype.render = function () {
        var previewProps = {
            getOverflowDocumentCountText: function (overflowCount) { return "+" + overflowCount + " more"; },
            previewImages: [
                {
                    name: '2016 Conference Presentation',
                    linkProps: {
                        href: 'http://bing.com',
                        target: '_blank',
                    },
                    previewImageSrc: TestImages.documentPreview,
                    iconSrc: TestImages.iconPpt,
                    imageFit: ImageFit.cover,
                    width: 318,
                    height: 196,
                },
                {
                    name: 'New Contoso Collaboration for Conference Presentation Draft',
                    linkProps: {
                        href: 'http://bing.com',
                        target: '_blank',
                    },
                    previewImageSrc: TestImages.documentPreviewTwo,
                    iconSrc: TestImages.iconPpt,
                    imageFit: ImageFit.cover,
                    width: 318,
                    height: 196,
                },
                {
                    name: 'Spec Sheet for design',
                    linkProps: {
                        href: 'http://bing.com',
                        target: '_blank',
                    },
                    previewImageSrc: TestImages.documentPreviewThree,
                    iconSrc: TestImages.iconPpt,
                    imageFit: ImageFit.cover,
                    width: 318,
                    height: 196,
                },
                {
                    name: 'Contoso Marketing Presentation',
                    linkProps: {
                        href: 'http://bing.com',
                        target: '_blank',
                    },
                    previewImageSrc: TestImages.documentPreview,
                    iconSrc: TestImages.iconPpt,
                    imageFit: ImageFit.cover,
                    width: 318,
                    height: 196,
                },
                {
                    name: 'Notes from Ignite conference',
                    linkProps: {
                        href: 'http://bing.com',
                        target: '_blank',
                    },
                    previewImageSrc: TestImages.documentPreviewTwo,
                    iconSrc: TestImages.iconPpt,
                    imageFit: ImageFit.cover,
                    width: 318,
                    height: 196,
                },
                {
                    name: 'FY17 Cost Projections',
                    linkProps: {
                        href: 'http://bing.com',
                        target: '_blank',
                    },
                    previewImageSrc: TestImages.documentPreviewThree,
                    iconSrc: TestImages.iconPpt,
                    imageFit: ImageFit.cover,
                    width: 318,
                    height: 196,
                },
            ],
        };
        return (React.createElement(DocumentCard, { "aria-label": "Document Card with multiple items, commands and views. Marketing documents. 6 files were uploaded.\n        Created by Annie Lindqvist in February 23, 2016. 432 views.", onClick: this._onClick },
            React.createElement(DocumentCardPreview, __assign({}, previewProps)),
            React.createElement(DocumentCardLocation, { location: "Marketing Documents", locationHref: "http://microsoft.com", ariaLabel: "Location, Marketing Documents" }),
            React.createElement(DocumentCardTitle, { title: "6 files were uploaded" }),
            React.createElement(DocumentCardActivity, { activity: "Created Feb 23, 2016", people: [{ name: 'Annie Lindqvist', profileImageSrc: TestImages.personaFemale }] }),
            React.createElement(DocumentCardActions, { actions: [
                    {
                        iconProps: { iconName: 'Share' },
                        onClick: this._onActionClick.bind(this, 'share'),
                        ariaLabel: 'share action',
                    },
                    {
                        iconProps: { iconName: 'Pin' },
                        onClick: this._onActionClick.bind(this, 'pin'),
                        ariaLabel: 'pin action',
                    },
                    {
                        iconProps: { iconName: 'Ringer' },
                        onClick: this._onActionClick.bind(this, 'notifications'),
                        ariaLabel: 'notifications action',
                    },
                ], views: 432 })));
    };
    DocumentCardCompleteExample.prototype._onActionClick = function (action, ev) {
        console.log("You clicked the " + action + " action");
        ev.stopPropagation();
        ev.preventDefault();
    };
    DocumentCardCompleteExample.prototype._onClick = function () {
        console.log('You clicked the card.');
    };
    return DocumentCardCompleteExample;
}(React.PureComponent));
export { DocumentCardCompleteExample };
//# sourceMappingURL=DocumentCard.Complete.Example.js.map