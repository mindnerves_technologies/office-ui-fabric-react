var COMMAND_BAR_HEIGHT = 44;
export var getStyles = function (props) {
    var className = props.className, theme = props.theme;
    var semanticColors = theme.semanticColors;
    return {
        root: [
            theme.fonts.medium,
            'ms-CommandBar',
            {
                display: 'flex',
                backgroundColor: semanticColors.bodyBackground,
                padding: '0 14px 0 24px',
                height: COMMAND_BAR_HEIGHT,
            },
            className,
        ],
        primarySet: [
            'ms-CommandBar-primaryCommand',
            {
                flexGrow: '1',
                display: 'flex',
                alignItems: 'stretch',
            },
        ],
        secondarySet: [
            'ms-CommandBar-secondaryCommand',
            {
                flexShrink: '0',
                display: 'flex',
                alignItems: 'stretch',
            },
        ],
    };
};
//# sourceMappingURL=CommandBar.styles.js.map