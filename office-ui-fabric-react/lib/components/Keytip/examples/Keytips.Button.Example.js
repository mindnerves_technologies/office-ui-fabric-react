import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton, CompoundButton } from 'office-ui-fabric-react/lib/Button';
import { keytipMap } from 'office-ui-fabric-react/lib/components/Keytip/examples/KeytipSetup';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
var stackTokens = { childrenGap: 20 };
var marginBottom = { root: { marginBottom: 28 } };
var KeytipsButtonExample = /** @class */ (function (_super) {
    __extends(KeytipsButtonExample, _super);
    function KeytipsButtonExample(props) {
        var _this = _super.call(this, props) || this;
        _this._toggleDisabled = function (ev, checked) {
            _this.setState({ btnDisabled: !checked });
        };
        _this.state = {
            btnDisabled: false,
        };
        return _this;
    }
    /* tslint:disable:jsx-no-lambda */
    KeytipsButtonExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("p", null, "When multiple Keytips start with the same character, typing that character will filter the visible keytips."),
            React.createElement(Stack, { horizontal: true, tokens: stackTokens },
                React.createElement(DefaultButton, { keytipProps: keytipMap.Button, text: "Button", onClick: function () {
                        console.log('Button');
                    } }),
                React.createElement(CompoundButton, { styles: marginBottom, keytipProps: keytipMap.CompoundButton, text: "Compound Button", secondaryText: 'With a Keytip', onClick: function () {
                        console.log('Compound Button');
                    } }),
                React.createElement(DefaultButton, { keytipProps: keytipMap.ButtonWithMenu, text: "Button with Menu", onClick: function () {
                        console.log('Button with Menu');
                    }, menuProps: {
                        items: [
                            {
                                key: 'buttonMenuItem1',
                                text: 'Menu Item 1',
                                keytipProps: keytipMap.ButtonMenuItem1,
                            },
                            {
                                key: 'buttonMenuItem2',
                                text: 'Menu Item 2',
                                keytipProps: keytipMap.ButtonMenuItem2,
                            },
                        ],
                    } }),
                React.createElement(DefaultButton, { styles: { splitButtonContainer: { height: 32 } }, keytipProps: keytipMap.SplitButton, text: "Split Button", split: true, onClick: function () {
                        console.log('Split Button');
                    }, menuProps: {
                        items: [
                            {
                                key: 'splitButtonMenuButton1',
                                text: 'Split Button Menu Item 1',
                                keytipProps: keytipMap.SplitButtonMenuItem1,
                            },
                            {
                                key: 'splitButtonMenuButton2',
                                text: 'Split Button Menu Item 2',
                                keytipProps: keytipMap.SplitButtonMenuItem2,
                            },
                        ],
                    } }),
                React.createElement(DefaultButton, { text: "I do not have a keytip" })),
            React.createElement("p", null, "The 'offset' prop can be used to position the keytip a set distance from the top-left corner of the element."),
            React.createElement(DefaultButton, { styles: marginBottom, keytipProps: keytipMap.ButtonOffset, text: "Button keytip offset 10x10", onClick: function () {
                    console.log('Button');
                } }),
            React.createElement("p", null, "When a Keytip's corresponding component is disabled, the keytip still appears but cannot be triggered."),
            React.createElement(Toggle, { onText: 'Enabled', offText: 'Disabled', defaultChecked: !this.state.btnDisabled, onChange: this._toggleDisabled }),
            React.createElement(DefaultButton, { keytipProps: keytipMap.DisabledButton, disabled: this.state.btnDisabled, text: (this.state.btnDisabled ? 'Disabled' : 'Enabled') + ' Button', onClick: function () {
                    console.log('Disabled Button');
                } })));
    };
    return KeytipsButtonExample;
}(React.Component));
export { KeytipsButtonExample };
//# sourceMappingURL=Keytips.Button.Example.js.map