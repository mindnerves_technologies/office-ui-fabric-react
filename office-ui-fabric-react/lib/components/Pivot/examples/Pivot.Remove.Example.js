import { __extends } from "tslib";
import * as React from 'react';
import { Pivot, PivotItem, PivotLinkFormat, PivotLinkSize } from 'office-ui-fabric-react/lib/Pivot';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Label } from 'office-ui-fabric-react/lib/Label';
var PivotRemoveExample = /** @class */ (function (_super) {
    __extends(PivotRemoveExample, _super);
    function PivotRemoveExample(props) {
        var _this = _super.call(this, props) || this;
        _this._shouldShowFirstPivotItem = true;
        _this.state = {
            shouldShowFirstPivotItem: true,
        };
        _this._handleClick = _this._handleClick.bind(_this);
        return _this;
    }
    PivotRemoveExample.prototype.render = function () {
        var pivotArray = [];
        if (this.state.shouldShowFirstPivotItem) {
            pivotArray.push(React.createElement(PivotItem, { headerText: "Foo", itemKey: "Foo", key: "Foo" },
                React.createElement(Label, null, "Click the button below to show/hide this pivot item."),
                React.createElement(Label, null, "The selected item will not change when the number of pivot items changes."),
                React.createElement(Label, null, "If the selected item was removed, the new first item will be selected.")));
        }
        pivotArray = pivotArray.concat(React.createElement(PivotItem, { headerText: "Bar", itemKey: "Bar", key: "Bar" },
            React.createElement(Label, null, "Pivot #2")), React.createElement(PivotItem, { headerText: "Bas", itemKey: "Bas", key: "Bas" },
            React.createElement(Label, null, "Pivot #3")), React.createElement(PivotItem, { headerText: "Biz", itemKey: "Biz", key: "Biz" },
            React.createElement(Label, null, "Pivot #4")));
        return (React.createElement("div", null,
            React.createElement(Pivot, { "aria-label": "Remove Pivot Example", linkSize: PivotLinkSize.large, linkFormat: PivotLinkFormat.tabs }, pivotArray),
            React.createElement("div", null,
                React.createElement(DefaultButton, { "aria-live": "polite", onClick: this._handleClick, text: (this.state.shouldShowFirstPivotItem ? 'Hide' : 'Show') + " First Pivot Item" }))));
    };
    PivotRemoveExample.prototype._handleClick = function () {
        this._shouldShowFirstPivotItem = !this._shouldShowFirstPivotItem;
        this.setState({
            shouldShowFirstPivotItem: this._shouldShowFirstPivotItem,
        });
    };
    return PivotRemoveExample;
}(React.Component));
export { PivotRemoveExample };
//# sourceMappingURL=Pivot.Remove.Example.js.map