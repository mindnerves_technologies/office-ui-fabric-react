import { __extends } from "tslib";
import * as React from 'react';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Pivot, PivotItem } from 'office-ui-fabric-react/lib/Pivot';
var PivotOverrideExample = /** @class */ (function (_super) {
    __extends(PivotOverrideExample, _super);
    function PivotOverrideExample(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            selectedKey: 0,
        };
        _this._handleClick = _this._handleClick.bind(_this);
        return _this;
    }
    PivotOverrideExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(Pivot, { "aria-label": "Override Selected Item Pivot Example", selectedKey: "" + this.state.selectedKey },
                React.createElement(PivotItem, { headerText: "My Files", itemKey: "0" },
                    React.createElement(Label, null, "Pivot #1")),
                React.createElement(PivotItem, { headerText: "Recent", itemKey: "1" },
                    React.createElement(Label, null, "Pivot #2")),
                React.createElement(PivotItem, { headerText: "Shared with me", itemKey: "2" },
                    React.createElement(Label, null, "Pivot #3"))),
            React.createElement(DefaultButton, { onClick: this._handleClick }, "Select next item")));
    };
    PivotOverrideExample.prototype._handleClick = function () {
        this.setState({ selectedKey: (this.state.selectedKey + 1) % 3 });
    };
    return PivotOverrideExample;
}(React.Component));
export { PivotOverrideExample };
//# sourceMappingURL=Pivot.Override.Example.js.map