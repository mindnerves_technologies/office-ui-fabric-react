import { __extends } from "tslib";
import * as React from 'react';
import { Pivot, PivotItem } from 'office-ui-fabric-react/lib/Pivot';
var PivotSeparateExample = /** @class */ (function (_super) {
    __extends(PivotSeparateExample, _super);
    function PivotSeparateExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { selectedKey: 'rectangleRed' };
        _this._handleLinkClick = function (item) {
            _this.setState({
                selectedKey: item.props.itemKey,
            });
        };
        _this._getTabId = function (itemKey) {
            return "ShapeColorPivot_" + itemKey;
        };
        return _this;
    }
    PivotSeparateExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("div", { "aria-labelledby": this._getTabId(this.state.selectedKey), role: "tabpanel", style: {
                    float: 'left',
                    width: 100,
                    height: this.state.selectedKey === 'squareRed' ? 100 : 200,
                    background: this.state.selectedKey === 'rectangleGreen' ? 'green' : 'red',
                } }),
            React.createElement(Pivot, { "aria-label": "Separately Rendered Content Pivot Example", selectedKey: this.state.selectedKey, onLinkClick: this._handleLinkClick, headersOnly: true, getTabId: this._getTabId },
                React.createElement(PivotItem, { headerText: "Rectangle red", itemKey: "rectangleRed" }),
                React.createElement(PivotItem, { headerText: "Square red", itemKey: "squareRed" }),
                React.createElement(PivotItem, { headerText: "Rectangle green", itemKey: "rectangleGreen" }))));
    };
    return PivotSeparateExample;
}(React.Component));
export { PivotSeparateExample };
//# sourceMappingURL=Pivot.Separate.Example.js.map