import { __extends } from "tslib";
import * as React from 'react';
import { Pivot, PivotItem, PivotLinkFormat, PivotLinkSize } from 'office-ui-fabric-react/lib/Pivot';
import { Label } from 'office-ui-fabric-react/lib/Label';
var PivotOnChangeExample = /** @class */ (function (_super) {
    __extends(PivotOnChangeExample, _super);
    function PivotOnChangeExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PivotOnChangeExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(Pivot, { "aria-label": "OnChange Pivot Example", linkSize: PivotLinkSize.large, linkFormat: PivotLinkFormat.tabs, onLinkClick: this.onLinkClick },
                React.createElement(PivotItem, { headerText: "Foo" },
                    React.createElement(Label, null, "Pivot #1")),
                React.createElement(PivotItem, { headerText: "Bar" },
                    React.createElement(Label, null, "Pivot #2")),
                React.createElement(PivotItem, { headerText: "Bas" },
                    React.createElement(Label, null, "Pivot #3")),
                React.createElement(PivotItem, { headerText: "Biz" },
                    React.createElement(Label, null, "Pivot #4")))));
    };
    PivotOnChangeExample.prototype.onLinkClick = function (item) {
        alert(item.props.headerText);
    };
    return PivotOnChangeExample;
}(React.Component));
export { PivotOnChangeExample };
//# sourceMappingURL=Pivot.OnChange.Example.js.map