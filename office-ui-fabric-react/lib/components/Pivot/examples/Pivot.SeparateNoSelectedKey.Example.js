import { __assign, __extends } from "tslib";
import * as React from 'react';
import { Pivot, PivotItem } from 'office-ui-fabric-react/lib/Pivot';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
var PivotSeparateNoSelectedKeyExample = /** @class */ (function (_super) {
    __extends(PivotSeparateNoSelectedKeyExample, _super);
    function PivotSeparateNoSelectedKeyExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { selectedKey: 'Settings' };
        _this._handleSettingsIconClick = function () { return _this.setState({ selectedKey: 'Settings' }); };
        _this._handleLinkClick = function (item) {
            _this.setState({
                selectedKey: item.props.itemKey,
            });
        };
        _this._getTabId = function (itemKey) {
            return "ShapeColorPivot_" + itemKey;
        };
        return _this;
    }
    PivotSeparateNoSelectedKeyExample.prototype.render = function () {
        var pivotItems = { Thing1: React.createElement("div", null, "thing 1"), Thing2: React.createElement("div", null, "thing 2"), Thing3: React.createElement("div", null, "thing 3") };
        var items = __assign(__assign({}, pivotItems), { Settings: React.createElement("div", null, "settings") });
        return (React.createElement("div", { style: { width: '50%', maxWidth: '500px' } },
            "Current selectedKey: ",
            this.state.selectedKey || 'null',
            React.createElement("div", { style: {
                    display: 'flex',
                    alignItems: 'center',
                } },
                React.createElement(Pivot, { "aria-label": "No Selected Pivot Example", style: { flexGrow: 1 }, selectedKey: Object.keys(pivotItems).indexOf(this.state.selectedKey) >= 0 ? this.state.selectedKey : null, onLinkClick: this._handleLinkClick, headersOnly: true, getTabId: this._getTabId }, Object.keys(pivotItems).map(function (name) { return (React.createElement(PivotItem, { key: "pivotItemKey_" + name, headerText: name, itemKey: name })); })),
                React.createElement(DefaultButton, { iconProps: {
                        iconName: 'Settings',
                        style: { color: this.state.selectedKey === 'Settings' ? 'blue' : 'black' },
                    }, onClick: this._handleSettingsIconClick, text: "Settings" })),
            items[this.state.selectedKey]));
    };
    return PivotSeparateNoSelectedKeyExample;
}(React.Component));
export { PivotSeparateNoSelectedKeyExample };
//# sourceMappingURL=Pivot.SeparateNoSelectedKey.Example.js.map