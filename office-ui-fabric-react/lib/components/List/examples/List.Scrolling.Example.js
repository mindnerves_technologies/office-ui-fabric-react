import { __extends } from "tslib";
import * as React from 'react';
import { FocusZone, FocusZoneDirection } from 'office-ui-fabric-react/lib/FocusZone';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { List, ScrollToMode } from 'office-ui-fabric-react/lib/List';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import { createListItems } from '@uifabric/example-data';
import { mergeStyleSets, getTheme, normalize } from 'office-ui-fabric-react/lib/Styling';
var theme = getTheme();
var styles = mergeStyleSets({
    container: {
        overflow: 'auto',
        maxHeight: 500,
        marginTop: 20,
        selectors: {
            '.ms-List-cell:nth-child(odd)': {
                height: 50,
                lineHeight: 50,
                background: theme.palette.neutralLighter,
            },
            '.ms-List-cell:nth-child(even)': {
                height: 25,
                lineHeight: 25,
            },
        },
    },
    itemContent: [
        theme.fonts.medium,
        normalize,
        {
            position: 'relative',
            display: 'block',
            borderLeft: '3px solid ' + theme.palette.themePrimary,
            paddingLeft: 27,
        },
    ],
});
var evenItemHeight = 25;
var oddItemHeight = 50;
var numberOfItemsOnPage = 10;
var ListScrollingExample = /** @class */ (function (_super) {
    __extends(ListScrollingExample, _super);
    function ListScrollingExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onChangeText = function (ev, value) {
            _this._scroll(parseInt(value, 10) || 0, _this.state.scrollToMode);
        };
        _this._onDropdownChange = function (event, option) {
            var scrollMode = _this.state.scrollToMode;
            switch (option.key) {
                case 'auto':
                    scrollMode = ScrollToMode.auto;
                    break;
                case 'top':
                    scrollMode = ScrollToMode.top;
                    break;
                case 'bottom':
                    scrollMode = ScrollToMode.bottom;
                    break;
                case 'center':
                    scrollMode = ScrollToMode.center;
                    break;
            }
            _this._scroll(_this.state.selectedIndex, scrollMode);
        };
        _this._onRenderCell = function (item, index) {
            return (React.createElement("div", { "data-is-focusable": true },
                React.createElement("div", { className: styles.itemContent },
                    index,
                    " \u00A0 ",
                    item.name)));
        };
        _this._scrollRelative = function (delta) {
            return function () {
                _this._scroll(_this.state.selectedIndex + delta, _this.state.scrollToMode);
            };
        };
        _this._scroll = function (index, scrollToMode) {
            var updatedSelectedIndex = Math.min(Math.max(index, 0), _this._items.length - 1);
            _this.setState({
                selectedIndex: updatedSelectedIndex,
                scrollToMode: scrollToMode,
            }, function () {
                _this._list.scrollToIndex(updatedSelectedIndex, function (idx) { return (idx % 2 === 0 ? evenItemHeight : oddItemHeight); }, scrollToMode);
            });
        };
        _this._resolveList = function (list) {
            _this._list = list;
        };
        _this._onShowItemIndexInViewChanged = function (event, checked) {
            _this.setState({
                showItemIndexInView: checked,
            });
        };
        _this._items = props.items || createListItems(5000);
        _this.state = {
            selectedIndex: 0,
            scrollToMode: ScrollToMode.auto,
            showItemIndexInView: false,
        };
        return _this;
    }
    ListScrollingExample.prototype.render = function () {
        return (React.createElement(FocusZone, { direction: FocusZoneDirection.vertical },
            React.createElement("div", null,
                React.createElement(DefaultButton, { onClick: this._scrollRelative(-10) }, "-10"),
                React.createElement(DefaultButton, { onClick: this._scrollRelative(-1) }, "-1"),
                React.createElement(DefaultButton, { onClick: this._scrollRelative(1) }, "+1"),
                React.createElement(DefaultButton, { onClick: this._scrollRelative(10) }, "+10")),
            React.createElement(Dropdown, { placeholder: "Select an Option", label: "Scroll To Mode:", ariaLabel: "Scroll To Mode", defaultSelectedKey: 'auto', options: [
                    { key: 'auto', text: 'Auto' },
                    { key: 'top', text: 'Top' },
                    { key: 'bottom', text: 'Bottom' },
                    { key: 'center', text: 'Center' },
                ], onChange: this._onDropdownChange }),
            React.createElement("div", null,
                "Scroll item index:",
                React.createElement(TextField, { value: this.state.selectedIndex.toString(10), onChange: this._onChangeText })),
            React.createElement("div", null,
                React.createElement(Checkbox, { label: "Show index of the first item in view when unmounting", checked: this.state.showItemIndexInView, onChange: this._onShowItemIndexInViewChanged })),
            React.createElement("div", { className: styles.container, "data-is-scrollable": true },
                React.createElement(List, { ref: this._resolveList, items: this._items, getPageHeight: this._getPageHeight, onRenderCell: this._onRenderCell }))));
    };
    ListScrollingExample.prototype.componentWillUnmount = function () {
        if (this.state.showItemIndexInView) {
            var itemIndexInView = this._list.getStartItemIndexInView(function (idx) { return (idx % 2 === 0 ? evenItemHeight : oddItemHeight); } /* measureItem */);
            alert('unmounting, getting first item index that was in view: ' + itemIndexInView);
        }
    };
    ListScrollingExample.prototype._getPageHeight = function (idx) {
        var h = 0;
        for (var i = idx; i < idx + numberOfItemsOnPage; ++i) {
            var isEvenRow = i % 2 === 0;
            h += isEvenRow ? evenItemHeight : oddItemHeight;
        }
        return h;
    };
    return ListScrollingExample;
}(React.Component));
export { ListScrollingExample };
//# sourceMappingURL=List.Scrolling.Example.js.map