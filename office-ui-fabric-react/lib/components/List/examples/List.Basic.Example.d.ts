import * as React from 'react';
import { IExampleItem } from '@uifabric/example-data';
export interface IListBasicExampleProps {
    items?: IExampleItem[];
}
export interface IListBasicExampleState {
    filterText?: string;
    items?: IExampleItem[];
}
export declare class ListBasicExample extends React.Component<IListBasicExampleProps, IListBasicExampleState> {
    private _originalItems;
    constructor(props: IListBasicExampleProps);
    render(): JSX.Element;
    private _onFilterChanged;
    private _onRenderCell;
}
