import { __extends } from "tslib";
import * as React from 'react';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
// tslint:disable:jsx-no-lambda
var SearchBoxFullSizeExample = /** @class */ (function (_super) {
    __extends(SearchBoxFullSizeExample, _super);
    function SearchBoxFullSizeExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SearchBoxFullSizeExample.prototype.render = function () {
        return (React.createElement(Stack, { tokens: { childrenGap: 20 } },
            React.createElement(SearchBox, { placeholder: "Search", onSearch: function (newValue) { return console.log('value is ' + newValue); }, onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, onChange: function () { return console.log('onChange called'); } }),
            React.createElement(SearchBox, { placeholder: "Search with no animation", onSearch: function (newValue) { return console.log('value is ' + newValue); }, onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, onChange: function () { return console.log('onChange called'); }, disableAnimation: true })));
    };
    return SearchBoxFullSizeExample;
}(React.Component));
export { SearchBoxFullSizeExample };
//# sourceMappingURL=SearchBox.FullSize.Example.js.map