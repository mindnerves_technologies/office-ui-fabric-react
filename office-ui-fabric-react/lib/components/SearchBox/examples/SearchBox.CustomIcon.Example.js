import { __extends } from "tslib";
import * as React from 'react';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
// tslint:disable:jsx-no-lambda
var SearchBoxCustomIconExample = /** @class */ (function (_super) {
    __extends(SearchBoxCustomIconExample, _super);
    function SearchBoxCustomIconExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SearchBoxCustomIconExample.prototype.render = function () {
        return (React.createElement(SearchBox, { placeholder: "Filter", onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, iconProps: { iconName: 'Filter' } }));
    };
    return SearchBoxCustomIconExample;
}(React.Component));
export { SearchBoxCustomIconExample };
//# sourceMappingURL=SearchBox.CustomIcon.Example.js.map