import { __extends } from "tslib";
import * as React from 'react';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
// tslint:disable:jsx-no-lambda
var SearchBoxDisabledExample = /** @class */ (function (_super) {
    __extends(SearchBoxDisabledExample, _super);
    function SearchBoxDisabledExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SearchBoxDisabledExample.prototype.render = function () {
        return (React.createElement(Stack, { tokens: { childrenGap: 20 } },
            React.createElement(SearchBox, { placeholder: "Search", onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, disabled: true }),
            React.createElement(SearchBox, { placeholder: "Search", onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, underlined: true, disabled: true })));
    };
    return SearchBoxDisabledExample;
}(React.Component));
export { SearchBoxDisabledExample };
//# sourceMappingURL=SearchBox.Disabled.Example.js.map