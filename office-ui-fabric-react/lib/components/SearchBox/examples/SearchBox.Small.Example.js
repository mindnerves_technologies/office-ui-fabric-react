import { __extends } from "tslib";
import * as React from 'react';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
// tslint:disable:jsx-no-lambda
var SearchBoxSmallExample = /** @class */ (function (_super) {
    __extends(SearchBoxSmallExample, _super);
    function SearchBoxSmallExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SearchBoxSmallExample.prototype.render = function () {
        return (React.createElement(SearchBox, { styles: { root: { width: 200 } }, placeholder: "Search", onEscape: function (ev) {
                console.log('Custom onEscape Called');
            }, onClear: function (ev) {
                console.log('Custom onClear Called');
            }, onChange: function (_, newValue) { return console.log('SearchBox onChange fired: ' + newValue); }, onSearch: function (newValue) { return console.log('SearchBox onSearch fired: ' + newValue); }, onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); } }));
    };
    return SearchBoxSmallExample;
}(React.Component));
export { SearchBoxSmallExample };
//# sourceMappingURL=SearchBox.Small.Example.js.map