import { __extends } from "tslib";
import * as React from 'react';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
// tslint:disable:jsx-no-lambda
var SearchBoxUnderlinedExample = /** @class */ (function (_super) {
    __extends(SearchBoxUnderlinedExample, _super);
    function SearchBoxUnderlinedExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SearchBoxUnderlinedExample.prototype.render = function () {
        return (React.createElement(SearchBox, { placeholder: "Search", onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, underlined: true }));
    };
    return SearchBoxUnderlinedExample;
}(React.Component));
export { SearchBoxUnderlinedExample };
//# sourceMappingURL=SearchBox.Underlined.Example.js.map