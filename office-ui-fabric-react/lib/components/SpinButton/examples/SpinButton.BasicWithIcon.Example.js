import { __extends } from "tslib";
import * as React from 'react';
import { SpinButton } from 'office-ui-fabric-react/lib/SpinButton';
var SpinButtonBasicWithIconExample = /** @class */ (function (_super) {
    __extends(SpinButtonBasicWithIconExample, _super);
    function SpinButtonBasicWithIconExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SpinButtonBasicWithIconExample.prototype.render = function () {
        return (React.createElement("div", { style: { width: '400px' } },
            React.createElement(SpinButton, { defaultValue: "0", iconProps: { iconName: 'IncreaseIndentLegacy' }, label: 'Basic SpinButton:', min: 0, max: 100, step: 1, 
                // tslint:disable:jsx-no-lambda
                onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, incrementButtonAriaLabel: 'Increase value by 1', decrementButtonAriaLabel: 'Decrease value by 1' })));
    };
    return SpinButtonBasicWithIconExample;
}(React.Component));
export { SpinButtonBasicWithIconExample };
//# sourceMappingURL=SpinButton.BasicWithIcon.Example.js.map