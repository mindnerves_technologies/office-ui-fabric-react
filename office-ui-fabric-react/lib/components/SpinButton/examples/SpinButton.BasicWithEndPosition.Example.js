import { __extends } from "tslib";
import * as React from 'react';
import { SpinButton } from 'office-ui-fabric-react/lib/SpinButton';
import { Position } from 'office-ui-fabric-react/lib/utilities/positioning';
var SpinButtonBasicWithEndPositionExample = /** @class */ (function (_super) {
    __extends(SpinButtonBasicWithEndPositionExample, _super);
    function SpinButtonBasicWithEndPositionExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SpinButtonBasicWithEndPositionExample.prototype.render = function () {
        return (React.createElement("div", { style: { width: '400px' } },
            React.createElement(SpinButton, { defaultValue: "0", iconProps: { iconName: 'Light' }, label: 'Basic SpinButton', labelPosition: Position.end, min: 0, max: 100, step: 1, 
                // tslint:disable:jsx-no-lambda
                onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, incrementButtonAriaLabel: 'Increase value by 1', decrementButtonAriaLabel: 'Decrease value by 1' })));
    };
    return SpinButtonBasicWithEndPositionExample;
}(React.Component));
export { SpinButtonBasicWithEndPositionExample };
//# sourceMappingURL=SpinButton.BasicWithEndPosition.Example.js.map