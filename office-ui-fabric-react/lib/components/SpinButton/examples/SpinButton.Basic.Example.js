import { __extends } from "tslib";
import * as React from 'react';
import { SpinButton } from 'office-ui-fabric-react/lib/SpinButton';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
var SpinButtonBasicExample = /** @class */ (function (_super) {
    __extends(SpinButtonBasicExample, _super);
    function SpinButtonBasicExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SpinButtonBasicExample.prototype.render = function () {
        return (React.createElement(Stack, { tokens: { childrenGap: 10 }, styles: { root: { width: 400 } } },
            React.createElement(SpinButton, { defaultValue: "0", label: 'Basic SpinButton:', min: 0, max: 100, step: 1, iconProps: { iconName: 'IncreaseIndentLegacy' }, 
                // tslint:disable:jsx-no-lambda
                onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, incrementButtonAriaLabel: 'Increase value by 1', decrementButtonAriaLabel: 'Decrease value by 1' }),
            React.createElement(SpinButton, { defaultValue: "0", label: 'Decimal SpinButton:', min: 0, max: 10, step: 0.1, onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, incrementButtonAriaLabel: 'Increase value by 0.1', decrementButtonAriaLabel: 'Decrease value by 0.1' })));
    };
    return SpinButtonBasicExample;
}(React.Component));
export { SpinButtonBasicExample };
//# sourceMappingURL=SpinButton.Basic.Example.js.map