import { __assign, __extends } from "tslib";
import * as React from 'react';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import { Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { Facepile } from 'office-ui-fabric-react/lib/Facepile';
import { PersonaSize, PersonaPresence } from 'office-ui-fabric-react/lib/Persona';
import { Slider } from 'office-ui-fabric-react/lib/Slider';
import { facepilePersonas } from '@uifabric/example-data';
import { mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
var styles = mergeStyleSets({
    container: {
        maxWidth: 300,
    },
    control: {
        paddingTop: 20,
    },
    slider: {
        margin: '10px 0',
    },
    checkbox: {
        paddingTop: 15,
    },
    dropdown: {
        paddingTop: 0,
        margin: '10px 0',
    },
});
var FacepileBasicExample = /** @class */ (function (_super) {
    __extends(FacepileBasicExample, _super);
    function FacepileBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onChangeFadeIn = function (ev, checked) {
            _this.setState(function (prevState) {
                prevState.imagesFadeIn = checked;
                return prevState;
            });
        };
        _this._onChangePersonaNumber = function (value) {
            _this.setState(function (prevState) {
                prevState.numberOfFaces = value;
                return prevState;
            });
        };
        _this._onChangePersonaSize = function (event, value) {
            _this.setState(function (prevState) {
                prevState.personaSize = value.key;
                return prevState;
            });
        };
        _this._personaPresence = function (personaName) {
            var presences = [
                PersonaPresence.away,
                PersonaPresence.busy,
                PersonaPresence.online,
                PersonaPresence.offline,
                PersonaPresence.offline,
            ];
            return presences[personaName.charCodeAt(1) % 5];
        };
        _this.state = {
            numberOfFaces: 3,
            imagesFadeIn: true,
            personaSize: PersonaSize.size32,
        };
        return _this;
    }
    /**
     * Note: The implementation of presence below is simply for demonstration purposes.
     * Typically, the persona presence should be included when generating each facepile persona.
     */
    FacepileBasicExample.prototype.render = function () {
        var _this = this;
        var _a = this.state, numberOfFaces = _a.numberOfFaces, personaSize = _a.personaSize;
        var facepileProps = {
            personaSize: personaSize,
            personas: facepilePersonas.slice(0, numberOfFaces),
            overflowPersonas: facepilePersonas.slice(numberOfFaces),
            getPersonaProps: function (persona) {
                return {
                    imageShouldFadeIn: _this.state.imagesFadeIn,
                    presence: _this._personaPresence(persona.personaName),
                };
            },
            ariaDescription: 'To move through the items use left and right arrow keys.',
            ariaLabel: 'Example list of Facepile personas',
        };
        return (React.createElement("div", { className: styles.container },
            React.createElement(Facepile, __assign({}, facepileProps)),
            React.createElement("div", { className: styles.control },
                React.createElement(Slider, { label: "Number of Personas:", className: styles.slider, min: 1, max: 5, step: 1, showValue: true, value: numberOfFaces, onChange: this._onChangePersonaNumber }),
                React.createElement(Dropdown, { label: "Persona Size:", selectedKey: this.state.personaSize, className: styles.dropdown, options: [
                        { key: PersonaSize.size8, text: PersonaSize[PersonaSize.size8] },
                        { key: PersonaSize.size24, text: PersonaSize[PersonaSize.size24] },
                        { key: PersonaSize.size32, text: PersonaSize[PersonaSize.size32] },
                        { key: PersonaSize.size40, text: PersonaSize[PersonaSize.size40] },
                        { key: PersonaSize.size48, text: PersonaSize[PersonaSize.size48] },
                    ], onChange: this._onChangePersonaSize }),
                React.createElement(Checkbox, { className: styles.checkbox, styles: { root: { margin: '10px 0' } }, label: "Fade In", checked: this.state.imagesFadeIn, onChange: this._onChangeFadeIn }))));
    };
    return FacepileBasicExample;
}(React.Component));
export { FacepileBasicExample };
//# sourceMappingURL=Facepile.Basic.Example.js.map