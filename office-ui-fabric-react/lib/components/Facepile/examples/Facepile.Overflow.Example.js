import { __assign, __extends } from "tslib";
import * as React from 'react';
import { Facepile, OverflowButtonType } from 'office-ui-fabric-react/lib/Facepile';
import { Dropdown } from 'office-ui-fabric-react/lib/Dropdown';
import { Slider } from 'office-ui-fabric-react/lib/Slider';
import { facepilePersonas } from '@uifabric/example-data';
import { mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
var styles = mergeStyleSets({
    container: {
        maxWidth: 300,
    },
    control: {
        paddingTop: 20,
    },
    slider: {
        margin: '10px 0',
    },
    dropdown: {
        paddingTop: 0,
        margin: '10px 0',
    },
});
var facepileProps = {
    personas: facepilePersonas,
    maxDisplayablePersonas: 5,
    overflowButtonType: OverflowButtonType.downArrow,
    overflowButtonProps: {
        ariaLabel: 'More users',
        onClick: function (ev) { return alert('overflow icon clicked'); },
    },
    ariaDescription: 'To move through the items use left and right arrow keys.',
    ariaLabel: 'Example list of Facepile personas',
};
var FacepileOverflowExample = /** @class */ (function (_super) {
    __extends(FacepileOverflowExample, _super);
    function FacepileOverflowExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onChangePersonaNumber = function (value) {
            _this.setState(function (prevState) {
                prevState.displayedPersonas = value;
                return prevState;
            });
        };
        _this._onChangeType = function (event, value) {
            _this.setState(function (prevState) {
                prevState.overflowButtonType = value.key;
                return prevState;
            });
        };
        _this.state = {
            displayedPersonas: 5,
            overflowButtonType: OverflowButtonType.none,
        };
        return _this;
    }
    FacepileOverflowExample.prototype.render = function () {
        var _a = this.state, displayedPersonas = _a.displayedPersonas, overflowButtonType = _a.overflowButtonType;
        facepileProps.maxDisplayablePersonas = displayedPersonas;
        facepileProps.overflowButtonType = overflowButtonType;
        return (React.createElement("div", { className: styles.container },
            React.createElement(Facepile, __assign({}, facepileProps)),
            React.createElement("div", { className: styles.control },
                React.createElement(Slider, { label: "Number of Personas:", className: styles.slider, min: 1, max: 5, step: 1, showValue: true, value: this.state.displayedPersonas, onChange: this._onChangePersonaNumber }),
                React.createElement(Dropdown, { label: "Overflow Button Type:", className: styles.dropdown, selectedKey: this.state.overflowButtonType, options: [
                        { key: OverflowButtonType.none, text: OverflowButtonType[OverflowButtonType.none] },
                        { key: OverflowButtonType.descriptive, text: OverflowButtonType[OverflowButtonType.descriptive] },
                        { key: OverflowButtonType.downArrow, text: OverflowButtonType[OverflowButtonType.downArrow] },
                        { key: OverflowButtonType.more, text: OverflowButtonType[OverflowButtonType.more] },
                    ], onChange: this._onChangeType }))));
    };
    return FacepileOverflowExample;
}(React.Component));
export { FacepileOverflowExample };
//# sourceMappingURL=Facepile.Overflow.Example.js.map