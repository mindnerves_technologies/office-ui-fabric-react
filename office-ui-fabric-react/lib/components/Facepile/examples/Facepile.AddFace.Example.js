import { __assign, __extends } from "tslib";
import * as React from 'react';
import { Facepile, OverflowButtonType } from 'office-ui-fabric-react/lib/Facepile';
import { facepilePersonas } from '@uifabric/example-data';
var FacepileAddFaceExample = /** @class */ (function (_super) {
    __extends(FacepileAddFaceExample, _super);
    function FacepileAddFaceExample(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            numberOfFaces: 0,
        };
        return _this;
    }
    FacepileAddFaceExample.prototype.render = function () {
        var _this = this;
        var numberOfFaces = this.state.numberOfFaces;
        var facepileProps = {
            personas: facepilePersonas.slice(0, numberOfFaces),
            maxDisplayablePersonas: 5,
            overflowButtonProps: {
                ariaLabel: 'More users',
                onClick: function (ev) { return alert('overflow icon clicked'); },
            },
            overflowButtonType: OverflowButtonType.descriptive,
            showAddButton: true,
            addButtonProps: {
                ariaLabel: 'Add a new person',
                onClick: function (ev) {
                    return _this.setState({
                        numberOfFaces: _this.state.numberOfFaces + 1,
                    });
                },
            },
            ariaDescription: 'To move through the items use left and right arrow keys.',
        };
        return React.createElement(Facepile, __assign({}, facepileProps));
    };
    return FacepileAddFaceExample;
}(React.Component));
export { FacepileAddFaceExample };
//# sourceMappingURL=Facepile.AddFace.Example.js.map