import * as React from 'react';
import { PersonaSize } from 'office-ui-fabric-react/lib/Persona';
export interface IFacepileBasicExampleState {
    numberOfFaces: any;
    imagesFadeIn: boolean;
    personaSize: PersonaSize;
}
export declare class FacepileBasicExample extends React.Component<{}, IFacepileBasicExampleState> {
    constructor(props: {});
    /**
     * Note: The implementation of presence below is simply for demonstration purposes.
     * Typically, the persona presence should be included when generating each facepile persona.
     */
    render(): JSX.Element;
    private _onChangeFadeIn;
    private _onChangePersonaNumber;
    private _onChangePersonaSize;
    private _personaPresence;
}
