import { __extends } from "tslib";
import * as React from 'react';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { ContextualMenu } from 'office-ui-fabric-react/lib/ContextualMenu';
import { SpinButton } from 'office-ui-fabric-react/lib/SpinButton';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import { ComboBox, SelectableOptionMenuItemType } from 'office-ui-fabric-react/lib/index';
var INITIAL_OPTIONS = [
    { key: 'Header1', text: 'First heading', itemType: SelectableOptionMenuItemType.Header },
    { key: 'A', text: 'Option A' },
    { key: 'B', text: 'Option B' },
    { key: 'C', text: 'Option C' },
    { key: 'D', text: 'Option D' },
    { key: 'divider', text: '-', itemType: SelectableOptionMenuItemType.Divider },
    { key: 'Header2', text: 'Second heading', itemType: SelectableOptionMenuItemType.Header },
    { key: 'E', text: 'Option E' },
    { key: 'F', text: 'Option F', disabled: true },
    { key: 'G', text: 'Option G' },
    { key: 'H', text: 'Option H' },
    { key: 'I', text: 'Option I' },
    { key: 'J', text: 'Option J' },
];
var DialogBlockingExample = /** @class */ (function (_super) {
    __extends(DialogBlockingExample, _super);
    function DialogBlockingExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { hideDialog: true, isDraggable: false };
        _this._dragOptions = {
            moveMenuItemText: 'Move',
            closeMenuItemText: 'Close',
            menu: ContextualMenu,
        };
        _this._showDialog = function () {
            _this.setState({ hideDialog: false });
        };
        _this._closeDialog = function () {
            _this.setState({ hideDialog: true });
        };
        _this._toggleDraggable = function () {
            _this.setState({ isDraggable: !_this.state.isDraggable });
        };
        return _this;
    }
    DialogBlockingExample.prototype.render = function () {
        var _a = this.state, hideDialog = _a.hideDialog, isDraggable = _a.isDraggable;
        return (React.createElement("div", null,
            React.createElement(Checkbox, { label: "Is draggable", onChange: this._toggleDraggable, checked: isDraggable }),
            React.createElement(DefaultButton, { secondaryText: "Opens the Sample Dialog", onClick: this._showDialog, text: "Open Dialog" }),
            React.createElement(Dialog, { hidden: hideDialog, onDismiss: this._closeDialog, dialogContentProps: {
                    type: DialogType.normal,
                    title: 'Missing Subject',
                    subText: 'Do you want to send this message without a subject?',
                }, modalProps: {
                    isBlocking: true,
                    styles: { main: { maxWidth: 450 } },
                    dragOptions: isDraggable ? this._dragOptions : undefined,
                } },
                React.createElement(SpinButton, { defaultValue: "0", label: 'Number of subjects to add:', min: 0, max: 100, step: 1, iconProps: { iconName: 'IncreaseIndentLegacy' }, 
                    // tslint:disable:jsx-no-lambda
                    onFocus: function () { return console.log('onFocus called'); }, onBlur: function () { return console.log('onBlur called'); }, incrementButtonAriaLabel: 'Increase value by 1', decrementButtonAriaLabel: 'Decrease value by 1' }),
                React.createElement(ComboBox, { label: "Sample subject lines you could add instead", placeholder: "Select or type an option", allowFreeform: true, autoComplete: "on", options: INITIAL_OPTIONS }),
                React.createElement(DialogFooter, null,
                    React.createElement(PrimaryButton, { onClick: this._closeDialog, text: "Send" }),
                    React.createElement(DefaultButton, { onClick: this._closeDialog, text: "Don't send" })))));
    };
    return DialogBlockingExample;
}(React.Component));
export { DialogBlockingExample };
//# sourceMappingURL=Dialog.Blocking.Example.js.map