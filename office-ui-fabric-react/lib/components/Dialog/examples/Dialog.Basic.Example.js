import { __extends } from "tslib";
import * as React from 'react';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { getId } from 'office-ui-fabric-react/lib/Utilities';
import { hiddenContentStyle, mergeStyles } from 'office-ui-fabric-react/lib/Styling';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import { ContextualMenu } from 'office-ui-fabric-react/lib/ContextualMenu';
var screenReaderOnly = mergeStyles(hiddenContentStyle);
var DialogBasicExample = /** @class */ (function (_super) {
    __extends(DialogBasicExample, _super);
    function DialogBasicExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            hideDialog: true,
            isDraggable: false,
        };
        // Use getId() to ensure that the IDs are unique on the page.
        // (It's also okay to use plain strings without getId() and manually ensure uniqueness.)
        _this._labelId = getId('dialogLabel');
        _this._subTextId = getId('subTextLabel');
        _this._dragOptions = {
            moveMenuItemText: 'Move',
            closeMenuItemText: 'Close',
            menu: ContextualMenu,
        };
        _this._showDialog = function () {
            _this.setState({ hideDialog: false });
        };
        _this._closeDialog = function () {
            _this.setState({ hideDialog: true });
        };
        _this._toggleDraggable = function () {
            _this.setState({ isDraggable: !_this.state.isDraggable });
        };
        return _this;
    }
    DialogBasicExample.prototype.render = function () {
        var _a = this.state, hideDialog = _a.hideDialog, isDraggable = _a.isDraggable;
        return (React.createElement("div", null,
            React.createElement(Checkbox, { label: "Is draggable", onChange: this._toggleDraggable, checked: isDraggable }),
            React.createElement(DefaultButton, { secondaryText: "Opens the Sample Dialog", onClick: this._showDialog, text: "Open Dialog" }),
            React.createElement("label", { id: this._labelId, className: screenReaderOnly }, "My sample Label"),
            React.createElement("label", { id: this._subTextId, className: screenReaderOnly }, "My Sample description"),
            React.createElement(Dialog, { hidden: hideDialog, onDismiss: this._closeDialog, dialogContentProps: {
                    type: DialogType.normal,
                    title: 'Missing Subject',
                    closeButtonAriaLabel: 'Close',
                    subText: 'Do you want to send this message without a subject?',
                }, modalProps: {
                    titleAriaId: this._labelId,
                    subtitleAriaId: this._subTextId,
                    isBlocking: false,
                    styles: { main: { maxWidth: 450 } },
                    dragOptions: isDraggable ? this._dragOptions : undefined,
                } },
                React.createElement(DialogFooter, null,
                    React.createElement(PrimaryButton, { onClick: this._closeDialog, text: "Send" }),
                    React.createElement(DefaultButton, { onClick: this._closeDialog, text: "Don't send" })))));
    };
    return DialogBasicExample;
}(React.Component));
export { DialogBasicExample };
//# sourceMappingURL=Dialog.Basic.Example.js.map