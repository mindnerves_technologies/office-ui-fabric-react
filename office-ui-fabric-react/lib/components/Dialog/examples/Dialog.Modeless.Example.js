import { __extends } from "tslib";
import * as React from 'react';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import { ContextualMenu } from 'office-ui-fabric-react/lib/ContextualMenu';
var DialogModelessExample = /** @class */ (function (_super) {
    __extends(DialogModelessExample, _super);
    function DialogModelessExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            hideDialog: true,
            isDraggable: false,
        };
        _this._dragOptions = {
            moveMenuItemText: 'Move',
            closeMenuItemText: 'Close',
            menu: ContextualMenu,
        };
        _this._showDialog = function () {
            _this.setState({ hideDialog: false });
        };
        _this._closeDialog = function () {
            _this.setState({ hideDialog: true });
        };
        _this._toggleDraggable = function () {
            _this.setState({ isDraggable: !_this.state.isDraggable });
        };
        return _this;
    }
    DialogModelessExample.prototype.render = function () {
        var _a = this.state, hideDialog = _a.hideDialog, isDraggable = _a.isDraggable;
        return (React.createElement("div", null,
            React.createElement("input", { type: "text", placeholder: "Focus Me While Open" }),
            React.createElement("div", null,
                React.createElement(Checkbox, { label: "Is draggable", onChange: this._toggleDraggable, checked: isDraggable, disabled: !hideDialog }),
                React.createElement(DefaultButton, { secondaryText: "Opens the Sample Dialog", onClick: this._showDialog, text: "Open Dialog" }),
                React.createElement(DefaultButton, { secondaryText: "Closes the Sample Dialog", onClick: this._closeDialog, text: "Close Dialog" })),
            React.createElement(Dialog, { hidden: hideDialog, onDismiss: this._closeDialog, dialogContentProps: {
                    type: DialogType.normal,
                    title: 'Missing Subject',
                    subText: 'Do you want to send this message without a subject?',
                }, modalProps: {
                    styles: { main: { maxWidth: 450 } },
                    isModeless: true,
                    dragOptions: isDraggable ? this._dragOptions : undefined,
                    onDismissed: this._closeDialog,
                } },
                React.createElement(DialogFooter, null,
                    React.createElement(PrimaryButton, { onClick: this._closeDialog, text: "Send" }),
                    React.createElement(DefaultButton, { onClick: this._closeDialog, text: "Don't send" })))));
    };
    return DialogModelessExample;
}(React.Component));
export { DialogModelessExample };
//# sourceMappingURL=Dialog.Modeless.Example.js.map