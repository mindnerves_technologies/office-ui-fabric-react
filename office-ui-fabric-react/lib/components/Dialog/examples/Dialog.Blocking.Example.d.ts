import * as React from 'react';
export interface IDialogBlockingExampleState {
    hideDialog: boolean;
    isDraggable: boolean;
}
export declare class DialogBlockingExample extends React.Component<{}, IDialogBlockingExampleState> {
    state: IDialogBlockingExampleState;
    private _dragOptions;
    render(): JSX.Element;
    private _showDialog;
    private _closeDialog;
    private _toggleDraggable;
}
