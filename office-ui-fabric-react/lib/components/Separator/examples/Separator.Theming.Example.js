import { __extends } from "tslib";
import * as React from 'react';
import { Separator } from 'office-ui-fabric-react/lib/Separator';
import { createTheme } from 'office-ui-fabric-react/lib/Styling';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Text } from 'office-ui-fabric-react/lib/Text';
var theme = createTheme({
    fonts: {
        medium: {
            fontFamily: 'Monaco, Menlo, Consolas',
            fontSize: '30px',
        },
    },
});
var stackTokens = { childrenGap: 12 };
var SeparatorThemingExample = /** @class */ (function (_super) {
    __extends(SeparatorThemingExample, _super);
    function SeparatorThemingExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SeparatorThemingExample.prototype.render = function () {
        var content = 'Today';
        return (React.createElement(Stack, { tokens: stackTokens },
            React.createElement(Text, null, "Horizontal center aligned with custom theme"),
            React.createElement(Separator, { theme: theme }, content)));
    };
    return SeparatorThemingExample;
}(React.Component));
export { SeparatorThemingExample };
//# sourceMappingURL=Separator.Theming.Example.js.map