import { __extends } from "tslib";
import * as React from 'react';
import { HoverCard, DirectionalHint } from 'office-ui-fabric-react/lib/HoverCard';
import { DetailsList, buildColumns } from 'office-ui-fabric-react/lib/DetailsList';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { createListItems } from '@uifabric/example-data';
import { KeyCodes } from 'office-ui-fabric-react/lib/Utilities';
import { mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
var classNames = mergeStyleSets({
    compactCard: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    expandedCard: {
        padding: '16px 24px',
    },
    item: {
        selectors: {
            '&:hover': {
                textDecoration: 'underline',
                cursor: 'pointer',
            },
        },
    },
});
var HoverCardField = /** @class */ (function (_super) {
    __extends(HoverCardField, _super);
    function HoverCardField(props) {
        var _this = _super.call(this, props) || this;
        _this.targetElementRef = React.createRef();
        _this.state = {
            contentRendered: false,
        };
        return _this;
    }
    HoverCardField.prototype.componentDidMount = function () {
        this.setState({ contentRendered: true });
    };
    HoverCardField.prototype.render = function () {
        return (React.createElement("div", { ref: this.targetElementRef, "data-is-focusable": true },
            this.props.content,
            this.state.contentRendered && (React.createElement(HoverCard, { expandingCardProps: this.props.expandingCardProps, target: this.targetElementRef.current, cardDismissDelay: 300, onCardVisible: this._log('onCardVisible'), onCardHide: this._log('onCardHide'), trapFocus: true, openHotKey: KeyCodes.enter }))));
    };
    HoverCardField.prototype._log = function (text) {
        return function () {
            console.log(text);
        };
    };
    return HoverCardField;
}(React.Component));
var HoverCardTargetExample = /** @class */ (function (_super) {
    __extends(HoverCardTargetExample, _super);
    function HoverCardTargetExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._items = createListItems(10);
        _this._columns = _this._buildColumns();
        _this._onRenderItemColumn = function (item, index, column) {
            var expandingCardProps = {
                onRenderCompactCard: _this._onRenderCompactCard,
                onRenderExpandedCard: _this._onRenderExpandedCard,
                renderData: item,
                directionalHint: DirectionalHint.rightTopEdge,
                gapSpace: 16,
                calloutProps: {
                    isBeakVisible: true,
                },
            };
            if (column.key === 'key') {
                return (React.createElement("div", { className: classNames.item },
                    React.createElement(HoverCardField, { content: item.key, expandingCardProps: expandingCardProps })));
            }
            return item[column.key];
        };
        _this._onRenderCompactCard = function (item) {
            return (React.createElement("div", { className: classNames.compactCard },
                React.createElement("a", { target: "_blank", href: "http://wikipedia.org/wiki/" + item.location }, item.location)));
        };
        _this._onRenderExpandedCard = function (item) {
            return (React.createElement("div", { className: classNames.expandedCard },
                item.description,
                React.createElement(DetailsList, { setKey: "expandedCardSet", items: _this._items, columns: _this._columns })));
        };
        return _this;
    }
    HoverCardTargetExample.prototype.render = function () {
        return (React.createElement(Fabric, null,
            React.createElement("p", null,
                "Hover over the ",
                React.createElement("i", null, "key"),
                " cell of a row item to see the card or use the keyboard to navigate to it by tabbing to a row and hitting the right arrow key."),
            React.createElement("p", null, "When using the keyboard to navigate, open the card with the hotKey and it will automatically focus the first focusable element in the card allowing further navigation inside the card. The hotKey is defined by the hotKey prop and is defined as 'enter' in the following example."),
            React.createElement(DetailsList, { setKey: "hoverSet", items: this._items, columns: this._columns, onRenderItemColumn: this._onRenderItemColumn, ariaLabel: "Hover card DetailsList test" })));
    };
    HoverCardTargetExample.prototype._buildColumns = function () {
        return buildColumns(this._items).filter(function (column) { return column.name === 'location' || column.name === 'key'; });
    };
    return HoverCardTargetExample;
}(React.Component));
export { HoverCardTargetExample };
//# sourceMappingURL=HoverCard.Target.Example.js.map