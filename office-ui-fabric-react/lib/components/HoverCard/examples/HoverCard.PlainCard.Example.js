import { __extends } from "tslib";
import * as React from 'react';
import { HoverCard, HoverCardType } from 'office-ui-fabric-react/lib/HoverCard';
import { DetailsList, buildColumns } from 'office-ui-fabric-react/lib/DetailsList';
import { createListItems } from '@uifabric/example-data';
import { Image, ImageFit } from 'office-ui-fabric-react/lib/Image';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { getColorFromString } from 'office-ui-fabric-react/lib/Color';
import { mergeStyles } from 'office-ui-fabric-react/lib/Styling';
var itemClass = mergeStyles({
    selectors: {
        '&:hover': {
            textDecoration: 'underline',
            cursor: 'pointer',
        },
    },
});
var HoverCardPlainCardExample = /** @class */ (function (_super) {
    __extends(HoverCardPlainCardExample, _super);
    function HoverCardPlainCardExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._items = createListItems(10);
        _this._columns = _this._buildColumns();
        _this._onRenderItemColumn = function (item, index, column) {
            var plainCardProps = {
                onRenderPlainCard: _this._onRenderPlainCard,
                renderData: item,
            };
            if (column.key === 'color') {
                return (React.createElement(HoverCard, { plainCardProps: plainCardProps, instantOpenOnClick: true, type: HoverCardType.plain },
                    React.createElement("div", { className: itemClass, style: { color: item.color } }, item.color)));
            }
            return item[column.key];
        };
        _this._onRenderPlainCard = function (item) {
            var src = item.thumbnail + ("/" + getColorFromString(item.color).hex);
            return React.createElement(Image, { src: src, width: item.width, height: item.height, imageFit: ImageFit.cover });
        };
        return _this;
    }
    HoverCardPlainCardExample.prototype.render = function () {
        return (React.createElement(Fabric, null,
            React.createElement("p", null,
                "Hover over the ",
                React.createElement("i", null, "color"),
                " cell of a row item to see the card."),
            React.createElement(DetailsList, { setKey: "hoverSet", items: this._items, columns: this._columns, onRenderItemColumn: this._onRenderItemColumn })));
    };
    HoverCardPlainCardExample.prototype._buildColumns = function () {
        return buildColumns(this._items).filter(function (column) { return column.name === 'color' || column.name === 'width' || column.name === 'height'; });
    };
    return HoverCardPlainCardExample;
}(React.Component));
export { HoverCardPlainCardExample };
//# sourceMappingURL=HoverCard.PlainCard.Example.js.map