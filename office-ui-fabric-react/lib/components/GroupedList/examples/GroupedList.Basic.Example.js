import { __extends } from "tslib";
import * as React from 'react';
import { GroupedList } from 'office-ui-fabric-react/lib/GroupedList';
import { DetailsRow } from 'office-ui-fabric-react/lib/DetailsList';
import { FocusZone } from 'office-ui-fabric-react/lib/FocusZone';
import { Selection, SelectionMode, SelectionZone } from 'office-ui-fabric-react/lib/Selection';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { createListItems, createGroups } from '@uifabric/example-data';
var groupCount = 3;
var groupDepth = 3;
var GroupedListBasicExample = /** @class */ (function (_super) {
    __extends(GroupedListBasicExample, _super);
    function GroupedListBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onRenderCell = function (nestingDepth, item, itemIndex) {
            return (React.createElement(DetailsRow, { columns: _this._columns, groupNestingDepth: nestingDepth, item: item, itemIndex: itemIndex, selection: _this._selection, selectionMode: SelectionMode.multiple, compact: _this.state.isCompactMode }));
        };
        _this._onChangeCompactMode = function (ev, checked) {
            _this.setState({ isCompactMode: checked });
        };
        _this._items = createListItems(Math.pow(groupCount, groupDepth + 1));
        _this._columns = Object.keys(_this._items[0])
            .slice(0, 3)
            .map(function (key) { return ({
            key: key,
            name: key,
            fieldName: key,
            minWidth: 300,
        }); });
        _this._groups = createGroups(groupCount, groupDepth, 0, groupCount);
        _this._selection = new Selection();
        _this._selection.setItems(_this._items);
        _this.state = {
            isCompactMode: false,
        };
        return _this;
    }
    GroupedListBasicExample.prototype.render = function () {
        var isCompactMode = this.state.isCompactMode;
        return (React.createElement("div", null,
            React.createElement(Toggle, { label: "Enable compact mode", checked: isCompactMode, onChange: this._onChangeCompactMode, onText: "Compact", offText: "Normal", styles: { root: { marginBottom: '20px' } } }),
            React.createElement(FocusZone, null,
                React.createElement(SelectionZone, { selection: this._selection, selectionMode: SelectionMode.multiple },
                    React.createElement(GroupedList, { items: this._items, onRenderCell: this._onRenderCell, selection: this._selection, selectionMode: SelectionMode.multiple, groups: this._groups, compact: isCompactMode })))));
    };
    return GroupedListBasicExample;
}(React.Component));
export { GroupedListBasicExample };
//# sourceMappingURL=GroupedList.Basic.Example.js.map