import { __assign, __extends } from "tslib";
import * as React from 'react';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { lorem } from '@uifabric/example-data';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
var TextFieldMultilineExample = /** @class */ (function (_super) {
    __extends(TextFieldMultilineExample, _super);
    function TextFieldMultilineExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { multiline: false };
        _this._lorem = lorem(100);
        _this._onChange = function (ev, newText) {
            var newMultiline = newText.length > 50;
            if (newMultiline !== _this.state.multiline) {
                _this.setState({ multiline: newMultiline });
            }
        };
        return _this;
    }
    TextFieldMultilineExample.prototype.render = function () {
        // TextFields don't have to be inside Stacks, we're just using Stacks for layout
        var columnProps = {
            tokens: { childrenGap: 15 },
            styles: { root: { width: 300 } },
        };
        return (React.createElement(Stack, { horizontal: true, tokens: { childrenGap: 50 }, styles: { root: { width: 650 } } },
            React.createElement(Stack, __assign({}, columnProps),
                React.createElement(TextField, { label: "Standard", multiline: true, rows: 3 }),
                React.createElement(TextField, { label: "Disabled", multiline: true, rows: 3, disabled: true, defaultValue: this._lorem }),
                React.createElement(TextField, { label: "Non-resizable", multiline: true, resizable: false })),
            React.createElement(Stack, __assign({}, columnProps),
                React.createElement(TextField, { label: "With auto adjusting height", multiline: true, autoAdjustHeight: true }),
                React.createElement(TextField, { label: "Switches from single to multiline if more than 50 characters are entered", multiline: this.state.multiline, onChange: this._onChange }))));
    };
    return TextFieldMultilineExample;
}(React.Component));
export { TextFieldMultilineExample };
//# sourceMappingURL=TextField.Multiline.Example.js.map