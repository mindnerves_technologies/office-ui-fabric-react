import * as React from 'react';
import { MaskedTextField, Stack } from 'office-ui-fabric-react';
var maskFormat = {
    '*': /[a-zA-Z0-9_]/,
};
export var TextFieldMaskedExample = function () {
    return (React.createElement(Stack, { tokens: { maxWidth: 300 } },
        React.createElement("p", null, "The mask has been modified here to allow \"_\""),
        React.createElement(MaskedTextField, { label: "With input mask", mask: "m\\ask: ****", maskFormat: maskFormat, maskChar: "?" })));
};
//# sourceMappingURL=TextField.Masked.Example.js.map