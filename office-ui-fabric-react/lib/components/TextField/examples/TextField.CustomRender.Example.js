import { __extends } from "tslib";
import * as React from 'react';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { DefaultButton, IconButton } from 'office-ui-fabric-react/lib/Button';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import { Callout } from 'office-ui-fabric-react/lib/Callout';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { getId } from 'office-ui-fabric-react/lib/Utilities';
import { getTheme, FontWeights } from 'office-ui-fabric-react/lib/Styling';
var stackTokens = {
    childrenGap: 20,
    maxWidth: 300,
};
var TextFieldCustomRenderExample = /** @class */ (function (_super) {
    __extends(TextFieldCustomRenderExample, _super);
    function TextFieldCustomRenderExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { isCalloutVisible: false };
        _this._descriptionId = getId('description');
        _this._iconButtonId = getId('iconButton');
        _this._labelId = getId('label');
        _this._onRenderDescription = function (props) {
            var theme = getTheme();
            return (React.createElement(Text, { variant: "small", styles: { root: { color: theme.palette.green, fontWeight: FontWeights.bold } } }, props.description));
        };
        _this._onRenderLabel = function (props) {
            return (React.createElement(React.Fragment, null,
                React.createElement(Stack, { horizontal: true, verticalAlign: "center" },
                    React.createElement("span", { id: _this._labelId }, props.label),
                    React.createElement(IconButton, { id: _this._iconButtonId, iconProps: { iconName: 'Info' }, title: "Info", ariaLabel: "Info", onClick: _this._onIconClick, styles: { root: { marginBottom: -3 } } })),
                _this.state.isCalloutVisible && (React.createElement(Callout, { target: '#' + _this._iconButtonId, setInitialFocus: true, onDismiss: _this._onDismiss, ariaDescribedBy: _this._descriptionId, role: "alertdialog" },
                    React.createElement(Stack, { tokens: stackTokens, horizontalAlign: "start", styles: { root: { padding: 20 } } },
                        React.createElement("span", { id: _this._descriptionId }, "The custom label includes an IconButton that displays this Callout on click."),
                        React.createElement(DefaultButton, { onClick: _this._onDismiss }, "Close"))))));
        };
        _this._onWrapDefaultLabelRenderer = function (props, defaultRender) {
            return (React.createElement(React.Fragment, null,
                React.createElement(Stack, { horizontal: true, verticalAlign: "center", tokens: { childrenGap: 6 } },
                    React.createElement("span", null, defaultRender(props)),
                    React.createElement(Icon, { iconName: "Globe", title: "Globe", ariaLabel: "Globe", styles: { root: { marginBottom: -3 } } }))));
        };
        _this._onIconClick = function () {
            _this.setState({ isCalloutVisible: !_this.state.isCalloutVisible });
        };
        _this._onDismiss = function () {
            _this.setState({ isCalloutVisible: false });
        };
        return _this;
    }
    TextFieldCustomRenderExample.prototype.render = function () {
        return (React.createElement(Stack, { tokens: stackTokens },
            React.createElement(TextField, { "aria-labelledby": this._labelId, label: "Custom label rendering", onRenderLabel: this._onRenderLabel, description: "Click the (i) icon!" }),
            React.createElement(TextField, { label: "Wrapping default label renderer", onRenderLabel: this._onWrapDefaultLabelRenderer }),
            React.createElement(TextField, { label: "Custom description rendering", description: "A colorful description!", onRenderDescription: this._onRenderDescription })));
    };
    return TextFieldCustomRenderExample;
}(React.Component));
export { TextFieldCustomRenderExample };
//# sourceMappingURL=TextField.CustomRender.Example.js.map