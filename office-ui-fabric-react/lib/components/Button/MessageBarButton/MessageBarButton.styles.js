import { concatStyleSets, getFocusStyle } from '../../../Styling';
import { memoizeFunction } from '../../../Utilities';
import { getStyles as getBaseButtonStyles } from '../BaseButton.styles';
export var getStyles = memoizeFunction(function (theme, customStyles, focusInset, focusColor) {
    var baseButtonStyles = getBaseButtonStyles(theme);
    var messageBarButtonStyles = {
        root: [
            getFocusStyle(theme, {
                inset: 1,
                highContrastStyle: {
                    outlineOffset: '-4px',
                    outlineColor: 'ActiveBorder',
                },
                borderColor: 'transparent',
            }),
            {
                height: 24,
                borderColor: theme.palette.neutralTertiaryAlt,
            },
        ],
    };
    return concatStyleSets(baseButtonStyles, messageBarButtonStyles, customStyles);
});
//# sourceMappingURL=MessageBarButton.styles.js.map