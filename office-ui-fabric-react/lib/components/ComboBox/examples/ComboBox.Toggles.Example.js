import { __extends } from "tslib";
import * as React from 'react';
import { ComboBox, Fabric, mergeStyles, SelectableOptionMenuItemType, Toggle, } from 'office-ui-fabric-react/lib/index';
var INITIAL_OPTIONS = [
    { key: 'Header1', text: 'First heading', itemType: SelectableOptionMenuItemType.Header },
    { key: 'A', text: 'Option A' },
    { key: 'B', text: 'Option B' },
    { key: 'C', text: 'Option C' },
    { key: 'D', text: 'Option D' },
    { key: 'divider', text: '-', itemType: SelectableOptionMenuItemType.Divider },
    { key: 'Header2', text: 'Second heading', itemType: SelectableOptionMenuItemType.Header },
    { key: 'E', text: 'Option E' },
    { key: 'F', text: 'Option F', disabled: true },
    { key: 'G', text: 'Option G' },
    { key: 'H', text: 'Option H' },
    { key: 'I', text: 'Option I' },
    { key: 'J', text: 'Option J' },
];
var wrapperClassName = mergeStyles({
    display: 'flex',
    selectors: {
        '& > *': { marginRight: '20px' },
        '& .ms-ComboBox': { maxWidth: '300px' },
    },
});
// tslint:disable:jsx-no-lambda
var ComboBoxTogglesExample = /** @class */ (function (_super) {
    __extends(ComboBoxTogglesExample, _super);
    function ComboBoxTogglesExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            autoComplete: false,
            allowFreeform: true,
        };
        return _this;
    }
    ComboBoxTogglesExample.prototype.render = function () {
        var _this = this;
        var state = this.state;
        return (React.createElement(Fabric, { className: wrapperClassName },
            React.createElement(ComboBox, { label: "ComboBox with toggleable freeform/auto-complete", key: '' + state.autoComplete + state.allowFreeform /*key causes re-render when toggles change*/, allowFreeform: state.allowFreeform, autoComplete: state.autoComplete ? 'on' : 'off', options: INITIAL_OPTIONS }),
            React.createElement(Toggle, { label: "Allow freeform", checked: state.allowFreeform, onChange: function (ev, checked) {
                    _this.setState({ allowFreeform: !!checked });
                } }),
            React.createElement(Toggle, { label: "Auto-complete", checked: state.autoComplete, onChange: function (ev, checked) {
                    _this.setState({ autoComplete: !!checked });
                } })));
    };
    return ComboBoxTogglesExample;
}(React.Component));
export { ComboBoxTogglesExample };
//# sourceMappingURL=ComboBox.Toggles.Example.js.map