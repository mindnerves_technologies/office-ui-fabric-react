import { __extends } from "tslib";
import * as React from 'react';
import { ComboBox, Fabric, mergeStyles, PrimaryButton, SelectableOptionMenuItemType, } from 'office-ui-fabric-react/lib/index';
var INITIAL_OPTIONS = [
    { key: 'Header1', text: 'First heading', itemType: SelectableOptionMenuItemType.Header },
    { key: 'A', text: 'Option A' },
    { key: 'B', text: 'Option B' },
    { key: 'C', text: 'Option C' },
    { key: 'D', text: 'Option D' },
    { key: 'divider', text: '-', itemType: SelectableOptionMenuItemType.Divider },
    { key: 'Header2', text: 'Second heading', itemType: SelectableOptionMenuItemType.Header },
    { key: 'E', text: 'Option E' },
    { key: 'F', text: 'Option F', disabled: true },
    { key: 'G', text: 'Option G' },
    { key: 'H', text: 'Option H' },
    { key: 'I', text: 'Option I' },
    { key: 'J', text: 'Option J' },
];
var wrapperClassName = mergeStyles({
    selectors: {
        '& > *': { marginBottom: '20px' },
        '& .ms-ComboBox': { maxWidth: '300px' },
    },
});
// tslint:disable:jsx-no-lambda
var ComboBoxBasicExample = /** @class */ (function (_super) {
    __extends(ComboBoxBasicExample, _super);
    function ComboBoxBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._basicComboBox = React.createRef();
        _this._onChange = function (event, option) {
            if (option) {
                _this.setState({ dynamicErrorValue: option.key });
            }
        };
        _this.state = {
            dynamicErrorValue: '',
        };
        return _this;
    }
    ComboBoxBasicExample.prototype.render = function () {
        var _this = this;
        return (React.createElement(Fabric, { className: wrapperClassName },
            React.createElement("div", null,
                React.createElement(ComboBox, { defaultSelectedKey: "C", label: "Single-select ComboBox (uncontrolled, allowFreeform: T, autoComplete: T)", allowFreeform: true, autoComplete: "on", options: INITIAL_OPTIONS, componentRef: this._basicComboBox, onFocus: function () { return console.log('onFocus called for basic uncontrolled example'); }, onBlur: function () { return console.log('onBlur called for basic uncontrolled example'); }, onMenuOpen: function () { return console.log('ComboBox menu opened'); }, onPendingValueChanged: function (option, pendingIndex, pendingValue) {
                        return console.log("Preview value was changed. Pending index: " + pendingIndex + ". Pending value: " + pendingValue + ".");
                    } }),
                React.createElement(PrimaryButton, { text: "Open ComboBox", style: { display: 'block', marginTop: '10px' }, onClick: function () {
                        if (_this._basicComboBox.current) {
                            _this._basicComboBox.current.focus(true);
                        }
                    } })),
            React.createElement(ComboBox, { multiSelect: true, defaultSelectedKey: ['C', 'E'], label: "Multi-select ComboBox (uncontrolled)", allowFreeform: true, autoComplete: "on", options: INITIAL_OPTIONS }),
            React.createElement(ComboBox, { label: "ComboBox with placeholder text", placeholder: "Select or type an option", allowFreeform: true, autoComplete: "on", options: INITIAL_OPTIONS }),
            React.createElement(ComboBox, { label: "ComboBox with persisted menu", defaultSelectedKey: "B", allowFreeform: true, autoComplete: "on", persistMenu: true, options: INITIAL_OPTIONS }),
            React.createElement(ComboBox, { label: "ComboBox with static error message", defaultSelectedKey: "B", errorMessage: "Oh no! This ComboBox has an error!", options: INITIAL_OPTIONS }),
            React.createElement(ComboBox, { label: "ComboBox with dynamic error message", onChange: this._onChange, selectedKey: this.state.dynamicErrorValue, errorMessage: this._getErrorMessage(this.state.dynamicErrorValue), options: INITIAL_OPTIONS }),
            React.createElement(ComboBox, { disabled: true, label: "Disabled ComboBox", defaultSelectedKey: "D", options: INITIAL_OPTIONS })));
    };
    ComboBoxBasicExample.prototype._getErrorMessage = function (value) {
        if (value === 'B') {
            return 'B is not an allowed option!';
        }
        return '';
    };
    return ComboBoxBasicExample;
}(React.Component));
export { ComboBoxBasicExample };
//# sourceMappingURL=ComboBox.Basic.Example.js.map