import { __extends, __spreadArrays } from "tslib";
import * as React from 'react';
import { ComboBox, SelectableOptionMenuItemType, Fabric, mergeStyles, } from 'office-ui-fabric-react/lib/index';
var wrapperClassName = mergeStyles({
    selectors: {
        '& > *': { marginBottom: '20px' },
        '& .ms-ComboBox': { maxWidth: '300px' },
    },
});
var ComboBoxCustomStyledExample = /** @class */ (function (_super) {
    __extends(ComboBoxCustomStyledExample, _super);
    function ComboBoxCustomStyledExample(props) {
        var _a;
        var _this = _super.call(this, props) || this;
        _this._fontMapping = (_a = {},
            _a['Arial Black'] = '"Arial Black", "Arial Black_MSFontService", sans-serif',
            _a['Times New Roman'] = '"Times New Roman", "Times New Roman_MSFontService", serif',
            _a['Comic Sans MS'] = '"Comic Sans MS", "Comic Sans MS_MSFontService", fantasy',
            _a['Calibri'] = 'Calibri, Calibri_MSFontService, sans-serif',
            _a);
        /**
         * Render function for non-header/divider options in the second example.
         */
        _this._onRenderOption = function (item) {
            if (item.itemType === SelectableOptionMenuItemType.Header ||
                item.itemType === SelectableOptionMenuItemType.Divider) {
                return React.createElement("span", null, item.text);
            }
            var fontFamily = _this._fontMapping[item.text];
            if (!fontFamily) {
                // This is a new user-entered font. Add a font family definition for it.
                var newFontName = item.text;
                fontFamily = _this._fontMapping[newFontName] = "\"" + newFontName + "\",\"Segoe UI\",Tahoma,Sans-Serif";
            }
            return React.createElement("span", { style: { fontFamily: fontFamily } }, item.text);
        };
        var fonts = Object.keys(_this._fontMapping);
        // Options for first example
        _this._optionsWithCustomStyling = fonts.map(function (fontName) { return ({
            key: fontName,
            text: fontName,
            styles: {
                optionText: {
                    // This will cause the options to render with the given font
                    fontFamily: _this._fontMapping[fontName],
                },
            },
        }); });
        // Options for second example
        _this._optionsForCustomRender = __spreadArrays([
            // Default font options are listed under this heading
            { key: 'header1', text: 'Theme Fonts', itemType: SelectableOptionMenuItemType.Header }
        ], fonts.map(function (fontName) { return ({ key: fontName, text: fontName }); }), [
            { key: 'divider', text: '-', itemType: SelectableOptionMenuItemType.Divider },
            // User-added font options will be listed under this heading
            { key: 'header2', text: 'Other Options', itemType: SelectableOptionMenuItemType.Header },
        ]);
        return _this;
    }
    ComboBoxCustomStyledExample.prototype.render = function () {
        return (React.createElement(Fabric, { className: wrapperClassName },
            React.createElement(ComboBox, { defaultSelectedKey: "Calibri", label: "Custom styled ComboBox", options: this._optionsWithCustomStyling, styles: {
                    container: {
                        maxWidth: '300px',
                    },
                    // Light purple input background
                    root: {
                        backgroundColor: '#b4a0ff',
                    },
                    input: {
                        backgroundColor: '#b4a0ff',
                    },
                }, caretDownButtonStyles: {
                    // Purple caret button with white text on hover or press
                    rootHovered: {
                        color: 'white',
                        backgroundColor: '#5c2d91',
                    },
                    rootChecked: {
                        color: 'white',
                        backgroundColor: '#5c2d91',
                    },
                    rootCheckedHovered: {
                        color: 'white',
                        backgroundColor: '#32145a',
                    },
                }, comboBoxOptionStyles: {
                    optionText: {
                        fontFamily: 'initial',
                    },
                } }),
            React.createElement(ComboBox, { defaultSelectedKey: "Calibri", label: 'ComboBox with custom option rendering (type the name of a font and the option will render in that font)', allowFreeform: true, autoComplete: "on", options: this._optionsForCustomRender, onRenderOption: this._onRenderOption })));
    };
    return ComboBoxCustomStyledExample;
}(React.Component));
export { ComboBoxCustomStyledExample };
//# sourceMappingURL=ComboBox.CustomStyled.Example.js.map