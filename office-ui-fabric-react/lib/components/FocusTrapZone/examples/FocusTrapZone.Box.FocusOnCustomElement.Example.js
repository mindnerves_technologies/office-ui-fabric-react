import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { FocusTrapZone } from 'office-ui-fabric-react/lib/FocusTrapZone';
import { Link } from 'office-ui-fabric-react/lib/Link';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
var _focusClassName = 'shouldFocusInput';
var FocusTrapZoneBoxCustomElementExample = /** @class */ (function (_super) {
    __extends(FocusTrapZoneBoxCustomElementExample, _super);
    function FocusTrapZoneBoxCustomElementExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            useTrapZone: false,
        };
        _this._toggle = React.createRef();
        _this._onButtonClickHandler = function () {
            _this.setState({
                useTrapZone: true,
            });
        };
        _this._onFocusTrapZoneToggleChanged = function (ev, checked) {
            _this.setState({ useTrapZone: !!checked });
        };
        return _this;
    }
    FocusTrapZoneBoxCustomElementExample.prototype.render = function () {
        var useTrapZone = this.state.useTrapZone;
        return (React.createElement(Stack, { tokens: { childrenGap: 8 } },
            React.createElement(Stack.Item, null,
                React.createElement(Text, null, "If this button is used to enable FocusTrapZone, the hyperlink should be focused.")),
            React.createElement(Stack.Item, null,
                React.createElement(DefaultButton, { onClick: this._onButtonClickHandler, text: "Focus Custom Element" })),
            React.createElement(FocusTrapZone, { disabled: !useTrapZone, firstFocusableSelector: _focusClassName },
                React.createElement(Stack, { horizontalAlign: "start", tokens: { childrenGap: 15 }, styles: {
                        root: { border: "2px solid " + (useTrapZone ? '#ababab' : 'transparent'), padding: 10 },
                    } },
                    React.createElement(Toggle, { label: "Use trap zone", componentRef: this._toggle, checked: useTrapZone, onChange: this._onFocusTrapZoneToggleChanged, onText: "On (toggle to exit)", offText: "Off" }),
                    React.createElement(TextField, { label: "Input inside trap zone", styles: { root: { width: 300 } } }),
                    React.createElement(Link, { href: "https://bing.com", className: _focusClassName, target: "_blank" }, "Hyperlink which will receive initial focus when trap zone is activated")))));
    };
    return FocusTrapZoneBoxCustomElementExample;
}(React.Component));
export { FocusTrapZoneBoxCustomElementExample };
//# sourceMappingURL=FocusTrapZone.Box.FocusOnCustomElement.Example.js.map