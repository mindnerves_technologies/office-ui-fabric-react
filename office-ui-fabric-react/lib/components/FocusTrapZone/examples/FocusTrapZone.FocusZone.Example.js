import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { FocusTrapZone } from 'office-ui-fabric-react/lib/FocusTrapZone';
import { FocusZone, FocusZoneDirection } from 'office-ui-fabric-react/lib/FocusZone';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
var FocusTrapZoneFocusZoneExample = /** @class */ (function (_super) {
    __extends(FocusTrapZoneFocusZoneExample, _super);
    function FocusTrapZoneFocusZoneExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            useTrapZone: false,
        };
        _this._toggle = React.createRef();
        _this._onFocusTrapZoneToggleChanged = function (ev, checked) {
            _this.setState({ useTrapZone: !!checked }, function () {
                // Restore focus to toggle after re-rendering
                _this._toggle.current.focus();
            });
        };
        return _this;
    }
    FocusTrapZoneFocusZoneExample.prototype.render = function () {
        var useTrapZone = this.state.useTrapZone;
        var padding = 10;
        var border = '2px dashed #ababab';
        var rootBorder = "2px solid " + (useTrapZone ? '#ababab' : 'transparent');
        var tokens = { childrenGap: 10 };
        return (React.createElement(FocusTrapZone, { disabled: !useTrapZone, forceFocusInsideTrap: true, focusPreviouslyFocusedInnerElement: true },
            React.createElement(Stack, { tokens: tokens, horizontalAlign: "start", styles: { root: { border: rootBorder, padding: padding } } },
                React.createElement(Toggle, { label: "Use trap zone", componentRef: this._toggle, checked: useTrapZone, onChange: this._onFocusTrapZoneToggleChanged, onText: "On (toggle to exit)", offText: "Off" }),
                React.createElement(FocusZone, { direction: FocusZoneDirection.horizontal, "data-is-visible": true },
                    React.createElement(Stack, { horizontal: true, tokens: tokens, styles: { root: { border: border, padding: padding } } },
                        React.createElement(DefaultButton, { text: "FZ1" }),
                        React.createElement(DefaultButton, { text: "FZ1" }),
                        React.createElement(DefaultButton, { text: "FZ1" }))),
                React.createElement(DefaultButton, { text: "No FZ" }),
                React.createElement(FocusZone, { direction: FocusZoneDirection.horizontal, "data-is-visible": true },
                    React.createElement(Stack, { horizontal: true, tokens: tokens, styles: { root: { border: border, padding: padding } } },
                        React.createElement(DefaultButton, { text: "FZ2" }),
                        React.createElement(DefaultButton, { text: "FZ2" }),
                        React.createElement(DefaultButton, { text: "FZ2" }))))));
    };
    return FocusTrapZoneFocusZoneExample;
}(React.Component));
export { FocusTrapZoneFocusZoneExample };
//# sourceMappingURL=FocusTrapZone.FocusZone.Example.js.map