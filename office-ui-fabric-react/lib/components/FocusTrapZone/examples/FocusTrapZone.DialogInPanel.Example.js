import { __extends } from "tslib";
import * as React from 'react';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import { Panel, PanelType } from 'office-ui-fabric-react/lib/Panel';
var FocusTrapZoneDialogInPanelExample = /** @class */ (function (_super) {
    __extends(FocusTrapZoneDialogInPanelExample, _super);
    function FocusTrapZoneDialogInPanelExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            hideDialog: true,
            showPanel: false,
        };
        _this._showDialog = function () {
            _this.setState({ hideDialog: false });
        };
        _this._closeDialog = function () {
            _this.setState({ hideDialog: true });
        };
        _this._onClosePanel = function () {
            _this.setState({ showPanel: false });
        };
        _this._onShowPanel = function () {
            _this.setState({ showPanel: true });
        };
        return _this;
    }
    FocusTrapZoneDialogInPanelExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(DefaultButton, { text: "Open Panel", secondaryText: "Opens the Sample Panel", onClick: this._onShowPanel }),
            React.createElement(Panel, { isOpen: this.state.showPanel, type: PanelType.smallFixedFar, onDismiss: this._onClosePanel, headerText: "This panel makes use of FocusTrapZone. Focus should be trapped in the panel.", closeButtonAriaLabel: "Close" },
                React.createElement(DefaultButton, { text: "Open Dialog", secondaryText: "Opens the Sample Dialog", onClick: this._showDialog }),
                React.createElement(Dialog, { hidden: this.state.hideDialog, onDismiss: this._closeDialog, isBlocking: true, dialogContentProps: {
                        type: DialogType.normal,
                        title: 'This dialog also makes use of FocusTrapZone. Focus should be trapped in the dialog.',
                        subText: "Focus will move back to the panel if you press 'OK' or 'Cancel'.",
                    }, modalProps: {
                        titleAriaId: 'myLabelId',
                        subtitleAriaId: 'mySubTextId',
                        isBlocking: false,
                        styles: { main: { maxWidth: 450 } },
                    } },
                    React.createElement(DialogFooter, null,
                        React.createElement(PrimaryButton, { onClick: this._closeDialog, text: "OK" }),
                        React.createElement(DefaultButton, { onClick: this._closeDialog, text: "Cancel" }))))));
    };
    return FocusTrapZoneDialogInPanelExample;
}(React.Component));
export { FocusTrapZoneDialogInPanelExample };
//# sourceMappingURL=FocusTrapZone.DialogInPanel.Example.js.map