import { __extends } from "tslib";
import * as React from 'react';
import { FocusTrapZone } from 'office-ui-fabric-react/lib/FocusTrapZone';
import { Link } from 'office-ui-fabric-react/lib/Link';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
var FocusTrapZoneBoxClickExample = /** @class */ (function (_super) {
    __extends(FocusTrapZoneBoxClickExample, _super);
    function FocusTrapZoneBoxClickExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { useTrapZone: false };
        _this._toggle = React.createRef();
        _this._onFocusTrapZoneToggleChanged = function (ev, checked) {
            _this.setState({ useTrapZone: !!checked });
        };
        return _this;
    }
    FocusTrapZoneBoxClickExample.prototype.render = function () {
        var useTrapZone = this.state.useTrapZone;
        return (React.createElement(FocusTrapZone, { disabled: !useTrapZone, isClickableOutsideFocusTrap: true, forceFocusInsideTrap: false },
            React.createElement(Stack, { horizontalAlign: "start", tokens: { childrenGap: 15 }, styles: {
                    root: { border: "2px dashed " + (useTrapZone ? '#ababab' : 'transparent'), padding: 10 },
                } },
                React.createElement(Toggle, { label: "Use trap zone", componentRef: this._toggle, checked: useTrapZone, onChange: this._onFocusTrapZoneToggleChanged, onText: "On (toggle to exit)", offText: "Off" }),
                React.createElement(TextField, { label: "Input inside trap zone", styles: { root: { width: 300 } } }),
                React.createElement(Link, { href: "https://bing.com", target: "_blank" }, "Hyperlink inside trap zone"))));
    };
    return FocusTrapZoneBoxClickExample;
}(React.Component));
export { FocusTrapZoneBoxClickExample };
//# sourceMappingURL=FocusTrapZone.Box.Click.Example.js.map