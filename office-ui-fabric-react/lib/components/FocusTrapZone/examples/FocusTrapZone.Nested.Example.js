import { __assign, __extends } from "tslib";
import * as React from 'react';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { FocusTrapZone } from 'office-ui-fabric-react/lib/FocusTrapZone';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
var FocusTrapComponent = /** @class */ (function (_super) {
    __extends(FocusTrapComponent, _super);
    function FocusTrapComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onStringButtonClicked = function () {
            alert("Button " + _this.props.zoneNumber + " clicked");
        };
        _this._onFocusTrapZoneToggleChanged = function (ev, isChecked) {
            _this.props.setIsActive(_this.props.zoneNumber, isChecked);
        };
        return _this;
    }
    FocusTrapComponent.prototype.render = function () {
        var _a = this.props, isActive = _a.isActive, zoneNumber = _a.zoneNumber, children = _a.children;
        return (React.createElement(FocusTrapZone, { disabled: !isActive, forceFocusInsideTrap: false },
            React.createElement(Stack, { horizontalAlign: "start", tokens: { childrenGap: 10 }, styles: {
                    root: { border: "2px solid " + (isActive ? '#ababab' : 'transparent'), padding: 10 },
                } },
                React.createElement(Toggle, { checked: isActive, onChange: this._onFocusTrapZoneToggleChanged, label: 'Enable trap zone ' + zoneNumber, onText: "On (toggle to exit)", offText: "Off", styles: {
                        // Set a width on these toggles in the horizontal zone to prevent jumping when enabled
                        root: zoneNumber >= 2 && zoneNumber <= 4 && { width: 200 },
                    } }),
                React.createElement(DefaultButton, { onClick: this._onStringButtonClicked, text: "Zone " + zoneNumber + " button" }),
                children)));
    };
    return FocusTrapComponent;
}(React.Component));
var FocusTrapZoneNestedExample = /** @class */ (function (_super) {
    __extends(FocusTrapZoneNestedExample, _super);
    function FocusTrapZoneNestedExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = { activeStates: {} };
        _this._setIsActive = function (zoneNumber, isActive) {
            var _a;
            var activeStates = _this.state.activeStates;
            _this.setState({ activeStates: __assign(__assign({}, activeStates), (_a = {}, _a[zoneNumber] = isActive, _a)) });
        };
        return _this;
    }
    FocusTrapZoneNestedExample.prototype.render = function () {
        var activeStates = this.state.activeStates;
        return (React.createElement("div", null,
            React.createElement(FocusTrapComponent, { zoneNumber: 1, isActive: !!activeStates[1], setIsActive: this._setIsActive },
                React.createElement(FocusTrapComponent, { zoneNumber: 2, isActive: !!activeStates[2], setIsActive: this._setIsActive },
                    React.createElement(FocusTrapComponent, { zoneNumber: 3, isActive: !!activeStates[3], setIsActive: this._setIsActive }),
                    React.createElement(FocusTrapComponent, { zoneNumber: 4, isActive: !!activeStates[4], setIsActive: this._setIsActive })),
                React.createElement(FocusTrapComponent, { zoneNumber: 5, isActive: !!activeStates[5], setIsActive: this._setIsActive }))));
    };
    return FocusTrapZoneNestedExample;
}(React.Component));
export { FocusTrapZoneNestedExample };
//# sourceMappingURL=FocusTrapZone.Nested.Example.js.map