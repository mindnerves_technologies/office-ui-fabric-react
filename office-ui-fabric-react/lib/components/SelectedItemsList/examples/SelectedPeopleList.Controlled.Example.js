import { __assign, __extends, __spreadArrays } from "tslib";
import * as React from 'react';
import { PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import { SelectedPeopleList, ExtendedSelectedItem, } from 'office-ui-fabric-react/lib/SelectedItemsList';
import { Selection } from 'office-ui-fabric-react/lib/Selection';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { people, groupOne, groupTwo } from '@uifabric/example-data';
var SelectedPeopleListControlledExample = /** @class */ (function (_super) {
    __extends(SelectedPeopleListControlledExample, _super);
    function SelectedPeopleListControlledExample(props) {
        var _this = _super.call(this, props) || this;
        _this._selectionList = React.createRef();
        _this._onRenderItem = function (props) {
            return React.createElement(ExtendedSelectedItem, __assign({}, props));
        };
        _this._onAddItemButtonClicked = function () {
            var _a = _this.state, nextPersonIndex = _a.nextPersonIndex, currentSelectedItems = _a.currentSelectedItems;
            _this.setState({
                currentSelectedItems: __spreadArrays(currentSelectedItems, [people[nextPersonIndex]]),
                nextPersonIndex: nextPersonIndex + 1,
            });
        };
        _this._onItemDeleted = function (item) {
            var currentSelectedItems = _this.state.currentSelectedItems;
            var indexToRemove = currentSelectedItems.indexOf(item);
            var newSelectedItems = __spreadArrays(currentSelectedItems);
            newSelectedItems.splice(indexToRemove, 1);
            _this.setState({ currentSelectedItems: newSelectedItems });
        };
        _this._onExpandItem = function (item) {
            var currentSelectedItems = _this.state.currentSelectedItems;
            var expandedItem = item.text === 'Group One' ? groupOne : item.text === 'Group Two' ? groupTwo : [];
            var indexToExpand = currentSelectedItems.indexOf(item);
            _this.setState({
                currentSelectedItems: currentSelectedItems
                    .slice(0, indexToExpand)
                    .concat(expandedItem)
                    .concat(currentSelectedItems.slice(indexToExpand + 1)),
            });
        };
        _this.state = {
            currentSelectedItems: [people[40]],
            nextPersonIndex: 0,
        };
        _this._selection = new Selection({ onSelectionChanged: function () { return _this.forceUpdate(); } });
        return _this;
    }
    SelectedPeopleListControlledExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(PrimaryButton, { text: "Add another item", onClick: this._onAddItemButtonClicked, disabled: this.state.nextPersonIndex >= people.length, styles: { root: { display: 'block', marginBottom: 20 } } }),
            React.createElement(Stack, { horizontal: true, wrap: true, styles: { root: { maxWidth: '100%' } } },
                React.createElement(SelectedPeopleList, { key: "normal", removeButtonAriaLabel: "Remove", selectedItems: this.state.currentSelectedItems, componentRef: this._selectionList, onCopyItems: this._onCopyItems, onExpandGroup: this._onExpandItem, copyMenuItemText: "Copy", removeMenuItemText: "Remove", selection: this._selection, onRenderItem: this._onRenderItem, onItemDeleted: this._onItemDeleted }))));
    };
    SelectedPeopleListControlledExample.prototype._onCopyItems = function (items) {
        return items.map(function (item) { return item.text; }).join(', ');
    };
    return SelectedPeopleListControlledExample;
}(React.Component));
export { SelectedPeopleListControlledExample };
//# sourceMappingURL=SelectedPeopleList.Controlled.Example.js.map