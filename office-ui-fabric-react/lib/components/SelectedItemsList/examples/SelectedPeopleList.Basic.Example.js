import { __assign, __extends } from "tslib";
import * as React from 'react';
import { PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import { SelectedPeopleList, ExtendedSelectedItem, } from 'office-ui-fabric-react/lib/SelectedItemsList';
import { Selection } from 'office-ui-fabric-react/lib/Selection';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { people, groupOne, groupTwo } from '@uifabric/example-data';
var SelectedPeopleListBasicExample = /** @class */ (function (_super) {
    __extends(SelectedPeopleListBasicExample, _super);
    function SelectedPeopleListBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._selectionList = React.createRef();
        _this._onRenderItem = function (props) {
            return React.createElement(ExtendedSelectedItem, __assign({}, props));
        };
        _this._onAddItemButtonClicked = function () {
            if (_this._selectionList.current) {
                var nextPersonIndex = _this.state.nextPersonIndex;
                _this._selectionList.current.addItems([people[nextPersonIndex]]);
                _this.setState({ nextPersonIndex: nextPersonIndex + 1 });
            }
        };
        _this._onExpandItem = function (item) {
            var expandedItem = item.text === 'Group One' ? groupOne : item.text === 'Group Two' ? groupTwo : [];
            _this._selectionList.current.replaceItem(item, expandedItem);
        };
        _this.state = {
            nextPersonIndex: 0,
        };
        _this._selection = new Selection({ onSelectionChanged: function () { return _this.forceUpdate(); } });
        return _this;
    }
    SelectedPeopleListBasicExample.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(PrimaryButton, { text: "Add another item", onClick: this._onAddItemButtonClicked, disabled: this.state.nextPersonIndex >= people.length, styles: { root: { display: 'block', marginBottom: 20 } } }),
            React.createElement(Stack, { horizontal: true, wrap: true },
                React.createElement(SelectedPeopleList, { key: "normal", removeButtonAriaLabel: "Remove", defaultSelectedItems: [people[40]], componentRef: this._selectionList, onCopyItems: this._onCopyItems, onExpandGroup: this._onExpandItem, copyMenuItemText: "Copy", removeMenuItemText: "Remove", selection: this._selection, onRenderItem: this._onRenderItem }))));
    };
    SelectedPeopleListBasicExample.prototype._onCopyItems = function (items) {
        return items.map(function (item) { return item.text; }).join(', ');
    };
    return SelectedPeopleListBasicExample;
}(React.Component));
export { SelectedPeopleListBasicExample };
//# sourceMappingURL=SelectedPeopleList.Basic.Example.js.map