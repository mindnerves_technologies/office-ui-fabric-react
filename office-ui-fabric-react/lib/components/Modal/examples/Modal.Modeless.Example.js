import { __extends } from "tslib";
import * as React from 'react';
import { Modal, getTheme, mergeStyleSets, FontWeights, DefaultButton, Checkbox, ContextualMenu, IconButton, } from 'office-ui-fabric-react';
import { FontSizes } from '@uifabric/styling';
import { getId } from 'office-ui-fabric-react/lib/Utilities';
// Themed styles for the example.
var theme = getTheme();
var contentStyles = mergeStyleSets({
    container: {
        display: 'flex',
        flexFlow: 'column nowrap',
        alignItems: 'stretch',
    },
    header: [
        // tslint:disable-next-line:deprecation
        theme.fonts.xLargePlus,
        {
            flex: '1 1 auto',
            borderTop: "4px solid " + theme.palette.themePrimary,
            color: theme.palette.neutralPrimary,
            display: 'flex',
            fontSize: FontSizes.xLarge,
            alignItems: 'center',
            fontWeight: FontWeights.semibold,
            padding: '12px 12px 14px 24px',
        },
    ],
    body: {
        flex: '4 4 auto',
        padding: '0 24px 24px 24px',
        overflowY: 'hidden',
        selectors: {
            p: {
                margin: '14px 0',
            },
            'p:first-child': {
                marginTop: 0,
            },
            'p:last-child': {
                marginBottom: 0,
            },
        },
    },
});
var iconButtonStyles = mergeStyleSets({
    root: {
        color: theme.palette.neutralPrimary,
        marginLeft: 'auto',
        marginTop: '4px',
        marginRight: '2px',
    },
    rootHovered: {
        color: theme.palette.neutralDark,
    },
});
var ModalModelessExample = /** @class */ (function (_super) {
    __extends(ModalModelessExample, _super);
    function ModalModelessExample() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            showModal: false,
            isDraggable: false,
        };
        // Use getId() to ensure that the IDs are unique on the page.
        // (It's also okay to use plain strings without getId() and manually ensure uniqueness.)
        _this._titleId = getId('title');
        _this._subtitleId = getId('subText');
        _this._dragOptions = {
            moveMenuItemText: 'Move',
            closeMenuItemText: 'Close',
            menu: ContextualMenu,
        };
        _this._showModal = function () {
            _this.setState({ showModal: true });
        };
        _this._closeModal = function () {
            _this.setState({ showModal: false });
        };
        _this._toggleDraggable = function () {
            _this.setState({ isDraggable: !_this.state.isDraggable });
        };
        return _this;
    }
    ModalModelessExample.prototype.render = function () {
        var _a = this.state, showModal = _a.showModal, isDraggable = _a.isDraggable;
        return (React.createElement("div", null,
            React.createElement(Checkbox, { styles: { root: { marginBottom: '20px' } }, label: "Is draggable", onChange: this._toggleDraggable, checked: isDraggable, disabled: showModal }),
            React.createElement(DefaultButton, { secondaryText: "Opens the Sample Modal", onClick: this._showModal, text: "Open Modal" }),
            React.createElement(Modal, { titleAriaId: this._titleId, subtitleAriaId: this._subtitleId, isOpen: showModal, onDismiss: this._closeModal, isModeless: true, containerClassName: contentStyles.container, dragOptions: isDraggable ? this._dragOptions : undefined },
                React.createElement("div", { className: contentStyles.header },
                    React.createElement("span", { id: this._titleId }, "Lorem Ipsum"),
                    React.createElement(IconButton, { styles: iconButtonStyles, iconProps: { iconName: 'Cancel' }, ariaLabel: "Close popup modal", onClick: this._closeModal })),
                React.createElement("div", { id: this._subtitleId, className: contentStyles.body },
                    React.createElement("p", null,
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem nulla, malesuada ut sagittis sit amet, vulputate in leo. Maecenas vulputate congue sapien eu tincidunt. Etiam eu sem turpis. Fusce tempor sagittis nunc, ut interdum ipsum vestibulum non. Proin dolor elit, aliquam eget tincidunt non, vestibulum ut turpis. In hac habitasse platea dictumst. In a odio eget enim porttitor maximus. Aliquam nulla nibh, ullamcorper aliquam placerat eu, viverra et dui. Phasellus ex lectus, maximus in mollis ac, luctus vel eros. Vivamus ultrices, turpis sed malesuada gravida, eros ipsum venenatis elit, et volutpat eros dui et ante. Quisque ultricies mi nec leo ultricies mollis. Vivamus egestas volutpat lacinia. Quisque pharetra eleifend efficitur.",
                        ' ')))));
    };
    return ModalModelessExample;
}(React.Component));
export { ModalModelessExample };
//# sourceMappingURL=Modal.Modeless.Example.js.map