import * as React from 'react';
import { DayOfWeek } from 'office-ui-fabric-react/lib/DatePicker';
export interface IDatePickerBasicExampleState {
    firstDayOfWeek?: DayOfWeek;
}
export declare class DatePickerWeekNumbersExample extends React.Component<{}, IDatePickerBasicExampleState> {
    constructor(props: {});
    render(): JSX.Element;
    private _onDropdownChange;
}
