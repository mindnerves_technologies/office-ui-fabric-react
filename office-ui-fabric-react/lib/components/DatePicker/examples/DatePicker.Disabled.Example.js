import { __extends } from "tslib";
import * as React from 'react';
import { DatePicker, DayOfWeek } from 'office-ui-fabric-react/lib/DatePicker';
import { mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
var DayPickerStrings = {
    months: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ],
    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    goToToday: 'Go to today',
    prevMonthAriaLabel: 'Go to previous month',
    nextMonthAriaLabel: 'Go to next month',
    prevYearAriaLabel: 'Go to previous year',
    nextYearAriaLabel: 'Go to next year',
    closeButtonAriaLabel: 'Close date picker',
};
var controlClass = mergeStyleSets({
    control: {
        margin: '0 0 15px 0',
        maxWidth: '300px',
    },
});
var DatePickerDisabledExample = /** @class */ (function (_super) {
    __extends(DatePickerDisabledExample, _super);
    function DatePickerDisabledExample(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            firstDayOfWeek: DayOfWeek.Sunday,
        };
        return _this;
    }
    DatePickerDisabledExample.prototype.render = function () {
        var firstDayOfWeek = this.state.firstDayOfWeek;
        return (React.createElement("div", { className: "docs-DatePickerExample" },
            React.createElement(DatePicker, { className: controlClass.control, firstDayOfWeek: firstDayOfWeek, strings: DayPickerStrings, placeholder: "Select a date...", ariaLabel: "Select a date", disabled: true }),
            React.createElement(DatePicker, { className: controlClass.control, label: "Disabled (with label)", firstDayOfWeek: firstDayOfWeek, strings: DayPickerStrings, placeholder: "Select a date...", ariaLabel: "Select a date", disabled: true })));
    };
    return DatePickerDisabledExample;
}(React.Component));
export { DatePickerDisabledExample };
//# sourceMappingURL=DatePicker.Disabled.Example.js.map