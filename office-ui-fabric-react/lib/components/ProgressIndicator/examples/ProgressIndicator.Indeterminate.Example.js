import { __extends } from "tslib";
import * as React from 'react';
import { ProgressIndicator } from 'office-ui-fabric-react/lib/ProgressIndicator';
var ProgressIndicatorIndeterminateExample = /** @class */ (function (_super) {
    __extends(ProgressIndicatorIndeterminateExample, _super);
    function ProgressIndicatorIndeterminateExample(props) {
        return _super.call(this, props) || this;
    }
    ProgressIndicatorIndeterminateExample.prototype.render = function () {
        return React.createElement(ProgressIndicator, { label: "Example title", description: "Example description" });
    };
    return ProgressIndicatorIndeterminateExample;
}(React.Component));
export { ProgressIndicatorIndeterminateExample };
//# sourceMappingURL=ProgressIndicator.Indeterminate.Example.js.map