/* tslint:disable */
import { loadStyles } from '@microsoft/load-themed-styles';
loadStyles([{ "rawString": ".root_e2dcdb7c{overflow-y:auto;max-height:inherit;height:inherit;-webkit-overflow-scrolling:touch}.stickyAbove_e2dcdb7c,.stickyBelow_e2dcdb7c{position:absolute;pointer-events:auto;width:100%;z-index:1}.stickyAbove_e2dcdb7c{top:0}@media screen and (-ms-high-contrast: active){.stickyAbove_e2dcdb7c{border-bottom:1px solid WindowText}}.stickyBelow_e2dcdb7c{bottom:0}@media screen and (-ms-high-contrast: active){.stickyBelow_e2dcdb7c{border-top:1px solid WindowText}}\n" }]);
export var root = "root_e2dcdb7c";
export var stickyAbove = "stickyAbove_e2dcdb7c";
export var stickyBelow = "stickyBelow_e2dcdb7c";
//# sourceMappingURL=ScrollablePane.scss.js.map