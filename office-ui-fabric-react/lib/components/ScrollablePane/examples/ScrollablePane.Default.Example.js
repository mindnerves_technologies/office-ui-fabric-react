import { __extends } from "tslib";
import * as React from 'react';
import { getTheme, mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
import { lorem } from '@uifabric/example-data';
import { ScrollablePane } from 'office-ui-fabric-react/lib/ScrollablePane';
import { Sticky, StickyPositionType } from 'office-ui-fabric-react/lib/Sticky';
var theme = getTheme();
var classNames = mergeStyleSets({
    wrapper: {
        height: '40vh',
        position: 'relative',
        maxHeight: 'inherit',
    },
    pane: {
        maxWidth: 400,
        border: '1px solid ' + theme.palette.neutralLight,
    },
    sticky: {
        color: theme.palette.neutralDark,
        padding: '5px 20px 5px 10px',
        fontSize: '13px',
        borderTop: '1px solid ' + theme.palette.black,
        borderBottom: '1px solid ' + theme.palette.black,
    },
    textContent: {
        padding: '15px 10px',
    },
});
var ScrollablePaneDefaultExample = /** @class */ (function (_super) {
    __extends(ScrollablePaneDefaultExample, _super);
    function ScrollablePaneDefaultExample(props) {
        var _this = _super.call(this, props) || this;
        _this._createContentArea = function (item) {
            return (React.createElement("div", { key: item.index, style: {
                    backgroundColor: item.color,
                } },
                React.createElement(Sticky, { stickyPosition: StickyPositionType.Both },
                    React.createElement("div", { className: classNames.sticky },
                        "Sticky Component #",
                        item.index + 1)),
                React.createElement("div", { className: classNames.textContent }, item.text)));
        };
        var colors = ['#eaeaea', '#dadada', '#d0d0d0', '#c8c8c8', '#a6a6a6', '#c7e0f4', '#71afe5', '#eff6fc', '#deecf9'];
        _this._items = [];
        // Using splice prevents the colors from being duplicated
        for (var i = 0; i < 5; i++) {
            _this._items.push({
                color: colors.splice(Math.floor(Math.random() * colors.length), 1)[0],
                text: lorem(200),
                index: i,
            });
        }
        return _this;
    }
    ScrollablePaneDefaultExample.prototype.render = function () {
        var contentAreas = this._items.map(this._createContentArea);
        return (React.createElement("div", { className: classNames.wrapper },
            React.createElement(ScrollablePane, { styles: { root: classNames.pane } }, contentAreas)));
    };
    return ScrollablePaneDefaultExample;
}(React.Component));
export { ScrollablePaneDefaultExample };
//# sourceMappingURL=ScrollablePane.Default.Example.js.map