import { __extends } from "tslib";
import * as React from 'react';
import { Rating } from 'office-ui-fabric-react/lib/Rating';
import { PrimaryButton } from 'office-ui-fabric-react/lib/Button';
var RatingButtonControlledExample = /** @class */ (function (_super) {
    __extends(RatingButtonControlledExample, _super);
    function RatingButtonControlledExample(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            rating: 0,
        };
        return _this;
    }
    // tslint:disable:jsx-no-lambda
    RatingButtonControlledExample.prototype.render = function () {
        var _this = this;
        var maxrating = 5;
        return (React.createElement("div", { className: "ms-RatingButtonControlledExample" },
            React.createElement(Rating, { rating: this.state.rating, max: 5, readOnly: true, allowZeroStars: true, getAriaLabel: this._getRatingComponentAriaLabel, ariaLabelFormat: 'Select {0} of {1} stars' }),
            React.createElement(PrimaryButton, { text: 'Click to change rating to ' + (maxrating - this.state.rating), onClick: function (e) {
                    if (_this.state.rating === 0) {
                        _this.setState({ rating: 5 });
                    }
                    else {
                        _this.setState({ rating: 0 });
                    }
                } })));
    };
    RatingButtonControlledExample.prototype._getRatingComponentAriaLabel = function (rating, maxRating) {
        return "Rating value is " + rating + " of " + maxRating;
    };
    return RatingButtonControlledExample;
}(React.Component));
export { RatingButtonControlledExample };
//# sourceMappingURL=Rating.ButtonControlled.Example.js.map