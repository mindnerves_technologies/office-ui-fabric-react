import * as React from 'react';
export declare class RatingButtonControlledExample extends React.Component<{}, {
    rating: number;
}> {
    constructor(props: {});
    render(): JSX.Element;
    private _getRatingComponentAriaLabel;
}
