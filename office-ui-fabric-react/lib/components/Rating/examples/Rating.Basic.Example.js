import { __extends } from "tslib";
import * as React from 'react';
import { Rating, RatingSize } from 'office-ui-fabric-react/lib/Rating';
import { getTheme, createTheme } from 'office-ui-fabric-react/lib/Styling';
var RatingBasicExample = /** @class */ (function (_super) {
    __extends(RatingBasicExample, _super);
    function RatingBasicExample(props) {
        var _this = _super.call(this, props) || this;
        _this._onFocus = function () {
            console.log('onFocus called');
        };
        _this._onBlur = function () {
            console.log('onBlur called');
        };
        _this._onLargeStarChange = function (ev, rating) {
            _this.setState({ largeStarRating: rating });
        };
        _this._onSmallStarChange = function (ev, rating) {
            _this.setState({ smallStarRating: rating });
        };
        _this._onTenStarChange = function (ev, rating) {
            _this.setState({ tenStarRating: rating });
        };
        _this._onThemedStarChange = function (ev, rating) {
            _this.setState({ themedStarRating: rating });
        };
        _this._onCustomIconStarChange = function (ev, rating) {
            _this.setState({ customIconStarRating: rating });
        };
        _this.state = {
            largeStarRating: undefined,
            smallStarRating: 3,
            tenStarRating: undefined,
            themedStarRating: undefined,
            customIconStarRating: 2.5,
        };
        _this._customTheme = createTheme(getTheme());
        _this._customTheme.semanticColors.bodySubtext = '#DFDFDF';
        _this._customTheme.semanticColors.bodyTextChecked = '#1E9FE8';
        return _this;
    }
    RatingBasicExample.prototype.render = function () {
        return (React.createElement("div", { className: "ms-RatingBasicExample" },
            "Large Stars:",
            React.createElement(Rating, { min: 1, max: 5, size: RatingSize.Large, rating: this.state.largeStarRating, getAriaLabel: this._getRatingComponentAriaLabel, onChange: this._onLargeStarChange, onFocus: this._onFocus, onBlur: this._onBlur, ariaLabelFormat: 'Select {0} of {1} stars' }),
            "Small Stars",
            React.createElement(Rating, { min: 1, max: 5, rating: this.state.smallStarRating, onChange: this._onSmallStarChange, getAriaLabel: this._getRatingComponentAriaLabel, onFocus: this._onFocus, onBlur: this._onBlur, ariaLabelFormat: 'Select {0} of {1} stars' }),
            "10 Small Stars",
            React.createElement(Rating, { min: 1, max: 10, rating: this.state.tenStarRating, onChange: this._onTenStarChange, getAriaLabel: this._getRatingComponentAriaLabel, onFocus: this._onFocus, onBlur: this._onBlur, ariaLabelFormat: 'Select {0} of {1} stars' }),
            "Disabled:",
            React.createElement(Rating, { min: 1, max: 5, rating: this.state.rating, disabled: true, onFocus: this._onFocus, onBlur: this._onBlur, ariaLabelFormat: 'Select {0} of {1} stars' }),
            "Half star in readOnly mode:",
            React.createElement(Rating, { min: 1, max: 5, rating: 2.5, getAriaLabel: this._getRatingComponentAriaLabel, readOnly: true, ariaLabelFormat: 'Select {0} of {1} stars' }),
            "Custom icons:",
            React.createElement(Rating, { min: 1, max: 5, rating: this.state.customIconStarRating, onChange: this._onCustomIconStarChange, onFocus: this._onFocus, onBlur: this._onBlur, getAriaLabel: this._getRatingComponentAriaLabel, ariaLabelFormat: 'Select {0} of {1} stars', icon: "StarburstSolid", unselectedIcon: "Starburst" }),
            "Themed star",
            React.createElement(Rating, { min: 1, max: 5, rating: this.state.themedStarRating, onChange: this._onThemedStarChange, getAriaLabel: this._getRatingComponentAriaLabel, onFocus: this._onFocus, onBlur: this._onBlur, ariaLabelFormat: 'Select {0} of {1} stars', theme: this._customTheme })));
    };
    RatingBasicExample.prototype._getRatingComponentAriaLabel = function (rating, maxRating) {
        return "Rating value is " + rating + " of " + maxRating;
    };
    return RatingBasicExample;
}(React.Component));
export { RatingBasicExample };
//# sourceMappingURL=Rating.Basic.Example.js.map