// office-ui-fabric-react@7.105.4
// Do not modify this file, the file is generated as part of publish. The checked in version is a placeholder only.
import { setVersion } from '@uifabric/set-version';
setVersion('office-ui-fabric-react', '7.105.4');
//# sourceMappingURL=version.js.map