define(["require", "exports", "./hsv2rgb", "./rgb2hex", "./_rgbaOrHexString"], function (require, exports, hsv2rgb_1, rgb2hex_1, _rgbaOrHexString_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * Gets a color with the same saturation and value as `color` and the other components updated
     * to match the given hue.
     *
     * Does not modify the original `color` and does not supply a default alpha value.
     */
    function updateH(color, h) {
        var _a = hsv2rgb_1.hsv2rgb(h, color.s, color.v), r = _a.r, g = _a.g, b = _a.b;
        var hex = rgb2hex_1.rgb2hex(r, g, b);
        return {
            a: color.a,
            b: b,
            g: g,
            h: h,
            hex: hex,
            r: r,
            s: color.s,
            str: _rgbaOrHexString_1._rgbaOrHexString(r, g, b, color.a, hex),
            v: color.v,
        };
    }
    exports.updateH = updateH;
});
//# sourceMappingURL=updateH.js.map