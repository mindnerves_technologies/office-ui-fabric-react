import * as React from 'react';
export declare class OverflowSetBasicReversedExample extends React.PureComponent {
    render(): JSX.Element;
    private _onRenderItem;
    private _onRenderOverflowButton;
}
