define(["require", "exports", "tslib", "react", "office-ui-fabric-react/lib/Checkbox", "office-ui-fabric-react/lib/Dropdown", "office-ui-fabric-react/lib/Facepile", "office-ui-fabric-react/lib/Persona", "office-ui-fabric-react/lib/Slider", "@uifabric/example-data", "office-ui-fabric-react/lib/Styling"], function (require, exports, tslib_1, React, Checkbox_1, Dropdown_1, Facepile_1, Persona_1, Slider_1, example_data_1, Styling_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var styles = Styling_1.mergeStyleSets({
        container: {
            maxWidth: 300,
        },
        control: {
            paddingTop: 20,
        },
        slider: {
            margin: '10px 0',
        },
        checkbox: {
            paddingTop: 15,
        },
        dropdown: {
            paddingTop: 0,
            margin: '10px 0',
        },
    });
    var FacepileBasicExample = /** @class */ (function (_super) {
        tslib_1.__extends(FacepileBasicExample, _super);
        function FacepileBasicExample(props) {
            var _this = _super.call(this, props) || this;
            _this._onChangeFadeIn = function (ev, checked) {
                _this.setState(function (prevState) {
                    prevState.imagesFadeIn = checked;
                    return prevState;
                });
            };
            _this._onChangePersonaNumber = function (value) {
                _this.setState(function (prevState) {
                    prevState.numberOfFaces = value;
                    return prevState;
                });
            };
            _this._onChangePersonaSize = function (event, value) {
                _this.setState(function (prevState) {
                    prevState.personaSize = value.key;
                    return prevState;
                });
            };
            _this._personaPresence = function (personaName) {
                var presences = [
                    Persona_1.PersonaPresence.away,
                    Persona_1.PersonaPresence.busy,
                    Persona_1.PersonaPresence.online,
                    Persona_1.PersonaPresence.offline,
                    Persona_1.PersonaPresence.offline,
                ];
                return presences[personaName.charCodeAt(1) % 5];
            };
            _this.state = {
                numberOfFaces: 3,
                imagesFadeIn: true,
                personaSize: Persona_1.PersonaSize.size32,
            };
            return _this;
        }
        /**
         * Note: The implementation of presence below is simply for demonstration purposes.
         * Typically, the persona presence should be included when generating each facepile persona.
         */
        FacepileBasicExample.prototype.render = function () {
            var _this = this;
            var _a = this.state, numberOfFaces = _a.numberOfFaces, personaSize = _a.personaSize;
            var facepileProps = {
                personaSize: personaSize,
                personas: example_data_1.facepilePersonas.slice(0, numberOfFaces),
                overflowPersonas: example_data_1.facepilePersonas.slice(numberOfFaces),
                getPersonaProps: function (persona) {
                    return {
                        imageShouldFadeIn: _this.state.imagesFadeIn,
                        presence: _this._personaPresence(persona.personaName),
                    };
                },
                ariaDescription: 'To move through the items use left and right arrow keys.',
                ariaLabel: 'Example list of Facepile personas',
            };
            return (React.createElement("div", { className: styles.container },
                React.createElement(Facepile_1.Facepile, tslib_1.__assign({}, facepileProps)),
                React.createElement("div", { className: styles.control },
                    React.createElement(Slider_1.Slider, { label: "Number of Personas:", className: styles.slider, min: 1, max: 5, step: 1, showValue: true, value: numberOfFaces, onChange: this._onChangePersonaNumber }),
                    React.createElement(Dropdown_1.Dropdown, { label: "Persona Size:", selectedKey: this.state.personaSize, className: styles.dropdown, options: [
                            { key: Persona_1.PersonaSize.size8, text: Persona_1.PersonaSize[Persona_1.PersonaSize.size8] },
                            { key: Persona_1.PersonaSize.size24, text: Persona_1.PersonaSize[Persona_1.PersonaSize.size24] },
                            { key: Persona_1.PersonaSize.size32, text: Persona_1.PersonaSize[Persona_1.PersonaSize.size32] },
                            { key: Persona_1.PersonaSize.size40, text: Persona_1.PersonaSize[Persona_1.PersonaSize.size40] },
                            { key: Persona_1.PersonaSize.size48, text: Persona_1.PersonaSize[Persona_1.PersonaSize.size48] },
                        ], onChange: this._onChangePersonaSize }),
                    React.createElement(Checkbox_1.Checkbox, { className: styles.checkbox, styles: { root: { margin: '10px 0' } }, label: "Fade In", checked: this.state.imagesFadeIn, onChange: this._onChangeFadeIn }))));
        };
        return FacepileBasicExample;
    }(React.Component));
    exports.FacepileBasicExample = FacepileBasicExample;
});
//# sourceMappingURL=Facepile.Basic.Example.js.map