define(["require", "exports", "@microsoft/load-themed-styles"], function (require, exports, load_themed_styles_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    load_themed_styles_1.loadStyles([{ "rawString": ".editingInput_7dfbc845{border:0px;outline:none;width:100%}.editingInput_7dfbc845::-ms-clear{display:none}.editingContainer_7dfbc845{margin:4px}\n" }]);
    exports.editingInput = "editingInput_7dfbc845";
    exports.editingContainer = "editingContainer_7dfbc845";
});
//# sourceMappingURL=EditingItem.scss.js.map