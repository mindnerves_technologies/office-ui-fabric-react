define(["require", "exports", "@microsoft/load-themed-styles"], function (require, exports, load_themed_styles_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    load_themed_styles_1.loadStyles([{ "rawString": ".callout_8212a800 .ms-Suggestions-itemButton{padding:0px;border:none}.callout_8212a800 .ms-Suggestions{min-width:300px}\n" }]);
    exports.callout = "callout_8212a800";
});
//# sourceMappingURL=BaseFloatingPicker.scss.js.map