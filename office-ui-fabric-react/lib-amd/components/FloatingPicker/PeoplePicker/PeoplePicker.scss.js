define(["require", "exports", "@microsoft/load-themed-styles"], function (require, exports, load_themed_styles_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    load_themed_styles_1.loadStyles([{ "rawString": ".resultContent_f536abdc{display:table-row}.resultContent_f536abdc .resultItem_f536abdc{display:table-cell;vertical-align:bottom}.peoplePickerPersona_f536abdc{width:180px}.peoplePickerPersona_f536abdc .ms-Persona-details{width:100%}.peoplePicker_f536abdc .ms-BasePicker-text{min-height:40px}.peoplePickerPersonaContent_f536abdc{display:-webkit-box;display:-ms-flexbox;display:flex;width:100%;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:7px 12px}\n" }]);
    exports.resultContent = "resultContent_f536abdc";
    exports.resultItem = "resultItem_f536abdc";
    exports.peoplePickerPersona = "peoplePickerPersona_f536abdc";
    exports.peoplePicker = "peoplePicker_f536abdc";
    exports.peoplePickerPersonaContent = "peoplePickerPersonaContent_f536abdc";
});
//# sourceMappingURL=PeoplePicker.scss.js.map