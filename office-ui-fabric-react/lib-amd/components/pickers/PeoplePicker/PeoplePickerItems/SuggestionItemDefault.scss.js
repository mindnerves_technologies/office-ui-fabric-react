define(["require", "exports", "@microsoft/load-themed-styles"], function (require, exports, load_themed_styles_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    load_themed_styles_1.loadStyles([{ "rawString": ".peoplePickerPersona_e5789002{width:180px}.peoplePickerPersonaContent_e5789002{display:-webkit-box;display:-ms-flexbox;display:flex;width:100%;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:7px 12px}\n" }]);
    exports.peoplePickerPersona = "peoplePickerPersona_e5789002";
    exports.peoplePickerPersonaContent = "peoplePickerPersonaContent_e5789002";
});
//# sourceMappingURL=SuggestionItemDefault.scss.js.map