define(["require", "exports", "@microsoft/load-themed-styles"], function (require, exports, load_themed_styles_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    load_themed_styles_1.loadStyles([{ "rawString": ".ImageExample .ms-Image{border:2px solid " }, { "theme": "neutralSecondary", "defaultValue": "#605e5c" }, { "rawString": ";margin-bottom:20px}\n" }]);
});
//# sourceMappingURL=ImagePage.global.scss.js.map