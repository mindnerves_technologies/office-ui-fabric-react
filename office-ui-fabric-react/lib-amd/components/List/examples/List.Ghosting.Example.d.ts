import * as React from 'react';
import { IExampleItem } from '@uifabric/example-data';
export interface IListGhostingExampleProps {
    items?: IExampleItem[];
}
export declare class ListGhostingExample extends React.Component<IListGhostingExampleProps> {
    private _items;
    constructor(props: IListGhostingExampleProps);
    render(): JSX.Element;
    private _onRenderCell;
}
