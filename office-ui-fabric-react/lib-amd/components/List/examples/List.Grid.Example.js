define(["require", "exports", "tslib", "react", "office-ui-fabric-react/lib/FocusZone", "office-ui-fabric-react/lib/List", "office-ui-fabric-react/lib/Styling", "@uifabric/example-data"], function (require, exports, tslib_1, React, FocusZone_1, List_1, Styling_1, example_data_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var theme = Styling_1.getTheme();
    var palette = theme.palette, fonts = theme.fonts;
    var classNames = Styling_1.mergeStyleSets({
        listGridExample: {
            overflow: 'hidden',
            fontSize: 0,
            position: 'relative',
        },
        listGridExampleTile: {
            textAlign: 'center',
            outline: 'none',
            position: 'relative',
            float: 'left',
            background: palette.neutralLighter,
            selectors: {
                'focus:after': {
                    content: '',
                    position: 'absolute',
                    left: 2,
                    right: 2,
                    top: 2,
                    bottom: 2,
                    boxSizing: 'border-box',
                    border: "1px solid " + palette.white,
                },
            },
        },
        listGridExampleSizer: {
            paddingBottom: '100%',
        },
        listGridExamplePadder: {
            position: 'absolute',
            left: 2,
            top: 2,
            right: 2,
            bottom: 2,
        },
        listGridExampleLabel: {
            background: 'rgba(0, 0, 0, 0.3)',
            color: '#FFFFFF',
            position: 'absolute',
            padding: 10,
            bottom: 0,
            left: 0,
            width: '100%',
            fontSize: fonts.small.fontSize,
            boxSizing: 'border-box',
        },
        listGridExampleImage: {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
        },
    });
    var ROWS_PER_PAGE = 3;
    var MAX_ROW_HEIGHT = 250;
    var ListGridExample = /** @class */ (function (_super) {
        tslib_1.__extends(ListGridExample, _super);
        function ListGridExample(props) {
            var _this = _super.call(this, props) || this;
            _this._getItemCountForPage = function (itemIndex, surfaceRect) {
                if (itemIndex === 0) {
                    _this._columnCount = Math.ceil(surfaceRect.width / MAX_ROW_HEIGHT);
                    _this._columnWidth = Math.floor(surfaceRect.width / _this._columnCount);
                    _this._rowHeight = _this._columnWidth;
                }
                return _this._columnCount * ROWS_PER_PAGE;
            };
            _this._getPageHeight = function () {
                return _this._rowHeight * ROWS_PER_PAGE;
            };
            _this._onRenderCell = function (item, index) {
                return (React.createElement("div", { className: classNames.listGridExampleTile, "data-is-focusable": true, style: {
                        width: 100 / _this._columnCount + '%',
                    } },
                    React.createElement("div", { className: classNames.listGridExampleSizer },
                        React.createElement("div", { className: classNames.listGridExamplePadder },
                            React.createElement("img", { src: item.thumbnail, className: classNames.listGridExampleImage }),
                            React.createElement("span", { className: classNames.listGridExampleLabel }, "item " + index)))));
            };
            _this._items = props.items || example_data_1.createListItems(5000);
            return _this;
        }
        ListGridExample.prototype.render = function () {
            return (React.createElement(FocusZone_1.FocusZone, null,
                React.createElement(List_1.List, { className: classNames.listGridExample, items: this._items, getItemCountForPage: this._getItemCountForPage, getPageHeight: this._getPageHeight, renderedWindowsAhead: 4, onRenderCell: this._onRenderCell })));
        };
        return ListGridExample;
    }(React.Component));
    exports.ListGridExample = ListGridExample;
});
//# sourceMappingURL=List.Grid.Example.js.map