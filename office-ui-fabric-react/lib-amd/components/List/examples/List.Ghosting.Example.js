define(["require", "exports", "tslib", "react", "office-ui-fabric-react/lib/FocusZone", "office-ui-fabric-react/lib/List", "office-ui-fabric-react/lib/Image", "office-ui-fabric-react/lib/Styling", "@uifabric/example-data"], function (require, exports, tslib_1, React, FocusZone_1, List_1, Image_1, Styling_1, example_data_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var theme = Styling_1.getTheme();
    var palette = theme.palette, semanticColors = theme.semanticColors, fonts = theme.fonts;
    var classNames = Styling_1.mergeStyleSets({
        container: {
            overflow: 'auto',
            maxHeight: 500,
        },
        itemCell: [
            Styling_1.getFocusStyle(theme, { inset: -1 }),
            {
                minHeight: 54,
                padding: 10,
                boxSizing: 'border-box',
                borderBottom: "1px solid " + semanticColors.bodyDivider,
                display: 'flex',
                selectors: {
                    '&:hover': { background: palette.neutralLight },
                },
            },
        ],
        itemImage: {
            flexShrink: 0,
        },
        itemContent: {
            marginLeft: 10,
            overflow: 'hidden',
            flexGrow: 1,
        },
        itemName: [
            fonts.xLarge,
            {
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
            },
        ],
        itemIndex: {
            fontSize: fonts.small.fontSize,
            color: palette.neutralTertiary,
            marginBottom: 10,
        },
        chevron: {
            alignSelf: 'center',
            marginLeft: 10,
            color: palette.neutralTertiary,
            fontSize: fonts.large.fontSize,
            flexShrink: 0,
        },
    });
    var ListGhostingExample = /** @class */ (function (_super) {
        tslib_1.__extends(ListGhostingExample, _super);
        function ListGhostingExample(props) {
            var _this = _super.call(this, props) || this;
            _this._items = props.items || example_data_1.createListItems(5000);
            return _this;
        }
        ListGhostingExample.prototype.render = function () {
            return (React.createElement(FocusZone_1.FocusZone, { direction: FocusZone_1.FocusZoneDirection.vertical },
                React.createElement("div", { className: classNames.container, "data-is-scrollable": true },
                    React.createElement(List_1.List, { items: this._items, onRenderCell: this._onRenderCell }))));
        };
        ListGhostingExample.prototype._onRenderCell = function (item, index, isScrolling) {
            return (React.createElement("div", { className: classNames.itemCell, "data-is-focusable": true },
                React.createElement(Image_1.Image, { className: classNames.itemImage, src: isScrolling ? undefined : item.thumbnail, width: 50, height: 50, imageFit: Image_1.ImageFit.cover }),
                React.createElement("div", { className: classNames.itemContent },
                    React.createElement("div", { className: classNames.itemName }, item.name),
                    React.createElement("div", { className: classNames.itemIndex }, "Item " + index))));
        };
        return ListGhostingExample;
    }(React.Component));
    exports.ListGhostingExample = ListGhostingExample;
});
//# sourceMappingURL=List.Ghosting.Example.js.map