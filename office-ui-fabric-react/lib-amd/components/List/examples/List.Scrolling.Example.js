define(["require", "exports", "tslib", "react", "office-ui-fabric-react/lib/FocusZone", "office-ui-fabric-react/lib/Button", "office-ui-fabric-react/lib/Dropdown", "office-ui-fabric-react/lib/List", "office-ui-fabric-react/lib/TextField", "office-ui-fabric-react/lib/Checkbox", "@uifabric/example-data", "office-ui-fabric-react/lib/Styling"], function (require, exports, tslib_1, React, FocusZone_1, Button_1, Dropdown_1, List_1, TextField_1, Checkbox_1, example_data_1, Styling_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var theme = Styling_1.getTheme();
    var styles = Styling_1.mergeStyleSets({
        container: {
            overflow: 'auto',
            maxHeight: 500,
            marginTop: 20,
            selectors: {
                '.ms-List-cell:nth-child(odd)': {
                    height: 50,
                    lineHeight: 50,
                    background: theme.palette.neutralLighter,
                },
                '.ms-List-cell:nth-child(even)': {
                    height: 25,
                    lineHeight: 25,
                },
            },
        },
        itemContent: [
            theme.fonts.medium,
            Styling_1.normalize,
            {
                position: 'relative',
                display: 'block',
                borderLeft: '3px solid ' + theme.palette.themePrimary,
                paddingLeft: 27,
            },
        ],
    });
    var evenItemHeight = 25;
    var oddItemHeight = 50;
    var numberOfItemsOnPage = 10;
    var ListScrollingExample = /** @class */ (function (_super) {
        tslib_1.__extends(ListScrollingExample, _super);
        function ListScrollingExample(props) {
            var _this = _super.call(this, props) || this;
            _this._onChangeText = function (ev, value) {
                _this._scroll(parseInt(value, 10) || 0, _this.state.scrollToMode);
            };
            _this._onDropdownChange = function (event, option) {
                var scrollMode = _this.state.scrollToMode;
                switch (option.key) {
                    case 'auto':
                        scrollMode = List_1.ScrollToMode.auto;
                        break;
                    case 'top':
                        scrollMode = List_1.ScrollToMode.top;
                        break;
                    case 'bottom':
                        scrollMode = List_1.ScrollToMode.bottom;
                        break;
                    case 'center':
                        scrollMode = List_1.ScrollToMode.center;
                        break;
                }
                _this._scroll(_this.state.selectedIndex, scrollMode);
            };
            _this._onRenderCell = function (item, index) {
                return (React.createElement("div", { "data-is-focusable": true },
                    React.createElement("div", { className: styles.itemContent },
                        index,
                        " \u00A0 ",
                        item.name)));
            };
            _this._scrollRelative = function (delta) {
                return function () {
                    _this._scroll(_this.state.selectedIndex + delta, _this.state.scrollToMode);
                };
            };
            _this._scroll = function (index, scrollToMode) {
                var updatedSelectedIndex = Math.min(Math.max(index, 0), _this._items.length - 1);
                _this.setState({
                    selectedIndex: updatedSelectedIndex,
                    scrollToMode: scrollToMode,
                }, function () {
                    _this._list.scrollToIndex(updatedSelectedIndex, function (idx) { return (idx % 2 === 0 ? evenItemHeight : oddItemHeight); }, scrollToMode);
                });
            };
            _this._resolveList = function (list) {
                _this._list = list;
            };
            _this._onShowItemIndexInViewChanged = function (event, checked) {
                _this.setState({
                    showItemIndexInView: checked,
                });
            };
            _this._items = props.items || example_data_1.createListItems(5000);
            _this.state = {
                selectedIndex: 0,
                scrollToMode: List_1.ScrollToMode.auto,
                showItemIndexInView: false,
            };
            return _this;
        }
        ListScrollingExample.prototype.render = function () {
            return (React.createElement(FocusZone_1.FocusZone, { direction: FocusZone_1.FocusZoneDirection.vertical },
                React.createElement("div", null,
                    React.createElement(Button_1.DefaultButton, { onClick: this._scrollRelative(-10) }, "-10"),
                    React.createElement(Button_1.DefaultButton, { onClick: this._scrollRelative(-1) }, "-1"),
                    React.createElement(Button_1.DefaultButton, { onClick: this._scrollRelative(1) }, "+1"),
                    React.createElement(Button_1.DefaultButton, { onClick: this._scrollRelative(10) }, "+10")),
                React.createElement(Dropdown_1.Dropdown, { placeholder: "Select an Option", label: "Scroll To Mode:", ariaLabel: "Scroll To Mode", defaultSelectedKey: 'auto', options: [
                        { key: 'auto', text: 'Auto' },
                        { key: 'top', text: 'Top' },
                        { key: 'bottom', text: 'Bottom' },
                        { key: 'center', text: 'Center' },
                    ], onChange: this._onDropdownChange }),
                React.createElement("div", null,
                    "Scroll item index:",
                    React.createElement(TextField_1.TextField, { value: this.state.selectedIndex.toString(10), onChange: this._onChangeText })),
                React.createElement("div", null,
                    React.createElement(Checkbox_1.Checkbox, { label: "Show index of the first item in view when unmounting", checked: this.state.showItemIndexInView, onChange: this._onShowItemIndexInViewChanged })),
                React.createElement("div", { className: styles.container, "data-is-scrollable": true },
                    React.createElement(List_1.List, { ref: this._resolveList, items: this._items, getPageHeight: this._getPageHeight, onRenderCell: this._onRenderCell }))));
        };
        ListScrollingExample.prototype.componentWillUnmount = function () {
            if (this.state.showItemIndexInView) {
                var itemIndexInView = this._list.getStartItemIndexInView(function (idx) { return (idx % 2 === 0 ? evenItemHeight : oddItemHeight); } /* measureItem */);
                alert('unmounting, getting first item index that was in view: ' + itemIndexInView);
            }
        };
        ListScrollingExample.prototype._getPageHeight = function (idx) {
            var h = 0;
            for (var i = idx; i < idx + numberOfItemsOnPage; ++i) {
                var isEvenRow = i % 2 === 0;
                h += isEvenRow ? evenItemHeight : oddItemHeight;
            }
            return h;
        };
        return ListScrollingExample;
    }(React.Component));
    exports.ListScrollingExample = ListScrollingExample;
});
//# sourceMappingURL=List.Scrolling.Example.js.map