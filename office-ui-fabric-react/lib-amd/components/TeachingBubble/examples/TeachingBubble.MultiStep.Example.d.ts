import * as React from 'react';
export interface ITeachingBubbleMultiStepExampleState {
    isTeachingBubbleVisible?: boolean;
}
export declare class TeachingBubbleMultiStepExample extends React.Component<{}, ITeachingBubbleMultiStepExampleState> {
    private _menuButtonElement;
    constructor(props: {});
    render(): JSX.Element;
    private _onDismiss;
    private _onShow;
}
