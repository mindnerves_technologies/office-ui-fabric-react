import * as React from 'react';
export interface ITeachingBubbleWideExampleState {
    isTeachingBubbleVisible?: boolean;
}
export declare class TeachingBubbleWideExample extends React.Component<{}, ITeachingBubbleWideExampleState> {
    private _menuButtonElement;
    constructor(props: {});
    render(): JSX.Element;
    private _onDismiss;
    private _onShow;
}
