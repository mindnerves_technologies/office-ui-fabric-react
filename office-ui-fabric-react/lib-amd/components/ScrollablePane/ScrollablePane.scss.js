define(["require", "exports", "@microsoft/load-themed-styles"], function (require, exports, load_themed_styles_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    load_themed_styles_1.loadStyles([{ "rawString": ".root_e2dcdb7c{overflow-y:auto;max-height:inherit;height:inherit;-webkit-overflow-scrolling:touch}.stickyAbove_e2dcdb7c,.stickyBelow_e2dcdb7c{position:absolute;pointer-events:auto;width:100%;z-index:1}.stickyAbove_e2dcdb7c{top:0}@media screen and (-ms-high-contrast: active){.stickyAbove_e2dcdb7c{border-bottom:1px solid WindowText}}.stickyBelow_e2dcdb7c{bottom:0}@media screen and (-ms-high-contrast: active){.stickyBelow_e2dcdb7c{border-top:1px solid WindowText}}\n" }]);
    exports.root = "root_e2dcdb7c";
    exports.stickyAbove = "stickyAbove_e2dcdb7c";
    exports.stickyBelow = "stickyBelow_e2dcdb7c";
});
//# sourceMappingURL=ScrollablePane.scss.js.map