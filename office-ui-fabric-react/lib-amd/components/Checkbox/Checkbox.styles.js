define(["require", "exports", "../../Styling", "../../Utilities"], function (require, exports, Styling_1, Utilities_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var GlobalClassNames = {
        root: 'ms-Checkbox',
        label: 'ms-Checkbox-label',
        checkbox: 'ms-Checkbox-checkbox',
        checkmark: 'ms-Checkbox-checkmark',
        text: 'ms-Checkbox-text',
    };
    var MS_CHECKBOX_LABEL_SIZE = '20px';
    var MS_CHECKBOX_TRANSITION_DURATION = '200ms';
    var MS_CHECKBOX_TRANSITION_TIMING = 'cubic-bezier(.4, 0, .23, 1)';
    exports.getStyles = function (props) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
        var className = props.className, theme = props.theme, reversed = props.reversed, checked = props.checked, disabled = props.disabled, isUsingCustomLabelRender = props.isUsingCustomLabelRender, indeterminate = props.indeterminate;
        var semanticColors = theme.semanticColors, effects = theme.effects, palette = theme.palette, fonts = theme.fonts;
        var classNames = Styling_1.getGlobalClassNames(GlobalClassNames, theme);
        var checkmarkFontColor = semanticColors.inputForegroundChecked;
        // TODO: after updating the semanticColors slots mapping this needs to be semanticColors.inputBorder
        var checkmarkFontColorHovered = palette.neutralSecondary;
        // TODO: after updating the semanticColors slots mapping this needs to be semanticColors.smallInputBorder
        var checkboxBorderColor = palette.neutralPrimary;
        var checkboxBorderIndeterminateColor = semanticColors.inputBackgroundChecked;
        var checkboxBorderColorChecked = semanticColors.inputBackgroundChecked;
        var checkboxBorderColorDisabled = semanticColors.disabledBodySubtext;
        var checkboxBorderHoveredColor = semanticColors.inputBorderHovered;
        var checkboxBorderIndeterminateHoveredColor = semanticColors.inputBackgroundCheckedHovered;
        var checkboxBackgroundChecked = semanticColors.inputBackgroundChecked;
        // TODO: after updating the semanticColors slots mapping the following 2 tokens need to be
        // semanticColors.inputBackgroundCheckedHovered
        var checkboxBackgroundCheckedHovered = semanticColors.inputBackgroundCheckedHovered;
        var checkboxBorderColorCheckedHovered = semanticColors.inputBackgroundCheckedHovered;
        var checkboxHoveredTextColor = semanticColors.inputTextHovered;
        var checkboxBackgroundDisabledChecked = semanticColors.disabledBodySubtext;
        var checkboxTextColor = semanticColors.bodyText;
        var checkboxTextColorDisabled = semanticColors.disabledText;
        var indeterminateDotStyles = [
            {
                content: '""',
                borderRadius: effects.roundedCorner2,
                position: 'absolute',
                width: 10,
                height: 10,
                top: 4,
                left: 4,
                boxSizing: 'border-box',
                borderWidth: 5,
                borderStyle: 'solid',
                borderColor: disabled ? checkboxBorderColorDisabled : checkboxBorderIndeterminateColor,
                transitionProperty: 'border-width, border, border-color',
                transitionDuration: MS_CHECKBOX_TRANSITION_DURATION,
                transitionTimingFunction: MS_CHECKBOX_TRANSITION_TIMING,
            },
        ];
        return {
            root: [
                classNames.root,
                {
                    position: 'relative',
                    display: 'flex',
                },
                reversed && 'reversed',
                checked && 'is-checked',
                !disabled && 'is-enabled',
                disabled && 'is-disabled',
                !disabled && [
                    !checked && {
                        selectors: (_a = {},
                            _a[":hover ." + classNames.checkbox] = {
                                borderColor: checkboxBorderHoveredColor,
                                selectors: (_b = {},
                                    _b[Styling_1.HighContrastSelector] = {
                                        borderColor: 'Highlight',
                                    },
                                    _b),
                            },
                            _a[":focus ." + classNames.checkbox] = { borderColor: checkboxBorderHoveredColor },
                            _a[":hover ." + classNames.checkmark] = {
                                color: checkmarkFontColorHovered,
                                opacity: '1',
                                selectors: (_c = {},
                                    _c[Styling_1.HighContrastSelector] = {
                                        color: 'Highlight',
                                    },
                                    _c),
                            },
                            _a),
                    },
                    checked &&
                        !indeterminate && {
                        selectors: (_d = {},
                            _d[":hover ." + classNames.checkbox] = {
                                background: checkboxBackgroundCheckedHovered,
                                borderColor: checkboxBorderColorCheckedHovered,
                            },
                            _d[":focus ." + classNames.checkbox] = {
                                background: checkboxBackgroundCheckedHovered,
                                borderColor: checkboxBorderColorCheckedHovered,
                            },
                            _d["." + classNames.checkbox] = {
                                background: checkboxBorderColorChecked,
                                borderColor: checkboxBorderColorChecked,
                            },
                            _d[Styling_1.HighContrastSelector] = {
                                selectors: (_e = {},
                                    _e[":hover ." + classNames.checkbox] = {
                                        background: 'Window',
                                        borderColor: 'Highlight',
                                    },
                                    _e[":focus ." + classNames.checkbox] = {
                                        background: 'Highlight',
                                    },
                                    _e[":focus:hover ." + classNames.checkbox] = {
                                        background: 'Highlight',
                                    },
                                    _e[":focus:hover ." + classNames.checkmark] = {
                                        color: 'Window',
                                    },
                                    _e[":hover ." + classNames.checkmark] = {
                                        color: 'Highlight',
                                    },
                                    _e),
                            },
                            _d),
                    },
                    indeterminate && {
                        selectors: (_f = {},
                            _f[":hover ." + classNames.checkbox + ", :hover ." + classNames.checkbox + ":after"] = {
                                borderColor: checkboxBorderIndeterminateHoveredColor,
                            },
                            _f[":focus ." + classNames.checkbox] = {
                                borderColor: checkboxBorderIndeterminateHoveredColor,
                            },
                            _f[":hover ." + classNames.checkmark] = {
                                opacity: '0',
                            },
                            _f),
                    },
                    {
                        selectors: (_g = {},
                            _g[":hover ." + classNames.text] = { color: checkboxHoveredTextColor },
                            _g[":focus ." + classNames.text] = { color: checkboxHoveredTextColor },
                            _g),
                    },
                ],
                className,
            ],
            input: {
                position: 'absolute',
                background: 'none',
                opacity: 0,
                selectors: (_h = {},
                    _h["." + Utilities_1.IsFocusVisibleClassName + " &:focus + label::before"] = {
                        outline: '1px solid ' + theme.palette.neutralSecondary,
                        outlineOffset: '2px',
                        selectors: (_j = {},
                            _j[Styling_1.HighContrastSelector] = {
                                outline: '1px solid ActiveBorder',
                            },
                            _j),
                    },
                    _h),
            },
            label: [
                classNames.label,
                theme.fonts.medium,
                {
                    display: 'flex',
                    alignItems: isUsingCustomLabelRender ? 'center' : 'flex-start',
                    cursor: disabled ? 'default' : 'pointer',
                    position: 'relative',
                    userSelect: 'none',
                    textAlign: 'left',
                },
                reversed && {
                    flexDirection: 'row-reverse',
                    justifyContent: 'flex-end',
                },
                {
                    selectors: {
                        '&::before': {
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            content: '""',
                            pointerEvents: 'none',
                        },
                    },
                },
            ],
            checkbox: [
                classNames.checkbox,
                {
                    position: 'relative',
                    display: 'flex',
                    flexShrink: 0,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: MS_CHECKBOX_LABEL_SIZE,
                    width: MS_CHECKBOX_LABEL_SIZE,
                    border: "1px solid " + checkboxBorderColor,
                    borderRadius: effects.roundedCorner2,
                    boxSizing: 'border-box',
                    transitionProperty: 'background, border, border-color',
                    transitionDuration: MS_CHECKBOX_TRANSITION_DURATION,
                    transitionTimingFunction: MS_CHECKBOX_TRANSITION_TIMING,
                    /* in case the icon is bigger than the box */
                    overflow: 'hidden',
                    selectors: {
                        ':after': indeterminate ? indeterminateDotStyles : null,
                    },
                },
                indeterminate && {
                    borderColor: checkboxBorderIndeterminateColor,
                },
                !reversed
                    ? // This margin on the checkbox is for backwards compat. Notably it has the effect where a customRender
                        // is used, there will be only a 4px margin from checkbox to label. The label by default would have
                        // another 4px margin for a total of 8px margin between checkbox and label. We don't combine the two
                        // (and move it into the text) to not incur a breaking change for everyone using custom render atm.
                        {
                            marginRight: 4,
                        }
                    : {
                        marginLeft: 4,
                    },
                !disabled &&
                    !indeterminate &&
                    checked && {
                    background: checkboxBackgroundChecked,
                    borderColor: checkboxBorderColorChecked,
                    selectors: (_k = {},
                        _k[Styling_1.HighContrastSelector] = {
                            background: 'Highlight',
                            borderColor: 'Highlight',
                        },
                        _k),
                },
                disabled && {
                    borderColor: checkboxBorderColorDisabled,
                    selectors: (_l = {},
                        _l[Styling_1.HighContrastSelector] = {
                            borderColor: 'InactiveBorder',
                        },
                        _l),
                },
                checked &&
                    disabled && {
                    background: checkboxBackgroundDisabledChecked,
                    borderColor: checkboxBorderColorDisabled,
                },
            ],
            checkmark: [
                classNames.checkmark,
                {
                    opacity: checked ? '1' : '0',
                    color: checkmarkFontColor,
                    selectors: (_m = {},
                        _m[Styling_1.HighContrastSelector] = {
                            color: disabled ? 'InactiveBorder' : 'Window',
                            MsHighContrastAdjust: 'none',
                        },
                        _m),
                },
            ],
            text: [
                classNames.text,
                {
                    color: disabled ? checkboxTextColorDisabled : checkboxTextColor,
                    fontSize: fonts.medium.fontSize,
                    lineHeight: '20px',
                },
                !reversed
                    ? {
                        marginLeft: 4,
                    }
                    : {
                        marginRight: 4,
                    },
                disabled && {
                    selectors: (_o = {},
                        _o[Styling_1.HighContrastSelector] = {
                            // backwards compat for the color of the text when the checkbox was rendered
                            // using a Button.
                            color: 'InactiveBorder',
                        },
                        _o),
                },
            ],
        };
    };
});
//# sourceMappingURL=Checkbox.styles.js.map