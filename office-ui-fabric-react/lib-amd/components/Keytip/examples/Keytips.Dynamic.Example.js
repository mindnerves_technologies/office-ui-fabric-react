define(["require", "exports", "tslib", "react", "office-ui-fabric-react/lib/Button"], function (require, exports, tslib_1, React, Button_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var KeytipsDynamicExample = /** @class */ (function (_super) {
        tslib_1.__extends(KeytipsDynamicExample, _super);
        function KeytipsDynamicExample(props) {
            var _this = _super.call(this, props) || this;
            _this.setCurrButton = function (button) {
                return function () {
                    _this.setState({ currButton: button });
                };
            };
            _this.state = {
                currButton: 'Button 1',
            };
            return _this;
        }
        /* tslint:disable:jsx-no-lambda */
        KeytipsDynamicExample.prototype.render = function () {
            var startSequence = this.state.currButton === 'Button 1' ? 'gg1' : 'gg2';
            return (React.createElement("div", null,
                React.createElement("p", null, "There is another special case where controls on the page will change other controls down the chain in the keytip sequence. Take the case below; clicking Button 1 and Button 2 will update the text of Button3. Triggering the keytip for Button 1 or Button 2 will then also change the keytip sequence of Button 3, because it can be both a child of Button 1 or Button 2. For this to work fully, Button 1 and Button 2 should have `hasDynamicChildren: true` in their keytip props"),
                React.createElement(Button_1.DefaultButton, { text: "Button 1", onClick: this.setCurrButton('Button 1'), keytipProps: {
                        content: 'GG1',
                        keySequences: ['gg1'],
                        onExecute: this.setCurrButton('Button 1'),
                        hasDynamicChildren: true,
                    } }),
                React.createElement(Button_1.DefaultButton, { text: "Button 2", onClick: this.setCurrButton('Button 2'), keytipProps: {
                        content: 'GG2',
                        keySequences: ['gg2'],
                        onExecute: this.setCurrButton('Button 2'),
                        hasDynamicChildren: true,
                    } }),
                React.createElement("div", null,
                    React.createElement(Button_1.DefaultButton, { text: 'Button 3, active button is: ' + this.state.currButton, keytipProps: { content: 'GG3', keySequences: [startSequence, 'gg3'] } }))));
        };
        return KeytipsDynamicExample;
    }(React.Component));
    exports.KeytipsDynamicExample = KeytipsDynamicExample;
});
//# sourceMappingURL=Keytips.Dynamic.Example.js.map