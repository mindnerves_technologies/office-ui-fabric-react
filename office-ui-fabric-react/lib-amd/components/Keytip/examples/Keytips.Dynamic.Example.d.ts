import * as React from 'react';
interface IKeytipsTestState {
    currButton: string;
}
export declare class KeytipsDynamicExample extends React.Component<{}, IKeytipsTestState> {
    constructor(props: {});
    render(): JSX.Element;
    private setCurrButton;
}
export {};
