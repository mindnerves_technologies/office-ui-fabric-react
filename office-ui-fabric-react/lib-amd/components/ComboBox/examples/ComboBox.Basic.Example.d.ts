import * as React from 'react';
interface IComboBoxBasicExampleState {
    dynamicErrorValue: number | string;
}
export declare class ComboBoxBasicExample extends React.Component<{}, IComboBoxBasicExampleState> {
    private _basicComboBox;
    constructor(props: {});
    render(): JSX.Element;
    private _onChange;
    private _getErrorMessage;
}
export {};
