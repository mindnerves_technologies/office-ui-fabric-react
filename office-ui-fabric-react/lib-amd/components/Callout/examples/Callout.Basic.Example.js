define(["require", "exports", "tslib", "react", "office-ui-fabric-react"], function (require, exports, tslib_1, React, office_ui_fabric_react_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    // Themed styles for the example.
    var theme = office_ui_fabric_react_1.getTheme();
    var styles = office_ui_fabric_react_1.mergeStyleSets({
        buttonArea: {
            verticalAlign: 'top',
            display: 'inline-block',
            textAlign: 'center',
            margin: '0 100px',
            minWidth: 130,
            height: 32,
        },
        callout: {
            maxWidth: 300,
        },
        header: {
            padding: '18px 24px 12px',
        },
        title: [
            theme.fonts.xLarge,
            {
                margin: 0,
                fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
            },
        ],
        inner: {
            height: '100%',
            padding: '0 24px 20px',
        },
        actions: {
            position: 'relative',
            marginTop: 20,
            width: '100%',
            whiteSpace: 'nowrap',
        },
        subtext: [
            theme.fonts.small,
            {
                margin: 0,
                fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
            },
        ],
        link: [
            theme.fonts.medium,
            {
                color: theme.palette.neutralPrimary,
            },
        ],
    });
    // Example code
    var CalloutBasicExample = /** @class */ (function (_super) {
        tslib_1.__extends(CalloutBasicExample, _super);
        function CalloutBasicExample() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.state = {
                isCalloutVisible: false,
            };
            _this._menuButtonElement = React.createRef();
            // Use getId() to ensure that the callout label and description IDs are unique on the page.
            // (It's also okay to use plain strings without getId() and manually ensure their uniqueness.)
            _this._labelId = office_ui_fabric_react_1.getId('callout-label');
            _this._descriptionId = office_ui_fabric_react_1.getId('callout-description');
            _this._onShowMenuClicked = function () {
                _this.setState({
                    isCalloutVisible: !_this.state.isCalloutVisible,
                });
            };
            _this._onCalloutDismiss = function () {
                _this.setState({
                    isCalloutVisible: false,
                });
            };
            return _this;
        }
        CalloutBasicExample.prototype.render = function () {
            var isCalloutVisible = this.state.isCalloutVisible;
            return (React.createElement(React.Fragment, null,
                React.createElement("div", { className: styles.buttonArea, ref: this._menuButtonElement },
                    React.createElement(office_ui_fabric_react_1.DefaultButton, { onClick: this._onShowMenuClicked, text: isCalloutVisible ? 'Hide Callout' : 'Show Callout' })),
                this.state.isCalloutVisible && (React.createElement(office_ui_fabric_react_1.Callout, { className: styles.callout, ariaLabelledBy: this._labelId, ariaDescribedBy: this._descriptionId, role: "alertdialog", gapSpace: 0, target: this._menuButtonElement.current, onDismiss: this._onCalloutDismiss, setInitialFocus: true },
                    React.createElement("div", { className: styles.header },
                        React.createElement("p", { className: styles.title, id: this._labelId }, "All of your favorite people")),
                    React.createElement("div", { className: styles.inner },
                        React.createElement("p", { className: styles.subtext, id: this._descriptionId }, "Message body is optional. If help documentation is available, consider adding a link to learn more at the bottom."),
                        React.createElement("div", { className: styles.actions },
                            React.createElement(office_ui_fabric_react_1.Link, { className: styles.link, href: "http://microsoft.com", target: "_blank" }, "Go to microsoft")))))));
        };
        return CalloutBasicExample;
    }(React.Component));
    exports.CalloutBasicExample = CalloutBasicExample;
});
//# sourceMappingURL=Callout.Basic.Example.js.map