define(["require", "exports", "tslib", "react", "office-ui-fabric-react"], function (require, exports, tslib_1, React, office_ui_fabric_react_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DIRECTION_OPTIONS = [
        { key: office_ui_fabric_react_1.DirectionalHint.topLeftEdge, text: 'Top Left Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.topCenter, text: 'Top Center' },
        { key: office_ui_fabric_react_1.DirectionalHint.topRightEdge, text: 'Top Right Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.topAutoEdge, text: 'Top Auto Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.bottomLeftEdge, text: 'Bottom Left Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.bottomCenter, text: 'Bottom Center' },
        { key: office_ui_fabric_react_1.DirectionalHint.bottomRightEdge, text: 'Bottom Right Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.bottomAutoEdge, text: 'Bottom Auto Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.leftTopEdge, text: 'Left Top Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.leftCenter, text: 'Left Center' },
        { key: office_ui_fabric_react_1.DirectionalHint.leftBottomEdge, text: 'Left Bottom Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.rightTopEdge, text: 'Right Top Edge' },
        { key: office_ui_fabric_react_1.DirectionalHint.rightCenter, text: 'Right Center' },
        { key: office_ui_fabric_react_1.DirectionalHint.rightBottomEdge, text: 'Right Bottom Edge' },
    ];
    var theme = office_ui_fabric_react_1.getTheme();
    var styles = office_ui_fabric_react_1.mergeStyleSets({
        buttonArea: {
            verticalAlign: 'top',
            display: 'inline-block',
            textAlign: 'center',
            margin: '0 100px',
            minWidth: 130,
            height: 32,
        },
        configArea: {
            minWidth: '300px',
            display: 'inline-block',
        },
        callout: {
            maxWidth: 300,
        },
        header: {
            padding: '18px 24px 12px',
        },
        title: [
            theme.fonts.xLarge,
            {
                margin: 0,
                fontWeight: office_ui_fabric_react_1.FontWeights.semilight,
            },
        ],
        inner: {
            height: '100%',
            padding: '0 24px 20px',
        },
    });
    var CalloutCoverExample = /** @class */ (function (_super) {
        tslib_1.__extends(CalloutCoverExample, _super);
        function CalloutCoverExample(props) {
            var _this = _super.call(this, props) || this;
            _this._onDismiss = _this._onDismiss.bind(_this);
            _this._onShowMenuClicked = _this._onShowMenuClicked.bind(_this);
            _this._onDirectionalChanged = _this._onDirectionalChanged.bind(_this);
            _this.state = {
                isCalloutVisible: false,
                directionalHint: office_ui_fabric_react_1.DirectionalHint.bottomLeftEdge,
            };
            return _this;
        }
        CalloutCoverExample.prototype.render = function () {
            var _this = this;
            var _a = this.state, isCalloutVisible = _a.isCalloutVisible, directionalHint = _a.directionalHint;
            // ms-Callout-smallbeak is used in this directional example to reflect all the positions.
            // Large beak will disable some position to avoid beak over the callout edge.
            return (React.createElement(React.Fragment, null,
                React.createElement("div", { className: styles.configArea },
                    React.createElement(office_ui_fabric_react_1.Dropdown, { label: "Directional hint", selectedKey: directionalHint, options: DIRECTION_OPTIONS, onChange: this._onDirectionalChanged })),
                React.createElement("div", { className: styles.buttonArea, ref: function (menuButton) { return (_this._menuButtonElement = menuButton); } },
                    React.createElement(office_ui_fabric_react_1.DefaultButton, { text: isCalloutVisible ? 'Hide callout' : 'Show callout', onClick: this._onShowMenuClicked })),
                isCalloutVisible ? (React.createElement(office_ui_fabric_react_1.Callout, { className: styles.callout, onDismiss: this._onDismiss, target: this._menuButtonElement, directionalHint: directionalHint, coverTarget: true, isBeakVisible: false, gapSpace: 0, setInitialFocus: true },
                    React.createElement("div", { className: styles.header },
                        React.createElement("p", { className: styles.title }, "I'm covering the target!")),
                    React.createElement("div", { className: styles.inner },
                        React.createElement(office_ui_fabric_react_1.DefaultButton, { onClick: this._onShowMenuClicked, text: "Click to dismiss" })))) : null));
        };
        CalloutCoverExample.prototype._onDismiss = function () {
            this.setState({ isCalloutVisible: false });
        };
        CalloutCoverExample.prototype._onShowMenuClicked = function () {
            this.setState({
                isCalloutVisible: !this.state.isCalloutVisible,
            });
        };
        CalloutCoverExample.prototype._onDirectionalChanged = function (event, option) {
            this.setState({
                directionalHint: option.key,
            });
        };
        return CalloutCoverExample;
    }(React.Component));
    exports.CalloutCoverExample = CalloutCoverExample;
});
//# sourceMappingURL=Callout.Cover.Example.js.map