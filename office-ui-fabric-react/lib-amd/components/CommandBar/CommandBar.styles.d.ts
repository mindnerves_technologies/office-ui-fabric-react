import { ICommandBarStyleProps, ICommandBarStyles } from './CommandBar.types';
export declare const getStyles: (props: ICommandBarStyleProps) => ICommandBarStyles;
